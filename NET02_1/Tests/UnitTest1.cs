﻿using NET02_1;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ToString_DecimalProvider_RightResult()
        {
            RationalNumber target = new RationalNumber(1, 127);
            string result = target.ToString("d");
            Assert.AreEqual("0,(007874015748031496062992125984251968503937)", result);

            target = new RationalNumber(1, 13);
            result = target.ToString("d");
            Assert.AreEqual("0,(076923)", result);

            target = new RationalNumber(1, 3);
            result = target.ToString("d");
            Assert.AreEqual("0,(3)", result);

            target = new RationalNumber(20, 5);
            result = target.ToString("D");
            Assert.AreEqual("4", result);

            target = new RationalNumber(20, 8);
            result = target.ToString("D");
            Assert.AreEqual("2,5", result);

            target = new RationalNumber(-20, 8);
            result = target.ToString("D");
            Assert.AreEqual("-2,5", result);

            target = new RationalNumber(11, 56);
            result = target.ToString("D");
            Assert.AreEqual("0,196(428571)", result);

            target = new RationalNumber(2, 5);
            result = target.ToString("D");
            Assert.AreEqual("0,4", result);

            target = new RationalNumber(1, 56);
            result = target.ToString("D");
            Assert.AreEqual("0,017(857142)", result);
        }

        [TestMethod]
        public void ToString_WithourOrNormalProvider_RightResult()
        {
            RationalNumber target = new RationalNumber(1, 127);
            string result = target.ToString();
            Assert.AreEqual("1/127", result);

            target = new RationalNumber(-1, 127);
            result = target.ToString();
            Assert.AreEqual("-1/127", result);

            target = new RationalNumber(1, 127);
            result = target.ToString("n");
            Assert.AreEqual("1/127", result);

            target = new RationalNumber(-1, 127);
            result = target.ToString("N");
            Assert.AreEqual("-1/127", result);

            target = new RationalNumber(0, 2);
            result = target.ToString("N");
            Assert.AreEqual("0", result);
        }

        //[TestMethod]
        //public void Parse_ValidValue_True()
        //{
        //    RationalNumber target;
        //    RationalNumber tmp = new RationalNumber(1, 2);
        //    //bool result;

        //    target = RationalNumber.Parse("1/2");
        //    Assert.AreEqual(target, tmp);

        //    target = RationalNumber.Parse("+1/2");
        //    //Assert.IsTrue(result);

        //    target = RationalNumber.Parse("-1/2");
        //    //Assert.IsTrue(result);
        //}

        //[TestMethod]
        //public void Parse_WrongValue_False()
        //{
        //    RationalNumber target;

        //    target = RationalNumber.Parse("++1/2");
        //    //Assert.IsFalse(result);

        //    target = RationalNumber.Parse("1/+2");
        //    //Assert.IsFalse(result);
        //}
    }
}
