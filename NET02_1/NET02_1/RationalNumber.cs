﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace NET02_1
{
    public class RationalNumber : IComparable<RationalNumber>, IEquatable<RationalNumber>
    {
        private int _n;
        private uint _m;

        public RationalNumber()
            : this(0, 1, false)
        {
        }

        public RationalNumber(int n, uint m = 1)
            : this(n, m, false)
        {
        }

        public RationalNumber(int n, uint m, bool simplify)
        {
            if (m == 0)
            {
                throw new ArgumentOutOfRangeException("Divisor can't be equal zero.");
            }

            _n = n;
            _m = m;

            if (simplify)
            {
                Simplify();
            }
        }
        
        public static RationalNumber Parse(string s)
        {
            int n = 0;
            uint m = 1;
            var parts = Regex.Split(s, @"/");

            if (parts.Length > 2)
            {
                throw new ArgumentException("Wrong value");
            }

            n = int.Parse(parts[0]);
            m = uint.Parse(parts[1]);

            return new RationalNumber(n, m);
        }

        public override string ToString() => ToString("N");

        public string ToString(string provider = "N")
        {
            Simplify();
            switch (provider.ToUpper())
            {
                case "N":
                    {
                        return (_n == _m || _n == 0) ? $"{_n}" : $"{_n}/{_m}";
                    }
                case "D":
                    {
                        return GetDecimalFraction();
                    }
                default:
                    {
                        throw new ArgumentException("Wrong type provider");
                    }
            }
        }

        private string GetDecimalFraction()
        {
            uint n = (uint)Math.Abs(_n);
            uint m = _m;
            var result = GetIntegerPart(ref n, ref m) + GetFractionalPart(n, m);
                
            return result;
        }

        private string GetIntegerPart(ref uint n, ref uint m)
        {
            StringBuilder result = new StringBuilder();

            if (n > m)
            {
                result.Append($"{(_n / _m)}");
                if (n % m != 0)
                {
                    result.Append(",");
                }
                n %= m;
            }
            else
            {
                result.Append(_n > 0 ? "0," : "-0,");
            }

            return result.ToString();
        }


        private string GetFractionalPart(uint n, uint m)
        {
            uint remainder;
            var remainders = new List<uint>();
            var result = new StringBuilder();

            while (n % m != 0)
            {
                n *= 10;
                remainder = n;

                if (remainders.Contains(n))
                {
                    result.Append(")");
                    result.Insert(GetPosition(n, remainders), "(");
                    break;
                }

                if (n / m > 0)
                {
                    result.Append($"{n / m}");
                    n -= n / m * m;
                }
                else
                {
                    result.Append("0");
                }

                remainders.Add(remainder);
            }

            return result.ToString();
        }

        private int GetPosition(uint remainder, List<uint> remainders) => remainders.FindIndex(x => x == remainder);

        public static RationalNumber operator +(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                int n = (int)(left._n * right._m) + (int)(right._n * left._m);
                uint m = left._m * right._m;
                return new RationalNumber(n, m, true);
            }

            throw new NullReferenceException();
        }
            

        public static RationalNumber operator -(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                int n = (int)(left._n * right._m) - (int)(right._n * left._m);
                uint m = left._m * right._m;
                return new RationalNumber(n , m, true);
            }

            throw new NullReferenceException();                
        }
            

        public static RationalNumber operator *(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return new RationalNumber(left._n * right._n, left._m * right._m, true);
            }

            throw new NullReferenceException();
        }


        public static RationalNumber operator /(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return new RationalNumber((int)(left._n * right._m), (uint)(left._m * right._n), true);
            }

            throw new NullReferenceException();
        }

        public static bool operator ==(RationalNumber left, RationalNumber right)
        {
             if(!IsNull(left) && !IsNull(right))
            {
                return left.Equals(right);
            }

            return false;
        }

        public static bool operator !=(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return !left.Equals(right);
            }

            return false;
        }

        public static bool operator >(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return left.CompareTo(right) > 0;
            }

            return false;
        }

        public static bool operator <(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return left.CompareTo(right) < 0;
            }

            return false;
        }

        public static bool operator >=(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return left.CompareTo(right) >= 0;
            }

            return false;
        }

        public static bool operator <=(RationalNumber left, RationalNumber right)
        {
            if (!IsNull(left) && !IsNull(right))
            {
                return left.CompareTo(right) <= 0;
            }

            return false;
        }

        public static implicit operator RationalNumber(int value) => new RationalNumber(value);

        public int CompareTo(RationalNumber other)
        {
            if (!IsNull(other))
            {
                if (_n / _m > other._n / other._m)
                    return 1;
                if (_n / _m < other._n / other._m)
                    return -1;
                else
                    return 0;
            }

            throw new NullReferenceException();
        }

        public bool Equals(RationalNumber other)
        {
            if (!IsNull(other))
            {
                var result = _n == other._n && _m == other._m;
                return result;
            }
            return false;
        }

        public override bool Equals(object obj)
        {
            RationalNumber temp = obj as RationalNumber;

            if(temp != null)
            {
                return Equals(temp);
            }

            return false;
        }

        public override int GetHashCode() =>(_n.ToString() + _m.ToString()).GetHashCode();

        public void Simplify()
        {
            for (int i = Math.Abs(_n); i >= 2; i--)
            {
                if (_n % i == 0 && _m % i == 0)
                {
                    _n = _n / i;
                    _m = (uint)(_m / i);
                }
            }
        }

        private static bool IsNull(RationalNumber value) => (object)value == null;
    }
}
