﻿using System;

namespace NET02_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = RationalNumber.Parse("+1/2");
            var b = RationalNumber.Parse("+1/2");
            var c = a.Equals(b);
            Console.WriteLine(c.ToString());
            Console.ReadLine();
        }
    }
}
