﻿using NET01_1.Entities;
using System;

namespace EntityExtensions
{
    public static class EntityExtensions
    {
        public static void SetGuid(this Entity entity)
        {
            if(Guid.Empty == entity.Id)
            {
                entity.Id = Guid.NewGuid();
            }
        }
    }
}
