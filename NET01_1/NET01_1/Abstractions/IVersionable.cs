﻿namespace NET01_1.Abstractions
{
    public interface IVersionable
    {
        byte[] Version { get; set; }
    }
}
