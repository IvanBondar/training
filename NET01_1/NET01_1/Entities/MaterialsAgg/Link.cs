﻿namespace NET01_1.Entities.MaterialsAgg
{
    public class Link : Material
    {
        public string URI { get; set; }
        public LinkTypes UriType { get; set; }

        public override Material Clone()
        {
            Link clone = new Link();
            clone.Description = Description;
            clone.Id = Id;
            return clone;
        }
    }
}
