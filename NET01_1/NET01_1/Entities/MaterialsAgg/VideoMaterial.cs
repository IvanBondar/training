﻿using NET01_1.Abstractions;

namespace NET01_1.Entities.MaterialsAgg
{
    public class VideoMaterial : Material, IVersionable
    {
        private byte[] _version;

        public const int VersionArraySize = 8;

        public string UriVideoContent { get; set; }
        public string UriImage { get; set; }
        public VideoTypes VideoType { get; set; }

        public VideoMaterial()
        {
            _version = new byte[VersionArraySize];
        }
        
        public byte[] Version
        {
            get
            {
                return _version;
            }

            set
            {
                _version = value;
            }
        }

        public override Material Clone()
        {
            VideoMaterial clone = new VideoMaterial();
            clone.UriVideoContent = UriVideoContent;
            clone.UriImage = UriImage;
            clone.VideoType = VideoType;
            clone.Description = Description;
            clone.Id = Id;
            Version.CopyTo(clone.Version, 0);

            return clone;
        }
    }
}
