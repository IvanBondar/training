﻿namespace NET01_1.Entities.MaterialsAgg
{
    public class Material : Entity
    {
        public virtual Material Clone()
        {
            Material result = new Material();
            result.Description = Description;
            result.Id = Id;

            return result;
        }

        protected Material()
        { }
    }
}
