﻿using System;

namespace NET01_1.Entities.MaterialsAgg
{
    public class TextMaterial : Material
    {
        private string _text;

        public const int MaxTextLength = 10000;

        public string Text
        {
            get
            {
                return _text;
            }
            set
            {
                if (value.Length > MaxTextLength)
                {
                    throw new ArgumentOutOfRangeException($"Wrong text length (Max length is {MaxTextLength} characters)");
                }

                _text = value;
            }
        }


        public TextMaterial()
        {
        }

        public TextMaterial(string text)
        {
            Text = text;
        }
        
        public override Material Clone()
        {
            TextMaterial clone = new TextMaterial();
            clone.Text = Text;
            clone.Description = Description;
            clone.Id = Id;

            return clone;
        }
    }
}
