﻿using System;

namespace NET01_1.Entities
{
    public abstract class Entity
    {
        private string _description;

        public Guid Id { get; set; }
        public const int MaxDescriptionLength = 256;
        public string Description
        {
            get
            {
                return _description;
            }
            set
            {
                if (value != null && value.Length > MaxDescriptionLength)
                {
                    throw new ArgumentOutOfRangeException
                        ($"Wrong description length (Max length is {MaxDescriptionLength} characters)");
                }

                _description = value;
            }
        }

        public override string ToString() 
            => string.IsNullOrEmpty(_description) ? "no description" : _description;

        public override int GetHashCode() => Id.GetHashCode();

        public override bool Equals(object obj)
        {
            Entity temp = obj as Entity;

            if(temp != null)
            {
                if(Id == temp.Id)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
