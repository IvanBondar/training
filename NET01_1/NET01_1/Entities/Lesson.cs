﻿using NET01_1.Abstractions;
using NET01_1.Entities.MaterialsAgg;
using System;

namespace NET01_1.Entities
{
    public class Lesson : Entity, ICloneable, IVersionable
    {
        private byte[] _version;

        public const int MaterialArraySize = 100;
        public const int VersionArraySize = 8;

        public Material[] LessonMaterial { get; set; }

        public Lesson()
        {
            LessonMaterial = new Material[MaterialArraySize];
            _version = new byte[VersionArraySize];
        }

        public LessonKinds GetLessonKind()
        {
            foreach(var item in LessonMaterial)
            {
                if(item is VideoMaterial)
                {
                    return LessonKinds.VideoLesson;
                }
            }

            return LessonKinds.TextLesson;
        }

        public byte[] Version
        {
            get
            {
                return _version;
            }

            set
            {
                _version = value;
            }
        }

        public object Clone()
        {
            Lesson result = new Lesson();
            result.Description = Description;
            result.Id = Id;
            Version.CopyTo(result.Version, 0);
            for (int i = 0; i < LessonMaterial.Length; i++)
            {
                if (LessonMaterial[i] != null)
                {
                    result.LessonMaterial[i] = LessonMaterial[i].Clone();
                }
            }

            return result;
        }

        public override string ToString() => base.ToString();

    }
}
