﻿using NET01_1.Entities;
using EntityExtensions;
using System;
using NET01_1.Entities.MaterialsAgg;

namespace NET01_1
{
class Program
    {
        static void Main(string[] args)
        {
            VideoMaterialClone();
            TextMaterialClone();
            LessonClone();
            LinkClone();
        }

        public static void TextMaterialClone()
        {
            TextMaterial text = new TextMaterial
            {
                Description = "text1",
                Text = "text"
            };

            TextMaterial textClone = (TextMaterial)text.Clone();

            PrintValue($"Is Equals Text Material clone? {text.Equals(textClone).ToString()}");
        }

        public static void VideoMaterialClone()
        {
            VideoMaterial video = new VideoMaterial
            {
                Description = "video",
                UriImage = "UriImage",
                UriVideoContent = "UriVideoContent",
                Version = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 },
                VideoType = VideoTypes.Mpeg4,
            };

            VideoMaterial videoClone = (VideoMaterial)video.Clone();

            PrintValue($"Is Equals Video Material clone? {video.Equals(videoClone).ToString()}");
        }

        public static void LinkClone()
        {
            Link link = new Link
            {
                Description = "link",
                URI = "linkUri",
                UriType = LinkTypes.Absolute
            };

            Link linkClone = (Link)link.Clone();

            PrintValue($"Is Equals Link clone? {link.Equals(linkClone).ToString()}");
        }

        public static void LessonClone()
        {
            VideoMaterial video = new VideoMaterial
            {
                Description = "video",
                UriImage = "UriImage",
                UriVideoContent = "UriVideoContent1",
                Version = new byte[] { 0, 1, 2, 3, 4, 5, 6, 7 },
                VideoType = VideoTypes.Mpeg4,
            };

            VideoMaterial videoClone = (VideoMaterial)video.Clone();

            Link link = new Link
            {
                Description = "link",
                URI = "linkUri",
                UriType = LinkTypes.Absolute
            };

            Link linkClone = (Link)link.Clone();

            TextMaterial text = new TextMaterial
            {
                Description = "text1",
                Text = "text"
            };

            TextMaterial textClone = (TextMaterial)text.Clone();


            var lesson1 = new Lesson
            {
                Description = "123",
                Version = new byte[] { 1, 2, 3, 3, 2, 1, 2, 3 }
            };
            var lessonClone = new Lesson();
            lesson1.SetGuid();
            lesson1.LessonMaterial[0] = video;
            lesson1.LessonMaterial[1] = text;
            lesson1.LessonMaterial[2] = link;

            lessonClone = (Lesson)lesson1.Clone();

            PrintValue($"Is Equals Lesson clone? {lesson1.Equals(lessonClone).ToString()}");

        }

        public static void PrintValue(string value)
        {
            Console.Write(value);
            Console.ReadLine();
        }
    }
}
