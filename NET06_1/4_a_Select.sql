USE NET06_1
GO

SELECT p.Name AS ProjectName, r.SpentTime
FROM (SELECT t.ProjectId, SUM(wt.Time) AS SpentTime
		FROM Task AS t
		JOIN TaskResource AS tr
		ON t.TaskId = tr.TaskId
		JOIN WorkTime AS wt
		ON wt.TaskResourceId = tr.TaskResourceId
		GROUP BY t.ProjectId) AS r
JOIN Project AS p
ON p.ProjectId = r.ProjectId