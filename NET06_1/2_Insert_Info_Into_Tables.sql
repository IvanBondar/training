USE [NET06_1]
GO

INSERT INTO [dbo].[Resource] VALUES ('user1');
INSERT INTO [dbo].[Resource] VALUES ('user2');
INSERT INTO [dbo].[Resource] VALUES ('user3');
INSERT INTO [dbo].[Resource] VALUES ('user4');
INSERT INTO [dbo].[Resource] VALUES ('user5');
INSERT INTO [dbo].[Resource] VALUES ('user6');
INSERT INTO [dbo].[Resource] VALUES ('user7');
INSERT INTO [dbo].[Resource] VALUES ('user8');
INSERT INTO [dbo].[Resource] VALUES ('user9');
INSERT INTO [dbo].[Resource] VALUES ('user10');

GO

INSERT INTO [dbo].[Project] VALUES ('project1', '2017-01-10', '2018-02-10');
INSERT INTO [dbo].[Project] VALUES ('project2', '2017-02-09', '2018-03-09');
INSERT INTO [dbo].[Project] VALUES ('project3', '2017-03-08', '2018-04-08');
INSERT INTO [dbo].[Project] VALUES ('project4', '2017-04-07', '2018-05-07');
INSERT INTO [dbo].[Project] VALUES ('project5', '2017-05-06', '2018-06-06');
INSERT INTO [dbo].[Project] VALUES ('project6', '2017-06-05', '2018-07-05');
INSERT INTO [dbo].[Project] VALUES ('project7', '2017-07-04', '2018-08-04');
INSERT INTO [dbo].[Project] VALUES ('project8', '2017-08-03', '2018-09-03');
INSERT INTO [dbo].[Project] VALUES ('project9', '2017-09-02', '2018-10-02');
INSERT INTO [dbo].[Project] VALUES ('project10', '2017-10-01', '2018-11-01');

GO

INSERT INTO [dbo].[Task] VALUES ('task1', 10, null, 1);
INSERT INTO [dbo].[Task] VALUES ('task1.1', 10, 1, 1);
INSERT INTO [dbo].[Task] VALUES ('task1.2', 10, 1, 1);
INSERT INTO [dbo].[Task] VALUES ('task1.1.1', 10, 2, 1);
INSERT INTO [dbo].[Task] VALUES ('task1.1.2', 10, 2, 1);
INSERT INTO [dbo].[Task] VALUES ('task1.2.1', 10, 3, 1);
INSERT INTO [dbo].[Task] VALUES ('task1.2.2', 10, 3, 1);

INSERT INTO [dbo].[Task] VALUES ('task2', 0, null, 2);
INSERT INTO [dbo].[Task] VALUES ('task2.1', 10, 8, 2);
INSERT INTO [dbo].[Task] VALUES ('task2.2', 10, 8, 2);
INSERT INTO [dbo].[Task] VALUES ('task2.1.1', 10, 9, 2);
INSERT INTO [dbo].[Task] VALUES ('task2.1.2', 10, 9, 2);
INSERT INTO [dbo].[Task] VALUES ('task2.2.1', 10, 10, 2);
INSERT INTO [dbo].[Task] VALUES ('task2.2.2', 10, 10, 2);

INSERT INTO [dbo].[Task] VALUES ('task3', 10, null, 3);
INSERT INTO [dbo].[Task] VALUES ('task3.1', 0, 15, 3);
INSERT INTO [dbo].[Task] VALUES ('task3.2', 10, 15, 3);
INSERT INTO [dbo].[Task] VALUES ('task3.1.1', 10, 16, 3);
INSERT INTO [dbo].[Task] VALUES ('task3.1.2', 10, 16, 3);
INSERT INTO [dbo].[Task] VALUES ('task3.2.1', 10, 17, 3);
INSERT INTO [dbo].[Task] VALUES ('task3.2.2', 10, 17, 3);

INSERT INTO [dbo].[Task] VALUES ('task4', 10, null, 4);
INSERT INTO [dbo].[Task] VALUES ('task4.1', 10, 22, 4);
INSERT INTO [dbo].[Task] VALUES ('task4.2', 10, 22, 4);
INSERT INTO [dbo].[Task] VALUES ('task4.1.1', 10, 23, 4);
INSERT INTO [dbo].[Task] VALUES ('task4.1.2', 10, 23, 4);
INSERT INTO [dbo].[Task] VALUES ('task4.2.1', 10, 24, 4);
INSERT INTO [dbo].[Task] VALUES ('task4.2.2', 10, 24, 4);

INSERT INTO [dbo].[Task] VALUES ('task5', 10, null, 5);
INSERT INTO [dbo].[Task] VALUES ('task5.1', 10, 29, 5);
INSERT INTO [dbo].[Task] VALUES ('task5.2', 10, 29, 5);
INSERT INTO [dbo].[Task] VALUES ('task5.1.1', 10, 30, 5);
INSERT INTO [dbo].[Task] VALUES ('task5.1.2', 10, 30, 5);
INSERT INTO [dbo].[Task] VALUES ('task5.2.1', 10, 31, 5);
INSERT INTO [dbo].[Task] VALUES ('task5.2.2', 10, 31, 5);

INSERT INTO [dbo].[Task] VALUES ('task6', 10, null, 5);
INSERT INTO [dbo].[Task] VALUES ('task6.1', 10, 36, 5);
INSERT INTO [dbo].[Task] VALUES ('task6.2', 10, 36, 5);
INSERT INTO [dbo].[Task] VALUES ('task6.1.1', 10, 37, 5);
INSERT INTO [dbo].[Task] VALUES ('task6.1.2', 10, 37, 5);
INSERT INTO [dbo].[Task] VALUES ('task6.2.1', 10, 38, 5);
INSERT INTO [dbo].[Task] VALUES ('task6.2.2', 10, 38, 5);

INSERT INTO [dbo].[Task] VALUES ('task7', 0, null, 5);
INSERT INTO [dbo].[Task] VALUES ('task8', 100, null, 5);
INSERT INTO [dbo].[Task] VALUES ('task9', 0, null, 5);
INSERT INTO [dbo].[Task] VALUES ('task10', 100, null, 5);

GO

INSERT INTO [dbo].[TaskResource] VALUES (1, 1);
INSERT INTO [dbo].[TaskResource] VALUES (1, 2);
INSERT INTO [dbo].[TaskResource] VALUES (2, 1);
INSERT INTO [dbo].[TaskResource] VALUES (2, 2);
INSERT INTO [dbo].[TaskResource] VALUES (3, 3);
INSERT INTO [dbo].[TaskResource] VALUES (3, 4);
INSERT INTO [dbo].[TaskResource] VALUES (4, 4);
INSERT INTO [dbo].[TaskResource] VALUES (5, 5);
INSERT INTO [dbo].[TaskResource] VALUES (6, 6);
INSERT INTO [dbo].[TaskResource] VALUES (7, 7);
INSERT INTO [dbo].[TaskResource] VALUES (8, 8);
INSERT INTO [dbo].[TaskResource] VALUES (9, 9);
INSERT INTO [dbo].[TaskResource] VALUES (10, 10);

GO

INSERT INTO [dbo].[WorkTime] VALUES (1, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (1, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (2, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (2, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (3, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (4, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (5, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (6, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (8, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (9, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (10, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (11, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (12, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (13, 8, '2017-05-01');
INSERT INTO [dbo].[WorkTime] VALUES (13, 8, '2017-05-01');

GO











