USE NET06_1
GO

CREATE VIEW SpentTime
	AS SELECT r.Name, p.ProjectId, t.Name AS TaskName, st.SpentTime
	FROM (SELECT TaskId, ResourceId, SUM(TIME) AS SpentTime 
			FROM WorkTime AS wt
			JOIN TaskResource AS tr
			ON wt.TaskResourceId = tr.TaskResourceId
			GROUP BY TaskId, ResourceId) AS st
	JOIN Resource AS r
	ON st.ResourceId = r.ResourceId
	JOIN Task AS t
	ON (t.TaskId = st.TaskId) AND (t.Duration != 0)
	JOIN Project AS p
	ON p.ProjectId = st.ResourceId;
GO

