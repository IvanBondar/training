USE NET06_1
GO

SELECT tr.TaskId, SUM(wt.Time) AS SpentTime
FROM WorkTime AS wt
JOIN TaskResource AS tr
ON wt.TaskResourceId = tr.ResourceId
WHERE tr.TaskId IN(SELECT t.TaskId
					FROM Task AS t
					WHERE (t.ParentTaskId IS NULL) AND (t.Duration != 0))
GROUP BY tr.TaskId
