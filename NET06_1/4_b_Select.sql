USE NET06_1
GO

SELECT p.Name AS ProjectName
FROM Project AS p
WHERE p.ProjectId NOT IN ((SELECT DISTINCT t.ProjectId
							FROM Task AS t
							JOIN TaskResource AS tr
							ON t.TaskId = tr.TaskId
							JOIN WorkTime AS wt
							ON wt.TaskResourceId = tr.TaskResourceId))
