USE [NET06_1]
GO

/*Create Resource*/
IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'Resource')
BEGIN
CREATE TABLE [dbo].[Resource](
	[ResourceId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Resource] PRIMARY KEY CLUSTERED 
(
	[ResourceId] ASC
)) ON [PRIMARY]
END
GO

/*Create Resource*/

/*Create Project*/
CREATE TABLE [dbo].[Project](
	[ProjectId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[StartDate] [date] NOT NULL,
	[EndDate] [date] NOT NULL,
 CONSTRAINT [PK_Project] PRIMARY KEY CLUSTERED 
(
	[ProjectId] ASC
))
GO

ALTER TABLE [dbo].[Project]  WITH CHECK ADD  CONSTRAINT [CK_ProjectCheckDate] CHECK  (([EndDate]>[StartDate]))
GO

ALTER TABLE [dbo].[Project] CHECK CONSTRAINT [CK_ProjectCheckDate]
GO

/*Create Project*/

/*Create Task*/
CREATE TABLE [dbo].[Task](
	[TaskId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[Duration] [int] NOT NULL,
	[ParentTaskId] [int] NULL,
	[ProjectId] [int] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[TaskId] ASC
))

GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_ChildTask] FOREIGN KEY([ParentTaskId])
REFERENCES [dbo].[Task] ([TaskId])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_ChildTask]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD  CONSTRAINT [FK_Task_Project] FOREIGN KEY([ProjectId])
REFERENCES [dbo].[Project] ([ProjectId])
GO

ALTER TABLE [dbo].[Task] CHECK CONSTRAINT [FK_Task_Project]
GO

ALTER TABLE [dbo].[Task]  WITH CHECK ADD CHECK  (([Duration]>=(0)))
GO

/*Create Task*/

/*Create TaskResource*/

CREATE TABLE [dbo].[TaskResource](
	[TaskResourceId] [int] IDENTITY(1,1) NOT NULL,
	[TaskId] [int] NOT NULL,
	[ResourceId] [int] NOT NULL,
 CONSTRAINT [PK_TaskResourceId] PRIMARY KEY CLUSTERED 
(
	[TaskResourceId] ASC
),
CONSTRAINT [IX_TaskResource_UniqueResourceId_TaskId] UNIQUE NONCLUSTERED 
(
	[ResourceId] ASC, [TaskId] ASC
))
GO



ALTER TABLE [dbo].[TaskResource]  WITH CHECK ADD  CONSTRAINT [FK_TaskResource_Resource] FOREIGN KEY([ResourceId])
REFERENCES [dbo].[Resource] ([ResourceId])
GO

ALTER TABLE [dbo].[TaskResource] CHECK CONSTRAINT [FK_TaskResource_Resource]
GO

ALTER TABLE [dbo].[TaskResource]  WITH CHECK ADD  CONSTRAINT [FK_TaskResource_Task] FOREIGN KEY([TaskId])
REFERENCES [dbo].[Task] ([TaskId])
GO

ALTER TABLE [dbo].[TaskResource] CHECK CONSTRAINT [FK_TaskResource_Task]
GO

/*Create TaskResource*/

/*Create WorkTime*/

CREATE TABLE [dbo].[WorkTime](
	[WorkTimeId] [int] IDENTITY(1,1) NOT NULL,
	[TaskResourceId] [int] NOT NULL,
	[Time] [int] NOT NULL,
	[Date] [date] NOT NULL,
 CONSTRAINT [PK_WorkTime] PRIMARY KEY CLUSTERED 
(
	[WorkTimeId] ASC
))

GO

ALTER TABLE [dbo].[WorkTime]  WITH CHECK ADD  CONSTRAINT [FK_WorkTime_TaskResource] FOREIGN KEY([TaskResourceId])
REFERENCES [dbo].[TaskResource] ([TaskResourceId])
GO

ALTER TABLE [dbo].[WorkTime] CHECK CONSTRAINT [FK_WorkTime_TaskResource]
GO

ALTER TABLE [dbo].[WorkTime]  WITH CHECK ADD  CONSTRAINT [CK_WorkTimeIsPositive] CHECK  (([Time]>=(0)))
GO

ALTER TABLE [dbo].[WorkTime] CHECK CONSTRAINT [CK_WorkTimeIsPositive]
GO

/*Create WorkTime*/