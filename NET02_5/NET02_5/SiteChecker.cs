﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Configuration;
using Infrastructure.Helpers;
using NET02_5.Config;

namespace NET02_5
{
    /// <summary>
    /// Class for checking the site for availability
    /// </summary>
    public static class SiteChecker
    {
        private static readonly NLog.Logger _logger;

        static SiteChecker()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }
        /// <summary>
        /// Method starting the checking
        /// </summary>
        public static void StartCheckResponse()
        {
            var section = (MonitoringSection)ConfigurationManager.GetSection("monitoringSection");
            Check.NotNull(section, nameof(section));

            var siteSettings = section.SiteSettingsSection;
            var timers = new List<Timer>();

            for (int i = 0; i < siteSettings.Count; i++)
            {
                timers.Add(new Timer(CheckResponse, siteSettings[i], 2000, siteSettings[i].Interval));
            }
        }

        /// <summary>
        /// Method for checking response in setted Time
        /// </summary>
        /// <param name="settings">object type of SiteSettingsSection</param>
        private static async void CheckResponse(object settings)
        {
            var siteSettings = (SiteSettingsSection)settings;
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(siteSettings.Site);
                request.Timeout = siteSettings.MaxTimeOut;
                using (var response = (HttpWebResponse)request.GetResponse())
                { }

                _logger.Info($"{siteSettings.Site} available");
            }
            catch(WebException)
            {
                await EmailSenderAsync.SendEmailAsync(siteSettings.Site);
                Console.WriteLine(DateTime.Now.TimeOfDay.ToString() + siteSettings.Site);
            }
        }
    }
}

