﻿using System.Configuration;

namespace NET02_5.Config
{
    /// <summary>
    /// Class for represent adminEmailSettings in App.config
    /// </summary>
    public class AdminEmailSettingsSection : ConfigurationElement
    {
        /// <summary>
        /// Store for adminEmail
        /// </summary>
        [ConfigurationProperty("adminEmail", IsRequired = true)]
        public string AdminEmail => (string)(base["adminEmail"]);
        /// <summary>
        /// Store for adminEmailPassword
        /// </summary>
        [ConfigurationProperty("adminEmailPassword", IsRequired = true)]
        public string AdminEmailPassword => (string)(base["adminEmailPassword"]);
    }
}