﻿using System.Configuration;

namespace NET02_5.Config
{
    /// <summary>
    /// Class for represent smtpSettings in App.config
    /// </summary>
    public class SmtpSettingsSection : ConfigurationElement
    {
        /// <summary>
        /// Store for smtp server name
        /// </summary>
        [ConfigurationProperty("server", IsRequired = true)]
        public string Server => (string)(base["server"]);
        /// <summary>
        /// Store for smpt port
        /// </summary>
        [ConfigurationProperty("port", IsRequired = true)]
        public int Port => (int)(base["port"]);
        /// <summary>
        /// Store for enableSsl flag
        /// </summary>
        [ConfigurationProperty("enableSsl", IsRequired = true)]
        public bool EnableSsl => (bool)(base["enableSsl"]);
    }
}