﻿using System;
using System.Configuration;

namespace NET02_5.Config
{
    /// <summary>
    /// Class for represent collection of siteSettings element in App.config
    /// </summary>
    [ConfigurationCollection( typeof(SiteSettingsSection), AddItemName = "siteSettings")]
    public class SiteSettingsCollection : ConfigurationElementCollection
    {
        /// <summary>
        /// Indexer
        /// </summary>
        /// <param name="i">Element number</param>
        /// <returns>SiteSettingsSection</returns>
        public SiteSettingsSection this[int i] => (SiteSettingsSection)BaseGet(i);
        /// <summary>
        /// Return new SiteSettingsSection element 
        /// </summary>
        /// <returns></returns>
        protected override ConfigurationElement CreateNewElement() => new SiteSettingsSection();

        /// <summary>
        /// Return key of SiteSettingsSection
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        protected override object GetElementKey(ConfigurationElement element) => ((SiteSettingsSection)(element)).Site;

        
    }
}