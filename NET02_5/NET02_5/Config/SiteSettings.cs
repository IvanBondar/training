﻿using System.Configuration;

namespace NET02_5.Config
{
    /// <summary>
    /// Class for represent siteSettings section in App.config
    /// </summary>
    public class SiteSettingsSection : ConfigurationElement
    {
        /// <summary>
        /// Property for represent interval in siteSetting section
        /// </summary>
        [ConfigurationProperty("interval", IsRequired = true)]
        public int Interval => (int)(base["interval"]);
        /// <summary>
        /// Property for represent maxTimeOut in siteSetting section
        /// </summary>
        [ConfigurationProperty("maxTimeOut", IsRequired = true)]
        public int MaxTimeOut => (int)(base["maxTimeOut"]);
        /// <summary>
        /// Property for represent site in siteSetting section
        /// </summary>
        [ConfigurationProperty("site", IsRequired = true)]
        public string Site => (string)(base["site"]);
    }
}
