﻿using System.Configuration;

namespace NET02_5.Config
{
    /// <summary>
    /// Class for represent monitoringSection in App.config
    /// </summary>
    public class MonitoringSection : ConfigurationSection
    {
        /// <summary>
        /// Property for represent siteSettingsSection in App.config
        /// </summary>
        [ConfigurationProperty("siteSettingsSection")]
        public SiteSettingsCollection SiteSettingsSection => ((SiteSettingsCollection)(base["siteSettingsSection"]));
        /// <summary>
        /// Property for represent smtpSettings in App.config
        /// </summary>
        [ConfigurationProperty("smtpSettings")]
        public SmtpSettingsSection SmtpSettingsSectoin => ((SmtpSettingsSection)(base["smtpSettings"]));
        /// <summary>
        /// Property for represent adminEmailSettings in App.config
        /// </summary>
        [ConfigurationProperty("adminEmailSettings")]
        public AdminEmailSettingsSection AdminSettingsSection => ((AdminEmailSettingsSection)(base["adminEmailSettings"]));
    }
}
