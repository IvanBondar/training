﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Configuration;
using Infrastructure.Helpers;
using NET02_5.Config;

namespace NET02_5
{
    /// <summary>
    /// Class for async sending email 
    /// </summary>
    public static class EmailSenderAsync
    {

        /// <summary>
        /// Method async sending email 
        /// </summary>
        /// <param name="siteName">siteName</param>
        /// <returns></returns>
        public static async Task SendEmailAsync(string siteName)
        {
            var settings = (MonitoringSection)ConfigurationManager.GetSection("monitoringSection");
            var admin = settings.AdminSettingsSection;
            Check.NotNull(settings, nameof(settings));

            var smtpClient = new SmtpClient()
            {
                Credentials = new NetworkCredential(admin.AdminEmail, admin.AdminEmailPassword),
                Host = settings.SmtpSettingsSectoin.Server,
                Port = settings.SmtpSettingsSectoin.Port,
                EnableSsl = settings.SmtpSettingsSectoin.EnableSsl
            };

            var from = new MailAddress(admin.AdminEmail);
            var to = new MailAddress(admin.AdminEmail);
            MailMessage message = new MailMessage(from, to);
            message.Subject = "Site Checker";
            message.Body = $"Website {siteName} not responding";

            await smtpClient.SendMailAsync(message);
        }
    }
}
