﻿using System;
using System.Threading;

namespace NET02_5
{
    class Program
    {
        /// <summary>
        /// Entry point of application
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            using (var mutex = new Mutex(false, "NET02_5"))
            {
                if (!mutex.WaitOne(TimeSpan.FromSeconds(3), false))
                {
                    Console.WriteLine("Another instance is running");
                    return;
                }

                SiteChecker.StartCheckResponse();
                Console.WriteLine("Press any key to stop monitoring");
                Console.ReadLine();
            }
        }
    }
}
