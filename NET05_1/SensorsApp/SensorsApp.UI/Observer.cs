﻿using SensorsApp.DL.Model.Abstraction;

namespace SensorsApp.UI
{
    /// <summary>
    /// Represent observer class for IObserver instance
    /// </summary>
    public class Observer : IObserver
    {
        /// <summary>
        /// Main window link
        /// </summary>
        private readonly MainWindow _mainWindow;
        /// <summary>
        /// Store for observable instance
        /// </summary>
        private IObservable _observable;

        /// <summary>
        /// Observer constructor 
        /// </summary>
        /// <param name="observable">observable</param>
        /// <param name="mainWindow">main window</param>
        public Observer(IObservable observable, MainWindow mainWindow)
        {
            observable.RegisterObserver(this);
            _observable = observable;
            _mainWindow = mainWindow;
        }

        /// <summary>
        /// Method for update prop CurrentMeasuredValue in Main Window, when current observable instance was changed
        /// </summary>
        /// <param name="measuredValue">measured value</param>
        public void Update(int measuredValue)
        {
            _mainWindow.CurrentMeasuredValue = measuredValue;
        }

        /// <summary>
        /// Remove observer from list in observable instance
        /// </summary>
        public void RemoveObserver()
        {
            _mainWindow.CurrentMeasuredValue = 0;
            _observable.RemoveObserver(this);
            _observable = null;     
        }
    }
}
