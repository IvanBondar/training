﻿using SensorsApp.BLL.Manager;
using SensorsApp.DL.Model.Sensors;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace SensorsApp.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private Observer _observer;
        private string _repositoryName;
        private int _currentMeasuredValue = 0;
        private Sensor _currentSensor;

        public ObservableCollection<Sensor> Sensors { get; set; }

        public string RepositoryName
        {
            get
            {
                return _repositoryName;
            }
            set
            {
                _repositoryName = value;
                OnPropertyChanged(nameof(RepositoryName));
            }
        }

        public int CurrentMeasuredValue
        {
            get
            {
                return _currentMeasuredValue;
            }
            set
            {
                _currentMeasuredValue = value;
                OnPropertyChanged(nameof(CurrentMeasuredValue));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            RepositoryName = SensorManager.GetCurrentRepositoryName();
            button_AddSensor.IsEnabled = false;
            button_RemoveSelectedSensor.IsEnabled = false;
            button_SwitchNextState.IsEnabled = false;
            textBlock_MeasuredValue.IsReadOnly = true;
        }

        private void button_GetAllSensors_Click(object sender, RoutedEventArgs e)
        {
            Sensors = new ObservableCollection<Sensor>(SensorManager.GetAll());
            listBox_Sensors.ItemsSource = Sensors;
            button_GetAllSensors.IsEnabled = false;
            button_AddSensor.IsEnabled = true;
        }

        private void listBox_Sensors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _currentSensor =(Sensor)((ListBox)sender).SelectedItem;

            _observer?.RemoveObserver();

            if (_currentSensor != null)
            {
                _observer = new Observer(_currentSensor, this);
            }

            button_RemoveSelectedSensor.IsEnabled = true;
            button_SwitchNextState.IsEnabled = true;
        }


        private void button_RemoveSelectedSensor_Click(object sender, RoutedEventArgs e)
        {
            if (_observer != null)
            {
                _observer.RemoveObserver();
                _observer = null;
            }

            SensorManager.Remove(_currentSensor);
            Sensors.Remove(_currentSensor);
            button_RemoveSelectedSensor.IsEnabled = false;

            if (Sensors.Count == 0)
            {
                button_SwitchNextState.IsEnabled = false;
            }
        }

        private void button_SwitchNextState_Click(object sender, RoutedEventArgs e)
        {
            _currentSensor.SwitchNextState();
        }

        private void button_AddSensor_Click(object sender, RoutedEventArgs e)
        {
            var dialogWindow = new DialogWindow();

            if (dialogWindow.ShowDialog() == false)
            {
                return;
            }
            var sensor = SensorManager.CreateSensor(dialogWindow.Interval, dialogWindow.SensorType);

            if (sensor == null)
            {
                // show window - sensor not created
                return;
            }

            Sensors.Add(sensor);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
