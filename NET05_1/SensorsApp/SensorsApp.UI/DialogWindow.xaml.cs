﻿using SensorsApp.DL.Model.Sensors;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace SensorsApp.UI
{
    /// <summary>
    /// Interaction logic for DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : Window, INotifyPropertyChanged
    {
        private SensorTypes _sensorType;
        private int _interval = 1000;

        public int Interval
        {
            get
            {
                return _interval;
            }
            set
            {
                _interval = value;
                OnPropertyChanged(nameof(Interval));
            }
        }

        public SensorTypes SensorType
        {
            get
            {
                return _sensorType;
            }
            set
            {
                _sensorType = value;
                OnPropertyChanged(nameof(SensorType));
            }
        }
        
        public DialogWindow()
        {
            InitializeComponent();
            comboBox_SensorTypeSelector.ItemsSource = Enum.GetValues(typeof(SensorTypes));
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void comboBox_SensorTypeSelector_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _sensorType = (SensorTypes)((ComboBox)sender).SelectedItem;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
