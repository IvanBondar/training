﻿using SensorsApp.DL.Model.Sensors;
using SensorsApp.DL.Repository;
using SensorsApp.DL.Repository.Factory;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace SensorsApp.BLL.Services
{
    /// <summary>
    /// Static class for work with sensor repository on UI layer
    /// </summary>
    public static class SensorManager
    {
        /// <summary>
        /// Store for repository
        /// </summary>
        private static readonly IRepository _repository;
        private static readonly IRepository _repositoryTest;

        /// <summary>
        /// Name of current repository
        /// </summary>
        private static readonly string _repositoryName;

        /// <summary>
        /// Constructor with check to target repository from App.config
        /// </summary>
        static SensorManager()
        {
            var repository = ConfigurationManager.AppSettings["repository"];
            var source = ConfigurationManager.AppSettings["source"];

            if (repository != "XmlRepository" && repository != "JsonRepository")
            {
                throw new ArgumentException("Wrong repository name", nameof(repository));
            }

            if (repository == "XmlRepository")
            {
                // => reflection
               _repository = new XmlRepositoryFactory().GetRepository(source);
                  // var _repository = _factory.GetRepository(source);
                _repositoryName = repository;
                return;
            }
            
            _repository = new JsonRepositoryFactory().GetRepository(source);
            _repositoryName = repository;
        }

        private static IRepository GetRepository()
        {
            //var repository = ConfigurationManager.AppSettings["XmlRepository"];
            //var source = ConfigurationManager.AppSettings["source"];
            //var assembly = Assembly.Load(new AssemblyName())

            return null;
        }

        /// <summary>
        /// Return current repository name
        /// </summary>
        /// <returns>repository name</returns>
        public static string GetCurrentRepositoryName() => _repositoryName;

        /// <summary>
        /// Get all sensors from repository
        /// </summary>
        /// <returns>sensors</returns>
        public static IEnumerable<Sensor> GetAll() => _repository.GetAll();

        /// <summary>
        /// Add sensor to reposiitory
        /// </summary>
        /// <param name="sensor">sensor</param>
        public static void Add(Sensor sensor)
        {
            _repository.Add(sensor);
        }

        /// <summary>
        /// Remove sensor from repository
        /// </summary>
        /// <param name="sensor">sensor</param>
        /// <returns>true if instance added to repository, else false</returns>
        public static bool Remove(Sensor sensor) => _repository.Remove(sensor);

        /// <summary>
        /// Method for create new sensor
        /// </summary>
        /// <param name="interval">interval</param>
        /// <param name="type">sensor type</param>
        /// <returns></returns>
        public static Sensor CreateSensor(int interval, SensorTypes type)
        {
            if (interval <= 0)
            {
                return null;
            }

            var sensor = new Sensor
            {
                Interval = interval,
                Type = type,
                Id = GuidGenerator.GetInstance().GetGuid()
            };

            Add(sensor);

            return sensor;
        }
    }
}
