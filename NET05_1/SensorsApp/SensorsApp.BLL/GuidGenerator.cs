﻿using System;

namespace SensorsApp.BLL
{
    /// <summary>
    /// Class for generate unique Id
    /// </summary>
    public class GuidGenerator
    {
        private static readonly GuidGenerator _instance = new GuidGenerator();
   
        static GuidGenerator()
        { }
    
        private GuidGenerator()
        { }

        public static GuidGenerator GetInstance() => _instance;

        public Guid GetGuid() => Guid.NewGuid();
    }
}
