﻿using SensorsApp.DL.Model.Sensors;
using SensorsApp.DL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Reflection;
using NLog;
using SensorsApp.DL.RepositorySectionConfig;

namespace SensorsApp.BLL.Manager
{
    /// <summary>
    /// Static class for work with sensor repository on UI layer
    /// </summary>
    public static class SensorManager
    {
        /// <summary>
        /// Store for repository
        /// </summary>
        private static readonly IRepository _repository;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly RepositorySettings _repositorySettings;

        /// <summary>
        /// Constructor with check to target repository from App.config
        /// </summary>
        static SensorManager()
        {
            _repositorySettings = ((RepositorySection)ConfigurationManager
                .GetSection("repositorySection")).RepositorySettings;
            var source = _repositorySettings.Source;
            var factory = GetRepositoryFactory();

            if (factory == null)
            {
                _logger.Error("Factory wasn't created");
                throw new NullReferenceException("Factory wasn't created");
            }

            _repository = factory.GetRepository(source);
        }

        /// <summary>
        /// Method for create RepositoryFactory instance through reflection
        /// </summary>
        /// <returns>repository factory instance</returns>
        private static RepositoryFactory GetRepositoryFactory()
        {
            var assemblyName = _repositorySettings.Assembly;
            var repositoryName = _repositorySettings.RepositoryName;

            if (!File.Exists($"{assemblyName}.dll"))
            {
                _logger.Warn($"There isn't {assemblyName}.dll in current directory.");

                return null;
            }

            var assembly = Assembly.Load(new AssemblyName(assemblyName));
            var factoryType = $"{assemblyName}.{repositoryName}Factory";
            var type = assembly.GetType(factoryType);

            if (type == null)
            {
                _logger.Warn($"There isn't factory type in {assemblyName}");

                return null;
            }

            if (!type.BaseType.ToString().Contains("RepositoryFactory"))
            {
                _logger.Warn($"{assemblyName} does not implement RepositoryFactory.");

                return null;
            }

            return (RepositoryFactory)Activator.CreateInstance(type);
        }

        /// <summary>
        /// Return current repository name
        /// </summary>
        /// <returns>repository name</returns>
        public static string GetCurrentRepositoryName() => _repositorySettings.RepositoryName;

        /// <summary>
        /// Get all sensors from repository
        /// </summary>
        /// <returns>sensors</returns>
        public static IEnumerable<Sensor> GetAll() => _repository.GetAll();

        /// <summary>
        /// Add sensor to reposiitory
        /// </summary>
        /// <param name="sensor">sensor</param>
        public static void Add(Sensor sensor)
        {
            _repository.Add(sensor);
        }

        /// <summary>
        /// Remove sensor from repository
        /// </summary>
        /// <param name="sensor">sensor</param>
        /// <returns>true if instance added to repository, else false</returns>
        public static bool Remove(Sensor sensor) => _repository.Remove(sensor);

        /// <summary>
        /// Method for create new sensor
        /// </summary>
        /// <param name="interval">interval</param>
        /// <param name="type">sensor type</param>
        /// <returns></returns>
        public static Sensor CreateSensor(int interval, SensorTypes type)
        {
            if (interval <= 0)
            {
                return null;
            }

            var sensor = new Sensor
            {
                Interval = interval,
                Type = type,
                Id = GuidGenerator.GetInstance().GetGuid()
            };

            Add(sensor);

            return sensor;
        }
    }
}
