﻿namespace SensorsApp.DL.Model.Sensors
{
    /// <summary>
    /// Sensor types
    /// </summary>
    public enum SensorTypes
    {
        Level,
        Flow
    }
}
