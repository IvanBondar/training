﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Newtonsoft.Json;
using SensorsApp.DL.Model.Abstraction;
using SensorsApp.DL.Model.SensorStates;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace SensorsApp.DL.Model.Sensors
{
    /// <summary>
    /// Represent sensor
    /// </summary>
    public class Sensor : INotifyPropertyChanged, IObservable, IDisposable
    {
        /// <summary>
        /// Start time for timer
        /// </summary>
        public const int DueTime = 0;
        /// <summary>
        /// Calibration interval for sensor
        /// </summary>
        public const int СalibrationInterval = 1000;
        
        /// <summary>
        /// Store for measured value
        /// </summary>
        private int _measuredValue;
        /// <summary>
        /// Observers list
        /// </summary>
        private readonly List<IObserver> _observers = new List<IObserver>();

        /// <summary>
        ///Event for register of changing measured value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Sensor ID
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Sensor measure interval
        /// </summary>
        public int Interval { get; set; }
        /// <summary>
        /// Sensor type
        /// </summary>
        public SensorTypes Type { get; set; }
        /// <summary>
        /// Min measured value, used in WorkState 
        /// </summary>
        public int MinMeasuredValue { get; set; } = 0;
        /// <summary>
        /// Max measured value, used in WorkState 
        /// </summary>
        public int MaxMeasuredValue { get; set; } = 1000;

        /// <summary>
        /// Property for measured value
        /// </summary>
        [XmlIgnore][JsonIgnore]
        public int MeasuredValue {
            get
            {
                return _measuredValue;
            }
            set
            {
                _measuredValue = value;
                OnPropertyChanged(nameof(MeasuredValue));
                NotifyObservers();
            } 
        }
        
        /// <summary>
        /// Property for Sensor State
        /// </summary>
        [JsonIgnore][XmlIgnore]
        public ISensorState State { get; set; } = InitialState.GetInstance();

        /// <summary>
        /// Method for invoke on propery changed
        /// </summary>
        /// <param name="prop"></param>
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        /// <summary>
        /// Switch next state
        /// </summary>
        public void SwitchNextState()
        {
            State.NextState(this);
        }

        public void Dispose()
        {
            State.StopMeasuring();
        }

        /// <summary>
        /// Register observer method
        /// </summary>
        /// <param name="observer">observer</param>
        public void RegisterObserver(IObserver observer)
        {
            _observers.Add(observer);
        }

        /// <summary>
        /// Remove observer method
        /// </summary>
        /// <param name="observer">observer</param>
        public void RemoveObserver(IObserver observer)
        {
            _observers.Remove(observer);
        }

        /// <summary>
        /// Method for notify observers
        /// </summary>
        public void NotifyObservers()
        {
            _observers.ForEach(observer => observer.Update(MeasuredValue));
        }
    }
}
