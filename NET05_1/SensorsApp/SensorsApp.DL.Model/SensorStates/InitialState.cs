﻿using SensorsApp.DL.Model.Abstraction;
using SensorsApp.DL.Model.Sensors;

namespace SensorsApp.DL.Model.SensorStates
{
    /// <summary>
    /// Represent initial state for sensor
    /// </summary>
    public class InitialState : ISensorState
    {
        private static readonly InitialState _instance = new InitialState();

        static InitialState()
        { }

        private InitialState()
        { }

        public static InitialState GetInstance() => _instance;

        /// <summary>
        /// Method for changing MeasuredValue of sensors
        /// </summary>
        /// <param name="sensor">sensor</param>
        public void Measure(Sensor sensor)
        {
            sensor.MeasuredValue = 0;
        }

        public void NextState(Sensor sensor)
        {
            sensor.State = CalibrationState.GetInstance();
            sensor.State.Measure(sensor);
        }

        public void StopMeasuring()
        { }
    }
}
