﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SensorsApp.DL.Model.Abstraction;
using SensorsApp.DL.Model.Sensors;

namespace SensorsApp.DL.Model.SensorStates
{
    public class СalibrationState : ISensorState
    {
        public void Measure(Sensor sensor)
        {
            sensor.MeasuredValue++;
            Console.WriteLine(sensor.MeasuredValue);
        }
    }
}
