﻿using SensorsApp.DL.Model.Abstraction;
using SensorsApp.DL.Model.Sensors;
using SensorsApp.Infrastructure.Helpers;
using System.Threading;

namespace SensorsApp.DL.Model.SensorStates
{
    /// <summary>
    /// Represent WorkState for sensor
    /// </summary>
    public class WorkState : ISensorState
    {
        private static readonly WorkState _instance = new WorkState();
        /// <summary>
        /// Timer for start Measure method with seted interval
        /// </summary>
        private static Timer _timer;

        static WorkState()
        { }

        private WorkState()
        { }

        public static WorkState GetInstance() => _instance;
        /// <summary>
        /// Method for changing MeasuredValue of sensors
        /// </summary>
        /// <param name="sensor">sensor</param>
        public void Measure(Sensor sensor)
        {
            _timer = new Timer((obj) =>
            {
                ((Sensor)obj).MeasuredValue = StaticRandom.Instance.Next(sensor.MinMeasuredValue, sensor.MaxMeasuredValue);
            }
            , sensor, Sensor.DueTime, sensor.Interval);
        }

        public void NextState(Sensor sensor)
        {
            _timer.Dispose();
            sensor.State = InitialState.GetInstance();
            sensor.State.Measure(sensor);
        }

        public void StopMeasuring()
        {
            _timer?.Dispose();
        }
    }

}
