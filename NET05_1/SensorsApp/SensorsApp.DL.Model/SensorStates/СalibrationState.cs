﻿using SensorsApp.DL.Model.Abstraction;
using SensorsApp.DL.Model.Sensors;
using System.Threading;

namespace SensorsApp.DL.Model.SensorStates
{
    /// <summary>
    /// Represent Calibration state for sensor
    /// </summary>
    public class CalibrationState : ISensorState
    {
        private static readonly CalibrationState _instance = new CalibrationState();
        /// <summary>
        /// Timer for start Measure method with seted interval
        /// </summary>
        private static Timer _timer;

        static CalibrationState()
        { }

        private CalibrationState()
        { }

        public static CalibrationState GetInstance() => _instance;

        /// <summary>
        /// Method for changing MeasuredValue of sensors
        /// </summary>
        /// <param name="sensor">sensor</param>
        public void Measure(Sensor sensor)
        {
            _timer = new Timer((obj) =>
            {
                var sensor_ = obj as Sensor;

                if (sensor_.MaxMeasuredValue == sensor_.MeasuredValue)
                {
                    sensor_.MeasuredValue = 0;

                    return;
                }

                sensor_.MeasuredValue++;
            }
            , sensor, Sensor.DueTime, Sensor.СalibrationInterval);
        }

        public void NextState(Sensor sensor)
        {
            _timer.Dispose();
            sensor.State = WorkState.GetInstance();
            sensor.State.Measure(sensor);
        }

        public void StopMeasuring()
        {
            _timer?.Dispose();
        }
    }
}
