﻿namespace SensorsApp.DL.Model.Abstraction
{
    /// <summary>
    /// Represent interface for observable class
    /// </summary>
    public interface IObservable
    {
        void RegisterObserver(IObserver observer);
        void RemoveObserver(IObserver observer);
        void NotifyObservers();
    }
}
