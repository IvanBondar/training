﻿namespace SensorsApp.DL.Model.Abstraction
{
    /// <summary>
    /// Represent interface for observer class
    /// </summary>
    public interface IObserver
    {
        void Update(int measuredValue);
    }
}
