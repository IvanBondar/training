﻿using SensorsApp.DL.Model.Sensors;

namespace SensorsApp.DL.Model.Abstraction
{
    /// <summary>
    /// Interface for sensor state
    /// </summary>
    public interface ISensorState
    {
        void Measure(Sensor sensor);
        void NextState(Sensor sensor);
        void StopMeasuring();
    }
}
