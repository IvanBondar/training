﻿using System;
using System.Threading;

namespace SensorsApp.Infrastructure.Helpers
{
    /// <summary>
    /// Class for generate random value;
    /// </summary>
    public static class StaticRandom
    {
        private static int _seed;

        private static readonly ThreadLocal<Random> _threadLocal = new ThreadLocal<Random>
            (() => new Random(Interlocked.Increment(ref _seed)));

        static StaticRandom()
        {
            _seed = Environment.TickCount;
        }

        public static Random Instance => _threadLocal.Value;
    }
}
