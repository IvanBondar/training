﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.DL.Model.Sensors;
using SensorsApp.DL.Model.SensorStates;

namespace SensorsApp.DL.Model.Tests
{
    [TestClass]
    public class SensorTests
    {
        [TestMethod]
        public void SwitchState_StateSwitched()
        {
            //arrange
            var sensor = new Sensor { Interval = 500, Type = SensorTypes.Flow };
            var currentState = sensor.State;

            //act
            sensor.SwitchNextState();
            var resultState = sensor.State;

            //assert
            Assert.IsTrue(currentState != resultState);
        }

        [TestMethod]
        public void SwitchState_StateSwitchingCycles()
        {
            //arrange
            var sensor = new Sensor { Interval = 500, Type = SensorTypes.Flow};
            var startState = sensor.State;

            //act
            sensor.SwitchNextState();
            var calibrationState = sensor.State;
            sensor.SwitchNextState();
            var workState = sensor.State;
            sensor.SwitchNextState();
            var finalState = sensor.State;

            //assert
            Assert.IsTrue(startState is InitialState);
            Assert.IsTrue(calibrationState is CalibrationState);
            Assert.IsTrue(workState is WorkState);
            Assert.IsTrue(startState == finalState);
        }
    }
}
