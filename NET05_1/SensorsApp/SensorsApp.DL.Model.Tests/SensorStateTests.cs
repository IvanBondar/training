﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SensorsApp.DL.Model.Sensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SensorsApp.DL.Model.Tests
{
    [TestClass]
    public class SensorStateTests
    {
        [TestMethod]
        public void InitialState_MeasuredValueIsZero()
        {
            //arrange 
            var sensor = new Sensor { Interval = 500, Type = SensorTypes.Flow };

            //act
            Thread.Sleep(2000);

            //assert
            Assert.AreEqual(0, sensor.MeasuredValue);
        }

        [TestMethod]
        public void CalibrationState_MeasuredValueChangedThreeTimes()
        {
            //arrange 
            var sensor = new Sensor { Interval = 500, Type = SensorTypes.Flow };

            //act
            sensor.SwitchNextState();
            Thread.Sleep(3000);

            //assert
            Assert.AreEqual(3, sensor.MeasuredValue);
        }

        [TestMethod]
        public void WorkState_MeasuredValueChangedRandomly()
        {
            //arrange 
            var sensor = new Sensor { Interval = 500, Type = SensorTypes.Flow };
            sensor.SwitchNextState();
            sensor.SwitchNextState();

            //act
            Thread.Sleep(100);
            var firstValue = sensor.MeasuredValue;
            Thread.Sleep(700);
            var secondValue = sensor.MeasuredValue;

            //assert
            Assert.AreNotEqual(firstValue, secondValue);
        }



    }
}
