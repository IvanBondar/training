﻿using SensorsApp.DL.Model.Sensors;
using SensorsApp.Infrastructure.Helpers;
using System.Collections.Generic;

namespace SensorsApp.DL
{ 
    public abstract class Repository : IRepository
    {
        /// <summary>
        /// Store for sensors
        /// </summary>
        protected List<Sensor> _sensors;
        /// <summary>
        /// Source  file
        /// </summary>
        protected string _source;

        /// <summary>
        /// Return all sensors from repository
        /// </summary>
        /// <returns>sensors</returns>
        public IEnumerable<Sensor> GetAll() => _sensors;

        /// <summary>
        /// Add sensor to repository
        /// </summary>
        /// <param name="sensor">sensor</param>
        public void Add(Sensor sensor)
        {
            _sensors.Add(sensor);
            Save();
        }

        /// <summary>
        /// Remove sensor from repository
        /// </summary>
        /// <param name="sensor">sensor</param>
        /// <returns>true if sensor removed, else - false</returns>
        public bool Remove(Sensor sensor)
        {
            Check.NotNull(sensor, nameof(sensor));

            sensor.Dispose();
            var result = _sensors.Remove(sensor);

            if (!result)
            {
                return false;
            }

            Save();

            return true;
        }

        public abstract void Save();
    }
}
