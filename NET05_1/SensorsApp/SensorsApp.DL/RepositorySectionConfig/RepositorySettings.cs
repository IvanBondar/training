﻿using System.Configuration;

namespace SensorsApp.DL.RepositorySectionConfig
{

    public class RepositorySettings : ConfigurationElement
    {
        [ConfigurationProperty("repositoryName", IsRequired = true)]
        public string RepositoryName => (string)(base["repositoryName"]);

        [ConfigurationProperty("assembly", IsRequired = true)]
        public string Assembly => (string)(base["assembly"]);

        [ConfigurationProperty("source", IsRequired = true)]
        public string Source => (string)(base["source"]);
    }
}
