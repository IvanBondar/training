﻿using System.Configuration;

namespace SensorsApp.DL.RepositorySectionConfig
{
    public class RepositorySection : ConfigurationSection
    {
        [ConfigurationProperty("repositorySettings")]
        public RepositorySettings RepositorySettings => ((RepositorySettings)(base["repositorySettings"]));
    }
}
