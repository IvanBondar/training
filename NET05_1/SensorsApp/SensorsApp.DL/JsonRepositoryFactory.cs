﻿namespace SensorsApp.DL
{
    /// <summary>
    /// Json Repository Factory
    /// </summary>
    public class JsonRepositoryFactory : RepositoryFactory
    {
        /// <summary>
        /// Return new instance of JsonRepository
        /// </summary>
        /// <param name="source">source to json file</param>
        /// <returns></returns>
        public override IRepository GetRepository(string source) => new JsonRepository(source);
    }
}
