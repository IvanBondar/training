﻿namespace SensorsApp.DL
{
    /// <summary>
    /// Abstract Repository Factory
    /// </summary>
    public abstract class RepositoryFactory
    {
        public abstract IRepository GetRepository(string source);
    }
}