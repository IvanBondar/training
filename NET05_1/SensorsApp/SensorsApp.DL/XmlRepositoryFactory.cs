﻿namespace SensorsApp.DL
{
    /// <summary>
    /// Xml Repository Factory
    /// </summary>
    public class XmlRepositoryFactory : RepositoryFactory
    {
        /// <summary>
        /// Return new instance of XmlRepository
        /// </summary>
        /// <param name="source">source to xml file</param>
        /// <returns></returns>
        public override IRepository GetRepository(string source) => new XmlRepository(source);
    }
}
