﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using SensorsApp.DL.Model.Sensors;
using SensorsApp.Infrastructure.Helpers;

namespace SensorsApp.DL
{
    /// <summary>
    /// Represent Xml Repository
    /// </summary>
    public class XmlRepository : Repository
    {
        /// <summary>
        /// Constructor for Xml Repository
        /// </summary>
        /// <param name="source">source</param>
        public XmlRepository(string source)
        {
            Check.NotNull(source, nameof(source));

            if (!File.Exists(source))
            {
                throw new FileNotFoundException("File not found", source);
            }

            _source = source;

            var formatter = new XmlSerializer(typeof(List<Sensor>));

            using (var fs = new FileStream(_source, FileMode.OpenOrCreate))
            {
                _sensors = (List<Sensor>)formatter.Deserialize(fs);
            }
        }
        
        /// <summary>
        /// Update repository
        /// </summary>
        public override void Save()
        {
            var formatter = new XmlSerializer(typeof(List<Sensor>));

            using (var fs = new FileStream(_source, FileMode.Create))
            {
                formatter.Serialize(fs, _sensors);
            }
        }
    }
}
