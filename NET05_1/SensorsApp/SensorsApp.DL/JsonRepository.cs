﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using SensorsApp.DL.Model.Sensors;
using SensorsApp.Infrastructure.Helpers;

namespace SensorsApp.DL
{
    /// <summary>
    /// Represent json repository
    /// </summary>
    public class JsonRepository : Repository
    {
        /// <summary>
        /// Constructor for repository
        /// </summary>
        /// <param name="source">source</param>
        public JsonRepository(string source)
        {
            Check.NotNull(source, nameof(source));

            if (!File.Exists(source))
            {
                throw new FileNotFoundException("File not found", source);
            }

            _source = source;
            var input = File.ReadAllLines(_source).FirstOrDefault();
            _sensors = JsonConvert.DeserializeObject<List<Sensor>>(input);
        }

        /// <summary>
        /// Update repository
        /// </summary>
        public override void Save() 
        {
            var jsonSerializeObject = JsonConvert.SerializeObject(_sensors);
            File.WriteAllText(_source, jsonSerializeObject);
        }
    }
}
