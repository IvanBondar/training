﻿using System.Collections.Generic;
using SensorsApp.DL.Model.Sensors;

namespace SensorsApp.DL
{
    /// <summary>
    /// Interface for repositories
    /// </summary>
    public interface IRepository
    {
        IEnumerable<Sensor> GetAll();
        void Add(Sensor sensor);
        bool Remove(Sensor sensor);
        void Save();
    }
}
