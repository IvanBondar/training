﻿using Infrastructure.Helpers;
using NET02_4.Abstactions;
using NET02_4.Infrastructure;
using NET02_4.Logger;
using System;
using System.Collections.Generic;
using System.IO;

namespace TextListener
{
    public class Listener : IListener
    {
        private static readonly InternalLogger _logger;

        public string Path { get; set; }

        static Listener()
        {
            _logger = new InternalLogger();
        }

        /// <summary>
        /// Method that set settings of instance;
        /// </summary>
        /// <param name="parameters">parameters for setup</param>
        /// <exception cref="ArgumentNullException">Throw when parameters is null</exception>
        /// <exception cref="ArgumentException">Throw when parameters does not contain required key</exception>
        public void Setup(Dictionary<string, string> parameters)
        {
            Check.NotNull(parameters, nameof(parameters));

            if (!parameters.ContainsKey("path"))
            {
                throw new ArgumentException(@"Parameters don't have ""path"" key", @"parameters[""path""]");
            }

            Check.NotNullOrWhiteSpace(parameters["path"], @"parameters[""path""]");

            Path = parameters["path"];
        }

        public void Write(string message, LogLevel logLevel)
        {
            WriteMessage(message, logLevel);
        }

        public void Track(string str)
        {
            WriteMessage(str, LogLevel.Info);
        }

        /// <summary>
        /// Method generate full path
        /// </summary>
        /// <param name="path">path</param>
        /// <param name="logLevel">log level</param>
        /// <returns></returns>
        private string GetFullPath(string path)
            => System.IO.Path.Combine(path, $"{DateTime.Today.Date.ToString("ddmmyyyy")}log.txt");


        /// <summary>
        /// Write message to a file 
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="logLevel">log level</param>
        private void WriteMessage(string message, LogLevel logLevel)
        {
            try
            {
                using (StreamWriter sw = File.AppendText(GetFullPath(Path)))
                {
                    sw.WriteLine($"{DateTime.Now} {logLevel} {message}");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Something went wrong");
            }
        }
    }
}
