﻿using System;

namespace NET02_4.Attributes
{
    /// <summary>
    /// Represent Attribute for Tracking property or field
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class TrackingPropertyAttribute : Attribute
    {
        /// <summary>
        /// Attribute name
        /// </summary>
        public string Name { get; set; }

        public TrackingPropertyAttribute()
        { }
    }
}
