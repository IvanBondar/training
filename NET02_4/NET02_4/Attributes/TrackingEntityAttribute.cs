﻿using System;

namespace NET02_4.Attributes
{
    /// <summary>
    /// Represent Attribute for Tracking Entity
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TrackingEntityAttribute : Attribute
    {
    }
}
