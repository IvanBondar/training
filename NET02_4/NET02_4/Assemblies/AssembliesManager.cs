﻿using Infrastructure.Helpers;
using NET02_4.Abstactions;
using NET02_4.Infrastructure;
using NET02_4.LoggerSectionConfig;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using NET02_4.LoggerSectionConfig.Settings;

namespace NET02_4.Assemblies
{
    /// <summary>
    /// Class for work with assemblies;
    /// </summary>
    static class AssembliesManager
    {
        private static readonly InternalLogger _logger;
        /// <summary>
        /// Store for listeners.
        /// </summary>
        private static readonly List<IListener> _listeners;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static AssembliesManager()
        {
            _logger = new InternalLogger();
            _listeners = CreateInstances();
        }

        public static IReadOnlyList<IListener> GetListeners() => _listeners.AsReadOnly();

        /// <summary>
        /// Method that creates instances of assemblies
        /// </summary>
        /// <returns>setted listeners</returns>
        private static List<IListener> CreateInstances()
        {
            var assembliesData = ConfigReader.GetLoggerSettings();
            var listeners = new List<IListener>();
            try
            {
                Check.NotNull(assembliesData, nameof(assembliesData));

                foreach (var listenerSettings in assembliesData.ListenersSettings)
                {
                    var tmp = CreateListener(listenerSettings);

                    if (tmp == null)
                    {
                        continue;
                    }

                    listeners.Add(tmp);
                }
            }
            catch (ArgumentNullException ex)
            {
                _logger.Error(ex, nameof(assembliesData));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Something went wrong");
            }

            return listeners;
        }

        private static IListener CreateListener(ListenerSettings listenerSettings)
        {
            if (!File.Exists($"{listenerSettings.AssemblyName}.dll"))
            {
                _logger.Warn($@"Assembly name: {listenerSettings.AssemblyName}. 
                                There isn't assembly in current directory.");
                return null;
            }

            var assembly = Assembly.Load(new AssemblyName(listenerSettings.AssemblyName));
            var type = assembly.GetType(listenerSettings.Type);

            if (type == null)
            {
                _logger.Warn($@"Assembly name: {listenerSettings.AssemblyName}. 
                                There isn't constructor type with name Listener");
                return null;
            }

            var ilistenerType = type.GetInterface("IListener");

            if (ilistenerType == null)
            {
                _logger.Warn($@"Assembly name: {listenerSettings.AssemblyName}. 
                                Assembly does not implement interface IListener.");
                return null;
            }

            var listener = (IListener)Activator.CreateInstance(type);
            try
            {
                listener.Setup(listenerSettings.Settings);
            }
            catch (ArgumentNullException ex)
            {
                _logger.Error(ex, $"{listenerSettings.Type}.Setup() method threw exception");

                return null;
            }

            return listener;
        }
    }
}
