﻿using System.Configuration;

namespace NET02_4.LoggerSectionConfig
{
    public class ListenersCollection : ConfigurationElementCollection
    {
        public ListenersCollection()
        {
            Listener listener = (Listener)CreateNewElement();

            if(listener.AssemblyName != "")
            {
                BaseAdd(listener);
            }
        }

        [ConfigurationProperty("assembly", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Assembly => (string)(base["assembly"]);

        public Listener this[int i] => (Listener)BaseGet(i);

        public override ConfigurationElementCollectionType CollectionType 
            => ConfigurationElementCollectionType.BasicMap;

        protected override string ElementName => "listener";

        protected override ConfigurationElement CreateNewElement() => new Listener();

        protected override object GetElementKey(ConfigurationElement element) => ((Listener)(element)).AssemblyName;
    }
}
