﻿using NET02_4.LoggerSectionConfig.Settings;
using Infrastructure.Helpers;
using System.Configuration;
using System;
using NET02_4.Logger;

namespace NET02_4.LoggerSectionConfig
{
    public static class ConfigReader
    {
        private static readonly LoggerSettings _settings;

        static ConfigReader()
        {
            _settings = new LoggerSettings();
            Read();
        }

        public static LogLevel GetLogLevel() => _settings.LogLevel;

        public static LoggerSettings GetLoggerSettings() => _settings;

        private static void Read()
        {
            var section = (LoggerSection)ConfigurationManager.GetSection("loggerSection");
            Check.NotNull(section, nameof(section), "Could not read loggerSection in App.config");

            LogLevel logLevel;
            Enum.TryParse(section.LogLevel, out logLevel);
            _settings.LogLevel = logLevel;

            for (var i = 0; i < section.Listeners.Count; i++)
            {
                var listenerSettings = new ListenerSettings();

                for (var j = 0; j < section.Listeners[i].Count; j++)
                {
                    listenerSettings.Settings.Add(section.Listeners[i][j].Key, section.Listeners[i][j].Value);
                }

                listenerSettings.AssemblyName = section.Listeners[i].AssemblyName;
                listenerSettings.Type = section.Listeners[i].Type;
                _settings.ListenersSettings.Add(listenerSettings);
            }
        }
    }
}
