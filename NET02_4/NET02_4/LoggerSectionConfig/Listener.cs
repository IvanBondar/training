﻿using System.Configuration;

namespace NET02_4.LoggerSectionConfig
{
    public class Listener : ConfigurationElementCollection
    {
        [ConfigurationProperty("assemblyName", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string AssemblyName => (string)(base["assemblyName"]);

        [ConfigurationProperty("type", DefaultValue = "", IsKey = false, IsRequired = true)]
        public string Type => (string)(base["type"]);

        public ListenerElement this[int i] => (ListenerElement)BaseGet(i); 

        protected override ConfigurationElement CreateNewElement() => new ListenerElement();

        protected override object GetElementKey(ConfigurationElement element) => ((ListenerElement)(element)).Key;
    }
}
