﻿using System.Configuration;

namespace NET02_4.LoggerSectionConfig
{
    public class ListenerElement : ConfigurationElement
    {
        [ConfigurationProperty("key", DefaultValue ="", IsKey = true, IsRequired = true)  ]
        public string Key => ((string)(base["key"]));

        [ConfigurationProperty("value", DefaultValue ="", IsKey = false, IsRequired = false)]
        public string Value => (string)(base["value"]);
    }
}
