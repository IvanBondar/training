﻿using System.Collections.Generic;

namespace NET02_4.LoggerSectionConfig.Settings
{
    public class ListenerSettings
    {
        public string AssemblyName { get; set; }
        public string Type { get; set; }
        public Dictionary<string, string> Settings { get; set; }

        public ListenerSettings()
        {
            Settings = new Dictionary<string, string>();
        }
    }
}
