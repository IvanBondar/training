﻿using NET02_4.Logger;
using System.Collections.Generic;

namespace NET02_4.LoggerSectionConfig.Settings
{
    public class LoggerSettings
    {
        public LogLevel LogLevel { get; set; }
        public List<ListenerSettings> ListenersSettings { get; set; }
        
        public LoggerSettings()
        {
            ListenersSettings = new List<ListenerSettings>();
        }
    }
}
