﻿using System.Configuration;

namespace NET02_4.LoggerSectionConfig
{
    public class LoggerSection : ConfigurationSection
    {
        [ConfigurationProperty("", IsDefaultCollection = true)]
        public ListenersCollection Listeners
        {
            get
            {
                ListenersCollection listenerCollection = (ListenersCollection)base[""];

                return listenerCollection;
            }
        }

        [ConfigurationProperty("logLevel", DefaultValue = "", IsRequired = true)]
        public string LogLevel => ((string)(base["logLevel"]));
    }
}
