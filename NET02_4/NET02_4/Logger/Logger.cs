﻿using Infrastructure.Helpers;
using NET02_4.Abstactions;
using NET02_4.Assemblies;
using NET02_4.Attributes;
using NET02_4.Infrastructure;
using NET02_4.LoggerSectionConfig;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace NET02_4.Logger
{
    /// <summary>
    /// Implement of personal log
    /// </summary>
    public class Logger
    {
        private static InternalLogger _logger;
        private static Logger _instance;
        private static LogLevel _minLogLevel;
        private readonly IReadOnlyList<IListener> _listeners;

        static Logger()
        {
            _logger = new InternalLogger();
        }

        private Logger()
        {
            _listeners = AssembliesManager.GetListeners();
            _minLogLevel = ConfigReader.GetLogLevel(); ;            
        }

        public static Logger GetLogger()
        {
            if(_instance == null)
            {
                _instance = new Logger();
            }

            return _instance;
        }

        public void Debug(string message)
        {
            Log(message, LogLevel.Debug);
        }

        public void Error(string message)
        {
            Log(message, LogLevel.Error);
        }

        public void Fatal(string message)
        {
            Log(message, LogLevel.Fatal);
        }

        public void Info(string message)
        {
            Log(message, LogLevel.Info);
        }

        public void Trace(string message)
        {
            Log(message, LogLevel.Trace);
        }

        public void Warn(string message)
        {
            Log(message, LogLevel.Warn);
        }

        public void Log(string message, LogLevel logLevel)
        {
            if (logLevel < _minLogLevel) return;

            foreach (var item in _listeners)
            {
                item.Write(message, logLevel);
            }
        }

        /// <summary>
        /// Method for tracking any object with TrackingAttributes
        /// </summary>
        /// <param name="obj">obj for tracking</param>
        public void Track(object obj)
        {
            Check.NotNull(obj, nameof(obj));

            var type = obj.GetType();
            var attr = type.GetCustomAttribute(typeof(TrackingEntityAttribute));

            if (attr == null) return;

            var props = type.GetProperties();
            var fields = type.GetFields();
            var result = TrackFields(fields, obj) + TrackProps(props, obj);

            foreach (var item in _listeners)
            {
                item.Track(result);
            }
        }

        private string TrackFields(IEnumerable<FieldInfo> fields, object obj)
        {
            var result = new StringBuilder();

            foreach (var field in fields)
            {
                var trackingPropAttr = field.GetCustomAttribute(typeof(TrackingPropertyAttribute)) as TrackingPropertyAttribute;

                if (trackingPropAttr == null)
                {
                    continue;
                }

                var value = field.GetValue(obj)?.ToString() ?? "null";
                var name = !string.IsNullOrEmpty(trackingPropAttr.Name) ? trackingPropAttr.Name : field.Name;
                result.Append($"{name} = {value} && ");
            }

            return result.ToString();
        }

        private string TrackProps(IEnumerable<PropertyInfo> props, object obj)
        {
            var result = new StringBuilder();

            foreach (var prop in props)
            {
                var trackingPropAttr = prop.GetCustomAttribute(typeof(TrackingPropertyAttribute)) as TrackingPropertyAttribute;

                if (trackingPropAttr == null)
                {
                    continue;
                }

                var value = prop.GetValue(obj)?.ToString() ?? "null";
                var name = !string.IsNullOrEmpty(trackingPropAttr.Name) ? trackingPropAttr.Name : prop.Name;

                result.Append($"{name} = {value} && ");
            }

            return result.ToString();
        }
    }
}
