﻿using NET02_4.Logger;
using System.Collections.Generic;

namespace NET02_4.Abstactions
{
    /// <summary>
    /// Represent interface for Listeners
    /// </summary>
    public interface IListener
    {
        void Setup(Dictionary<string, string> parameters);
        void Write(string message, LogLevel logLevel);
        void Track(string str);
    }
}
