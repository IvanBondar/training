﻿using System;

namespace NET02_4.Abstactions
{
    /// <summary>
    /// Represent interface for InternalLogger
    /// </summary>
    public interface ILogger
    {
        void Trace(string message);
        void Trace(Exception ex, string message);
        void Debug(string message);
        void Debug(Exception ex, string message);
        void Info(string message);
        void Info(Exception ex, string message);
        void Warn(string message);
        void Warn(Exception ex, string message);
        void Error(string message);
        void Error(Exception ex, string message);
        void Fatal(string message);
        void Fatal(Exception ex, string message);
    }
}
