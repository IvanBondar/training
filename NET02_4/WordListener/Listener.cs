﻿using Infrastructure.Helpers;
using Microsoft.Office.Interop.Word;
using NET02_4.Abstactions;
using NET02_4.Infrastructure;
using NET02_4.Logger;
using System;
using System.Collections.Generic;
using System.IO;

namespace WordListener
{
    public class Listener : IListener
    {
        private static readonly InternalLogger _logger;

        public string Path { get; set; }

        static Listener()
        {
            _logger = new InternalLogger();
        }

        public Listener()
        { }

        /// <summary>
        /// Method that set settings of instance;
        /// </summary>
        /// <param name="parameters">parameters for setup</param>
        /// <exception cref="ArgumentNullException">Throw when parameters is null</exception>
        /// <exception cref="ArgumentException">Throw when parameters does not contain required key</exception>
        public void Setup(Dictionary<string, string> parameters)
        {
            Check.NotNull(parameters, nameof(parameters));

            if (!parameters.ContainsKey("path"))
            {
                throw new ArgumentException(@"Parameters don't have ""path"" key", @"parameters[""path""]");
            }

            Check.NotNullOrWhiteSpace(parameters["path"], @"parameters[""path""]");

            Path = System.IO.Path.Combine(parameters["path"], $"{DateTime.Today.Date.ToString("ddmmyyyy")}_Log.doc");
        }

        public void Write(string message, LogLevel logLevel)
        {
            CreateLogAndWriteMessage(Path, $"{DateTime.Now} {logLevel} {message}\n");   
        }

        public void Track(string str)
        {
            CreateLogAndWriteMessage(Path, $"{DateTime.Now} {str}\n");
        }

        /// <summary>
        /// Create log and write message. 
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="path">path</param>
        private static void CreateLogAndWriteMessage(string path, string message)
        {
            try
            {
                var wordApp = new Application();
                wordApp.Visible = false;
                wordApp.DisplayAlerts = WdAlertLevel.wdAlertsNone;

                Document aDoc = File.Exists(path) ? wordApp.Documents.Open(path) : wordApp.Documents.Add();

                wordApp.Selection.InsertAfter(message);
                aDoc.SaveAs2(path);
                aDoc.Close();
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "Something went wrong");
            }
        }
    }
}
