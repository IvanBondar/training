﻿using NET02_4.Logger;
using NET02_4.LoggerSectionConfig;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var tmp = ConfigReader.GetLoggerSettings();
            var log = Logger.GetLogger();
            log.Warn("test Warn");
            log.Log("test Log method", LogLevel.Info);
            var test = new TestEntity();
            log.Track(test);
        }
    }
}
