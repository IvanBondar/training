﻿using NET02_4.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestProject
{
    /// <summary>
    /// Class for work with custom attributes
    /// </summary>
    [TrackingEntity]
    public class TestEntity
    {
        [TrackingProperty(Name = "Name First Field")]
        public string _testFieldFirst;

        [TrackingProperty]
        public string _testFiledSecond;

        public string _testFiledThird;

        [TrackingProperty]
        public string First { get; set; }

        public string Second { get; set; }

        [TrackingProperty(Name = "Name Third Prop")]
        public string Third { get; set; }

        public TestEntity()
        {
            _testFieldFirst = "value first test field";
            _testFiledSecond = "value second test filed";
            First = "value First prop";
            Second = "value Second prop";
        }
    }
}
