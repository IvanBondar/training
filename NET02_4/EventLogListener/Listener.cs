﻿using NET02_4.Abstactions;
using System;
using System.Diagnostics;
using System.Collections.Generic;
using NET02_4.Infrastructure;
using Infrastructure.Helpers;
using NET02_4.Logger;

namespace EventLogListener
{
    /// <summary>
    /// Represents Event Log Listener
    /// </summary>
    public class Listener : IListener
    {
        /// <summary>
        /// Internal logger
        /// </summary>
        private static readonly InternalLogger _logger;

        /// <summary>
        /// Store for the Source;
        /// </summary>
        public string Source { get; set; }
        /// <summary>
        /// Store for the LogName
        /// </summary>
        public string LogName { get; set; }


        /// <summary>
        /// The static class constructor.
        /// </summary>
        static Listener()
        {
            _logger = new InternalLogger();
        }

        /// <summary>
        /// The class constructor.
        /// </summary>
        public Listener()
        { }

        /// <summary>
        /// Method that set settings of instance;
        /// </summary>
        /// <param name="parameters">parameters for setup</param>
        /// <exception cref="ArgumentNullException">Throw when parameters is null</exception>
        /// <exception cref="ArgumentException">Throw when parameters does not contain required key</exception>
        public void Setup(Dictionary<string, string> parameters)
        {
            Check.NotNull(parameters, nameof(parameters));

            if (!parameters.ContainsKey("source"))
            {
                throw new ArgumentException(@"Parameters don't have ""source"" key", nameof(parameters));
            }

            if (!parameters.ContainsKey("logName"))
            {
                throw new ArgumentException(@"Parameters don't have ""logName"" key", nameof(parameters));
            }

            Check.NotNullOrWhiteSpace(parameters["source"], nameof(parameters));
            Check.NotNullOrEmpty(parameters["logName"], nameof(parameters));

            Source = parameters["source"];
            LogName = parameters["logName"];
        }

        /// <summary>
        /// Method write Debug message
        /// </summary>
        /// <param name="message">message</param>
        public void Write(string message, LogLevel logLevel)
        {
            var type = CastLogLevel(logLevel);
            CreateLogAndWriteMessage(message, type);
        }

        /// <summary>
        /// Method write Track
        /// </summary>
        /// <param name="str">str</param>
        public void Track(string str)
        {
            CreateLogAndWriteMessage(str, EventLogEntryType.Information);
        }

        /// <summary>
        /// Method create log and write message
        /// </summary>
        /// <param name="message">message</param>
        /// <param name="messageType">EventLogEntryType value</param>
        private void CreateLogAndWriteMessage(string message, EventLogEntryType messageType)
        {
            try
            {
                if (!EventLog.SourceExists(Source))
                {
                    EventLog.CreateEventSource(Source, LogName);
                }

                var eventLog = new EventLog();
                eventLog.Source = Source;
                eventLog.Log = LogName;
                eventLog.WriteEntry(message, messageType);
            }
            catch(Exception ex)
            {
                _logger.Error(ex, "Something went wrong");
            }
        }

        private EventLogEntryType CastLogLevel(LogLevel logLevel)
        {
            EventLogEntryType type;

            switch (logLevel)
            {
                case LogLevel.Debug:
                case LogLevel.Info:
                case LogLevel.Trace:
                    type = EventLogEntryType.Information;
                    break;
                case LogLevel.Error:
                case LogLevel.Fatal:
                    type = EventLogEntryType.Error;
                    break;
                case LogLevel.Warn:
                    type = EventLogEntryType.Warning;
                    break;
                default:
                    type = EventLogEntryType.Information;
                    break;
            }

            return type;
        }
    }
}
