﻿CREATE TABLE [dbo].[User](
	[UserId] [int] NOT NULL identity(1,1),
	[Name] [varchar](40) NOT NULL,
	[Login] [varchar](20) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Role] [varchar] (20) NOT NULL,
	[isActive] [bit] NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
),
 CONSTRAINT [IX_User_Login] UNIQUE NONCLUSTERED 
(
    [Login] ASC
))


GO




