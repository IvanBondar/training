﻿using NET07_1.Model;

namespace NET07_1.Abstractions
{
    /// <summary>
    /// Interface for UnitOfWork
    /// </summary>
    public interface IUnitOfWork
    {
        IRepository<Role> Roles { get; }
        IRepository<User> Users { get; }
        void Save();
    }
}