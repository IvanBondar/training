﻿using NET07_1.Model;
using System;
using System.Collections.Generic;

namespace NET07_1.Abstractions
{
    /// <summary>
    /// Generic Interface for repositories
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> : IDisposable where TEntity : Entity, new()
    {
        IEnumerable<TEntity> GetAll();
        TEntity GetById(int id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);
    }
}
