﻿using NET07_1.Abstractions;
using NET07_1.ADONET;
using NET07_1.EF;
using NET07_1.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace TestPerformanceProject
{
    internal class Program
    {
        private static readonly AdoNetUnitOfWork _adoNetUow = new AdoNetUnitOfWork();
        private static readonly EFUnitOfWork _efUow = new EFUnitOfWork();
        private static List<User> _users = new List<User>();

        private static void Main(string[] args)
        {
            var users = new List<User>();

            for (var i = 0; i < 1000; i++)
            {
                var user = new User { FirstName = $"testF{i}", LastName = $"testL{i}", Password = $"testP{i}", Login = $"testL{i}", isActive = true };
                users.Add(user);
            }

            var resultCreateADONET = TestCreateOperation(_adoNetUow, users);
            var resultGetAllADONET = TestGetAllOperation(_adoNetUow);
            var resultRemoveADONET = TestRemoveOperation(_adoNetUow, _users);

            var resultCreateEF = TestCreateOperation(_efUow, users);
            var resultGetAllEF = TestGetAllOperation(_efUow);
            var resultRemoveEF = TestRemoveOperation(_efUow, users);
        }

        // 00:00:00.2811762 / 1000 users ado
        // 00:00:01.8078555 / 1000 users ef
        public static TimeSpan TestRemoveOperation(IUnitOfWork uow, IEnumerable<User> users)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            foreach (var user in users)
            {
                uow.Users.Remove(user);
            }

            uow.Save();

            stopWatch.Stop();
            
            return stopWatch.Elapsed;
        }

        //00:00:03.0539656 / 1000 users EF 
        // 00:00:00.2569608 / 1000 users ADO
        public static TimeSpan TestCreateOperation(IUnitOfWork uow, IEnumerable<User> users)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            foreach (var user in users)
            {
                uow.Users.Create(user);
            }

            uow.Save();

            stopWatch.Stop();
            
            return stopWatch.Elapsed;
        }

        //00:00:00.0628417 / 1000 users ADO.NET
        //00:00:01.9539656 / 1000 users EF
        public static TimeSpan TestGetAllOperation(IUnitOfWork uow)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var users = uow.Users.GetAll();

            stopWatch.Stop();

            _users = users.ToList();

            return stopWatch.Elapsed;
        }
    }
}
