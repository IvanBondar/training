﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET07_1.Model;
using System.Linq;

namespace NET07_1.ADONET.Tests
{
    [TestClass]
    public class RoleRepositoryTests
    {
        private readonly AdoNetUnitOfWork _adoUow;
        private Role _testRole;
        private Role _createdRole;

        public RoleRepositoryTests()
        {
            _adoUow = new AdoNetUnitOfWork();
        }      

        [TestMethod]
        public void Step0_Roles_EmptyAtFirst()
        {
            //arrange, act
            int result = _adoUow.Roles.GetAll().Count();

            //assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Step1_CreateRole_RoleCreated()
        {
            //arrange, act
            CreateAndInsertRoleToDb();
            RemoveTestRoleFromDb();

            //assert
            Assert.IsNotNull(_createdRole);
            Assert.AreEqual(_createdRole.Description, _testRole.Description);
            Assert.AreEqual(_createdRole.Description, _testRole.Description);
        }

        [TestMethod]
        public void Step2_RemoveRole_RoleRemoved()
        {
            //arrange
            CreateAndInsertRoleToDb();

            //act
            _adoUow.Roles.Remove(_createdRole);
            var result = _adoUow.Roles.GetAll().Count();

            //assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Step3_Update_RoleUpdated()
        {
            //arrange
            CreateAndInsertRoleToDb();

            //act
            _createdRole.Name = "new name";
            _createdRole.Description = "new description";
            _adoUow.Roles.Update(_createdRole);
            var updatedRole = _adoUow.Roles.GetAll().ToList()[0];
            RemoveTestRoleFromDb();

            //assert
            Assert.AreEqual(_createdRole.Name, updatedRole.Name);
            Assert.AreEqual(_createdRole.Description, updatedRole.Description);
        }

        [TestMethod]
        public void Step4_GetById_ReturnRoleWithSameId()
        {
            //arrange
            CreateAndInsertRoleToDb();

            //act
            var actualRole = _adoUow.Roles.GetById(_createdRole.RoleId);
            RemoveTestRoleFromDb();

            //assert
            Assert.AreEqual(_createdRole.RoleId, actualRole.RoleId);
        }

        [TestMethod]
        public void Step5_GetAll_ReturnAllRoles()
        {
            //arrange
            var role1 = new Role { Name = "testRole1", Description = "test1" };
            var role2 = new Role { Name = "testRole2", Description = "test2" };
            _adoUow.Roles.Create(role1);
            _adoUow.Roles.Create(role2);
            
            //act 
            var roles = _adoUow.Roles.GetAll().ToList();
            _adoUow.Roles.Remove(roles[0]);
            _adoUow.Roles.Remove(roles[1]);

            //assert
            Assert.AreEqual(2, roles.Count());
        }

        private void CreateAndInsertRoleToDb()
        {
            _testRole = new Role { Name = "testRole", Description = "test" };
            _adoUow.Roles.Create(_testRole);
            _createdRole = _adoUow.Roles.GetAll().ToList()[0];
        }

        private void RemoveTestRoleFromDb()
        {
            _adoUow.Roles.Remove(_createdRole);
        }
    }
}
