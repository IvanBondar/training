﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using NET07_1.Model;

namespace NET07_1.ADONET.Tests
{
    [TestClass]
    public class UserRepositoryTests
    {
        private readonly AdoNetUnitOfWork _adoUow;
        private User _testUser;
        private User _createdUser;

        public UserRepositoryTests()
        {
            _adoUow = new AdoNetUnitOfWork();
        }


        [TestMethod]
        public void Step0_Users_EmptyAtFirst()
        {
            //arrange, act
            int result = _adoUow.Users.GetAll().Count();

            //assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Step1_CreateUser_UserCreated()
        {
            //arrange, act
            CreateAndInsertUserToDb();
            RemoveTestUserFromDb();

            //assert
            Assert.IsNotNull(_createdUser);
            Assert.AreEqual(_createdUser.FirstName, _testUser.FirstName);
            Assert.AreEqual(_createdUser.LastName, _testUser.LastName);
            Assert.AreEqual(_createdUser.Login, _testUser.Login);
            Assert.AreEqual(_createdUser.isActive, _testUser.isActive);
        }

        [TestMethod]
        public void Step2_RemoveUser_UserRemoved()
        {
            //arrange
            CreateAndInsertUserToDb();

            //act
            _adoUow.Users.Remove(_createdUser);
            var result = _adoUow.Users.GetAll().Count();

            //assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void Step3_Update_UserUpdated()
        {
            //arrange
            CreateAndInsertUserToDb();

            //act
            _createdUser.FirstName = "new fname";
            _createdUser.LastName = "new lname";
            _adoUow.Users.Update(_createdUser);
            var updatedUser = _adoUow.Users.GetAll().ToList()[0];
            RemoveTestUserFromDb();

            //assert
            Assert.AreEqual(_createdUser.FirstName, updatedUser.FirstName);
            Assert.AreEqual(_createdUser.LastName, updatedUser.LastName);
        }

        [TestMethod]
        public void Step4_GetById_ReturnUserWithSameId()
        {
            //arrange
            CreateAndInsertUserToDb();

            //act
            var actualUser = _adoUow.Users.GetById(_createdUser.UserId);
            RemoveTestUserFromDb();

            //assert
            Assert.AreEqual(_createdUser.UserId, actualUser.UserId);
        }

        [TestMethod]
        public void Step5_GetAll_ReturnAllUsers()
        {
            //arrange
            var user1 = new User
            {
                FirstName = "FName",
                LastName = "LName",
                Login = "Login1",
                isActive = true,
                Password = "password",
            };
            var user2 = new User
            {
                FirstName = "FName",
                LastName = "LName",
                Login = "Login2",
                isActive = true,
                Password = "password",
            };
            _adoUow.Users.Create(user1);
            _adoUow.Users.Create(user2);

            //act 
            var users = _adoUow.Users.GetAll().ToList();
            _adoUow.Users.Remove(users[0]);
            _adoUow.Users.Remove(users[1]);

            //assert
            Assert.AreEqual(2, users.Count());
        }

        private void CreateAndInsertUserToDb()
        {
            _testUser = new User
            {
                FirstName = "FName",
                LastName = "LName",
                Login = "Login",
                isActive = true,
                Password = "password",
            };

            _adoUow.Users.Create(_testUser);
            _createdUser = _adoUow.Users.GetAll().ToList()[0];
        }

        private void RemoveTestUserFromDb()
        {
            _adoUow.Users.Remove(_createdUser);
        }
    }
}
