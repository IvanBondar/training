﻿using System;
using System.Data;

namespace NET07_1.ADONET
{
    /// <summary>
    /// Represent ado.net context
    /// </summary>
    public class AdoNetContext : IDisposable
    {
        /// <summary>
        /// Connetction
        /// </summary>
        private IDbConnection _connection;
        /// <summary>
        /// Connection factory
        /// </summary>
        private readonly IConnectionFactory _connectionFactory;

        /// <summary>
        /// Constructor
        /// </summary>
        public AdoNetContext()
        {
            _connectionFactory = new AppConfigConnectionFactory("UserDb");
        }

        /// <summary>
        /// Method for create command
        /// </summary>
        /// <returns>command</returns>
        public IDbCommand CreateCommand()
        {
            if (_connection == null)
            {
                _connection = _connectionFactory.Create();
            }

            return _connection.CreateCommand();
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            _connection.Dispose();
        }
    }
}
