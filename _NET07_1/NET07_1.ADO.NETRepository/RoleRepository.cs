﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using NET07_1.Model;

namespace NET07_1.ADONET
{
    /// <summary>
    /// Role repository
    /// </summary>
    public class RoleRepository : BaseRepository<Role>
    {
        public RoleRepository(AdoNetContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Create operation for role repository
        /// </summary>
        /// <param name="role">role to create</param>
        /// <exception cref="System.InvalidOperationException"/>
        public override void Create(Role role)
        {
            try
            {
                using (var command = Context.CreateCommand())
                {
                    command.CommandText = @"INSERT INTO [dbo].[Role] 
                                        VALUES (@name, @description)";

                    command.AddParameter("name", role.Name);
                    command.AddParameter("description", role.Description);
                    command.ExecuteNonQuery();
                }
            }
            catch (System.InvalidOperationException ex)
            {
                // write to log
            }
        }

        /// <summary>
        /// Remove operation for role repository
        /// </summary>
        /// <param name="role">role to remove</param>
        public override void Remove(Role role)
        {
            try
            {
                using (var command = Context.CreateCommand())
                {
                    command.CommandText = @"DELETE FROM [dbo].[Role] WHERE [RoleId] = @roleId";
                    command.AddParameter("roleId", role.RoleId);
                    command.ExecuteNonQuery();
                }
            }
            catch (System.InvalidOperationException ex)
            {
                // write to log
            }
        }

        /// <summary>
        ///  Update operation for role repository
        /// </summary>
        /// <param name="role">role for update</param>
        public override void Update(Role role)
        {
            try
            {
                using (var command = Context.CreateCommand())
                {
                    command.CommandText = @"UPDATE [dbo].[Role]
                                            SET [Name] = @name,
                                                [Description] = @description
                                            WHERE [RoleId] = @roleId";

                    command.AddParameter("name", role.Name);
                    command.AddParameter("description", role.Description);
                    command.AddParameter("roleId", role.RoleId);
                    command.ExecuteNonQuery();
                }
            }
            catch (System.InvalidOperationException ex)
            {
                // write to log
            }
        }

        /// <summary>
        ///  Return role by RoleId
        /// </summary>
        /// <param name="id">RoleId</param>
        /// <returns>role with same id</returns>
        public override Role GetById(int id)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM [dbo].[Role] WHERE [RoleId] = @roleId";
                command.AddParameter("roleId", id);

                return ToList(command).FirstOrDefault();
            }
        }

        /// <summary>
        /// Return all role from repository
        /// </summary>
        /// <returns>roles</returns>
        public override IEnumerable<Role> GetAll()
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM [dbo].[Role]";
                return ToList(command);
            }
        }

        /// <summary>
        /// Map role
        /// </summary>
        /// <param name="record"></param>
        /// <param name="role"></param>
        protected override void Map(IDataRecord record, Role role)
        {
            role.RoleId = (int)record["RoleId"];
            role.Name = (string)record["Name"];
            role.Description = (string)record["Description"];
        }
    }
}
