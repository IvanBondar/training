﻿using NET07_1.Model;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NET07_1.ADONET
{
    /// <summary>
    /// UserRole repository
    /// </summary>
    public class UserRoleRepository : BaseRepository<UserRole>
    {
        public UserRoleRepository(AdoNetContext context)
            : base(context)
        {
        }

        /// <summary>
        /// Create operation for UserRole repository
        /// </summary>
        /// <param name="userRole">userRole to create</param>
        public override void Create(UserRole userRole)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"INSERT INTO [dbo].[UserRole] 
                                        VALUES (@userId, @roleId)";

                command.AddParameter("userId", userRole.UserId);
                command.AddParameter("roleId", userRole.RoleId);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Remove operation for UserRole repository
        /// </summary>
        /// <param name="userRole">UserRole to remove</param>
        public override void Remove(UserRole userRole)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"DELETE FROM [dbo].[UserRole] 
                                        WHERE [RoleId] = @roleId AND [UserId] = @userId";
                command.AddParameter("roleId", userRole.RoleId);
                command.AddParameter("userId", userRole.UserId);
                command.ExecuteNonQuery();
            }
        }

        public override void Update(UserRole userRole)
        {
            // throw new NotImplementedException();
        }

        /// <summary>
        /// Return all UserRole from repository
        /// </summary>
        /// <returns>userRoles</returns>
        public override IEnumerable<UserRole> GetAll()
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM [dbo].[UserRole]";

                return ToList(command);
            }
        }

        public override UserRole GetById(int id)
        {
            // throw new NotImplementedException();
            return null;
        }

        /// <summary>
        ///  Return userRole by UserId and RoleId
        /// </summary>
        /// <param name="userId">UserId</param>
        /// <param name="roleId">RoleId</param>
        /// <returns>userRole</returns>
        public UserRole GetByRoleIdUserId(int userId, int roleId)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM [dbo].[UserRole] 
                                        WHERE RoleId = @roleId AND UserId = @userId";
                command.AddParameter("roleId", roleId);
                command.AddParameter("userId", userId);

                return ToList(command).FirstOrDefault();
            }
        }


        /// <summary>
        /// Map userRole
        /// </summary>
        /// <param name="record"></param>
        /// <param name="userRole"></param>
        protected override void Map(IDataRecord record, UserRole userRole)
        {
            userRole.RoleId = (int)record["RoleId"];
            userRole.UserId = (int)record["UserId"];
        }
    }
}
