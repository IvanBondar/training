﻿using NET07_1.Abstractions;
using NET07_1.Model;
using System.Collections.Generic;
using System.Data;

namespace NET07_1.ADONET
{
    /// <summary>
    /// Base abstract class for generic repositories
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity, new()
    {
        /// <summary>
        /// Context
        /// </summary>
        protected AdoNetContext Context { get; }

        protected BaseRepository(AdoNetContext context)
        {
            Context = context;
        }

        /// <summary>
        /// Method for execute command and map result to entities
        /// </summary>
        /// <param name="command">select command</param>
        /// <returns>list of entities</returns>
        protected IEnumerable<TEntity> ToList(IDbCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                var items = new List<TEntity>();

                while (reader.Read())
                {
                    var item = new TEntity();
                    Map(reader, item);
                    items.Add(item);
                }

                return items;
            }
        }

        public abstract void Create(TEntity item);
        public abstract IEnumerable<TEntity> GetAll();
        public abstract TEntity GetById(int id);
        public abstract void Remove(TEntity id);
        public abstract void Update(TEntity item);

        public void Dispose()
        {
            Context.Dispose();
        }

        protected abstract void Map(IDataRecord record, TEntity entity);
    }
}
