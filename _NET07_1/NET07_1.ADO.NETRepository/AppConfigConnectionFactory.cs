﻿using System;
using System.Configuration;
using System.Data;
using System.Data.Common;

namespace NET07_1.ADONET
{
    /// <summary>
    /// App.config connection factory
    /// </summary>
    public class AppConfigConnectionFactory : IConnectionFactory
    {
        /// <summary>
        /// store for provider factory from connection string
        /// </summary>
        private readonly DbProviderFactory _provider;
        /// <summary>
        /// store for connection string
        /// </summary>
        private readonly string _connectionString;
        /// <summary>
        /// Connection name in App.config
        /// </summary>
        private readonly string _name;
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionName">connection name from App.config</param>
        public AppConfigConnectionFactory(string connectionName)
        {
            if (connectionName == null)
            {
                throw new ArgumentNullException(nameof(connectionName));
            }

            var conectionString = ConfigurationManager.ConnectionStrings[connectionName];

            if (conectionString == null)
            {
                throw new ConfigurationErrorsException
                    ($"Failed to find connection string named '{connectionName}' in app.config.");
            }

            _name = connectionName;
            _provider = DbProviderFactories.GetFactory(conectionString.ProviderName);
            _connectionString = conectionString.ConnectionString;
        }

        /// <summary>
        /// Create and open connection
        /// </summary>
        /// <returns>connection</returns>
        public IDbConnection Create()
        {
            var connection = _provider.CreateConnection();

            if (connection == null)
            {
                throw new ConfigurationErrorsException
                    ($"Failed to create a connection using the connection string named '{_name}' in app.config.");
            }
                
            connection.ConnectionString = _connectionString;
            connection.Open();

            return connection;
        }
    }
}
