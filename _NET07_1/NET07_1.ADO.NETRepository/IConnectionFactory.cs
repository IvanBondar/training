﻿using System.Data;

namespace NET07_1.ADONET
{
    public interface IConnectionFactory
    {
        IDbConnection Create();
    }
}
