﻿using System;
using System.Data;

namespace NET07_1.ADONET
{
    public static class CommandExtensions
    {
        /// <summary>
        /// Make a command
        /// </summary>
        /// <param name="command">Command</param>
        /// <param name="name">Paramater name</param>
        /// <param name="value">Value</param>
        public static void AddParameter(this IDbCommand command, string name, object value)
        {
            if (command == null)
            {
                throw new ArgumentNullException(nameof(command));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            var p = command.CreateParameter();
            p.ParameterName = name;
            p.Value = value ?? DBNull.Value;
            command.Parameters.Add(p);
        }
    }
}
