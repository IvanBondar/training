﻿using NET07_1.Model;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace NET07_1.ADONET
{
    /// <summary>
    /// User repository
    /// </summary>
    public class UserRepository : BaseRepository<User>
    {
        private readonly RoleRepository _roleRepository;

        public UserRepository(AdoNetContext context)
            : base(context)
        {
            _roleRepository = new RoleRepository(context);
        }

        /// <summary>
        /// Create operation for user repository
        /// </summary>
        /// <param name="user">user to create</param>
        public override void Create(User user)
        {

            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"INSERT INTO [dbo].[User] 
                        VALUES (@firstName, @lastName, @login, @password, @isActive)";

                command.AddParameter("firstName", user.FirstName);
                command.AddParameter("lastName", user.LastName);
                command.AddParameter("login", user.Login);
                command.AddParameter("password", user.Password);
                command.AddParameter("isActive", user.isActive);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Remove operation for user repository
        /// </summary>
        /// <param name="user">user to remove</param>
        public override void Remove(User user)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"DELETE FROM [dbo].[User] WHERE UserId = @userId";
                command.AddParameter("userId", user.UserId);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        ///  Update operation for user repository
        /// </summary>
        /// <param name="user">user for update</param>
        public override void Update(User user)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"UPDATE [dbo].[User]
                                           SET [FirstName] = @firstName,
                                               [LastName] =@lastName,
                                               [Login] = @login,
                                               [Password] = @password,
                                               [isActive] = @isActive 
                                       WHERE UserId = @userId";

                command.AddParameter("firstName", user.FirstName);
                command.AddParameter("lastName", user.LastName);
                command.AddParameter("login", user.Login);
                command.AddParameter("password", user.Password);
                command.AddParameter("isActive", user.isActive);
                command.AddParameter("userId", user.UserId);
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Return all user from repository
        /// </summary>
        /// <returns>users</returns>
        public override IEnumerable<User> GetAll()
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM [dbo].[User]";

                return ToList(command);
            }
        }

        /// <summary>
        ///  Return user by RoleId
        /// </summary>
        /// <param name="id">UserId</param>
        /// <returns>user with same id</returns>
        public override User GetById(int id)
        {
            using (var command = Context.CreateCommand())
            {
                command.CommandText = @"SELECT * FROM [dbo].[User] 
                                        WHERE [dbo].[User].UserId = @userId";
                command.AddParameter("userId", id);
                var user = ToList(command).FirstOrDefault();

                return user;
            }
        }

        /// <summary>
        /// Map user method
        /// </summary>
        /// <param name="record"></param>
        /// <param name="user"></param>
        protected override void Map(IDataRecord record, User user)
        {
            user.UserId = (int)record["UserId"];
            user.FirstName = (string)record["FirstName"];
            user.LastName = (string)record["LastName"];
            user.Login = (string)record["Login"];
            user.Password = (string)record["Password"];
            user.isActive = (bool)record["isActive"];
        }
    }
}