﻿using NET07_1.Abstractions;
using NET07_1.Model;

namespace NET07_1.ADONET
{
    /// <summary>
    /// ADO.NET Unot Of Work
    /// </summary>
    public class AdoNetUnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// Store for context
        /// </summary>
        private readonly AdoNetContext _context = new AdoNetContext();
        /// <summary>
        /// Store for role repository
        /// </summary>
        private RoleRepository _roleRepository;
        /// <summary>
        /// store for user repository
        /// </summary>
        private UserRepository _userRepository;

        /// <summary>
        /// Return role repository
        /// </summary>
        public IRepository<Role> Roles
        {
            get
            {
                if (_roleRepository != null)
                {
                    return _roleRepository;
                }

                _roleRepository = new RoleRepository(_context);

                return _roleRepository;
            }
        }

        /// <summary>
        /// return user repository
        /// </summary>
        public IRepository<User> Users
        {
            get
            {
                if (_userRepository != null)
                {
                    return _userRepository;
                }

                _userRepository = new UserRepository(_context);

                return _userRepository;
            }
        }

        /// <summary>
        /// Save context. Not implemented.
        /// </summary>
        public void Save()
        { }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
