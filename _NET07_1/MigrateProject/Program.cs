﻿using System.Linq;
using MigrateProject.OldDatabase;
using NET07_1.Model;
using NET07_1.EF;

namespace MigrateProject
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new OldDatabaseContext();
            var oldUsers = context.Users.ToList();

            using (var uow = new EFUnitOfWork())
            {
                var oldRoleName = oldUsers.FirstOrDefault()?.Role;

                if (string.IsNullOrEmpty(oldRoleName))
                {
                    oldRoleName = "defaultRole";
                }

                var role = new Role() { Name = oldRoleName, Description = "No description" };

                foreach (var oldUser in oldUsers)
                {
                    var fullName = oldUser.Name.Split(',').Select(x => x.Trim()).ToList();

                    var newUser = new User()
                    {
                        FirstName = fullName[1],
                        LastName = fullName[0],
                        Login = oldUser.Login,
                        Password = oldUser.Password,
                        isActive = oldUser.isActive,
                        Roles = { role }
                    };
                    uow.Users.Create(newUser);
                }

                uow.Save();
            }

            context.Dispose();
        }
    }
}
