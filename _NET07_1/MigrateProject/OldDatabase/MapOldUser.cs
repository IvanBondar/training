﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using MigrateProject.OldDatabase.Model;

namespace MigrateProject.OldDatabase
{
    /// <summary>
    /// Map user class
    /// </summary>
    public static class ModelBuilderExtensions
    {
        /// <summary>
        /// Map user method
        /// </summary>
        /// <param name="builder"></param>
        public static void MapOldRole(this DbModelBuilder builder)
        {
            var entity = builder.Entity<OldUser>();

            entity.ToTable("User");
            entity.HasKey(e => e.UserId);

            entity.Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(50).IsUnicode(false);
            entity.Property(e => e.UserId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            entity.Property(e => e.Login).HasColumnName("Login").IsRequired().HasMaxLength(20).IsUnicode(true);
            entity.Property(e => e.Password).HasColumnName("Password").IsRequired().HasMaxLength(50).IsUnicode(false);
            entity.Property(e => e.isActive).HasColumnName("isActive").IsRequired();
        }
    }
}
