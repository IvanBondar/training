﻿namespace MigrateProject.OldDatabase.Model
{
    /// <summary>
    /// Model for user from old db
    /// </summary>
    public class OldUser
    {
        public int UserId { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public bool isActive { get; set; }
    }
}
