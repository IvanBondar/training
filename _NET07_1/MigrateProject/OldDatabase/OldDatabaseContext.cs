﻿using MigrateProject.OldDatabase.Model;
using System.Data.Entity;

namespace MigrateProject.OldDatabase
{
    public class OldDatabaseContext : DbContext
    {
        public virtual DbSet<OldUser> Users { get; set; }

        public OldDatabaseContext()
            : base("name = OldDatabase")
        {
        }

        /// <summary>
        /// Map user on model creating
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.MapOldRole();
        }
    }

}
