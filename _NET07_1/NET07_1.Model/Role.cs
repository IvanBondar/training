﻿using System.Collections.Generic;

namespace NET07_1.Model
{
    public sealed class Role : Entity
    {
        public Role()
        {
            Users = new HashSet<User>();
        }

        public int RoleId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
