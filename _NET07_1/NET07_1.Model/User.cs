﻿using System.Collections.Generic;

namespace NET07_1.Model
{
    public sealed class User : Entity
    {
        public User()
        {
            Roles = new HashSet<Role>();
        }

        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool isActive { get; set; }

        public ICollection<Role> Roles { get; set; }
    }
}
