﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using NET07_1.Model;

namespace NET07_1.EF.Mappings
{
    public static partial class ModelBuilderExtensions
    {
        public static void MapRole(this DbModelBuilder builder)
        {
            var entity = builder.Entity<Role>();

            entity.ToTable("Role");
            entity.HasKey(e => e.RoleId);

            entity.Property(e => e.RoleId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            entity.Property(e => e.Name).HasColumnName("Name").IsRequired().HasMaxLength(20).IsUnicode(false);
            entity.Property(e => e.Description).HasColumnName("Description").IsRequired().HasMaxLength(100).IsUnicode(false);

            entity.HasMany(e => e.Users)
                  .WithMany(e => e.Roles)
                  .Map(m => m.ToTable("UserRole").MapLeftKey("RoleId").MapRightKey("UserId"));
        }
    }
}