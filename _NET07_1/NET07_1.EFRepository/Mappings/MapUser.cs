﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using NET07_1.Model;

namespace NET07_1.EF.Mappings
{
    public static partial class ModelBuilderExtensions
    {
        public static void MapUser(this DbModelBuilder builder)
        {
            var entity = builder.Entity<User>();

            entity.ToTable("User");
            entity.HasKey(e => e.UserId);

            entity.Property(e => e.UserId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            entity.Property(e => e.FirstName).HasColumnName("FirstName").IsRequired().HasMaxLength(20).IsUnicode(false);
            entity.Property(e => e.LastName).HasColumnName("LastName").IsRequired().HasMaxLength(20).IsUnicode(false);
            entity.Property(e => e.Login).HasColumnName("Login").IsRequired().HasMaxLength(20).IsUnicode(true);
            entity.Property(e => e.Password).HasColumnName("Password").IsRequired().HasMaxLength(50).IsUnicode(false);
        }
    }
}
