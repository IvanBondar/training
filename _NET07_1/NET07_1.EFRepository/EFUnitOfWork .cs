﻿using NET07_1.Abstractions;
using System;
using NET07_1.Model;

namespace NET07_1.EF
{
    /// <summary>
    /// Entity Framework Unit Of Work
    /// </summary>
    public class EFUnitOfWork : IUnitOfWork, IDisposable
    {
        /// <summary>
        /// Store for context
        /// </summary>
        private readonly EFDbContext _context = new EFDbContext();
        /// <summary>
        /// Store for role repository
        /// </summary>
        private RoleRepository _roleRepository;
        /// <summary>
        /// store for user repository
        /// </summary>
        private UserRepository _userRepository;

        /// <summary>
        /// Return role repository
        /// </summary>
        public IRepository<Role> Roles
        {
            get
            {
                if (_roleRepository != null)
                {
                    return _roleRepository;
                }

                _roleRepository = new RoleRepository(_context);

                return _roleRepository;
            }
        }

        /// <summary>
        /// Return user repository
        /// </summary>
        public IRepository<User> Users
        {
            get
            {
                if (_userRepository != null)
                {
                    return _userRepository;
                }

                _userRepository = new UserRepository(_context);

                return _userRepository;
            }
        }

        /// <summary>
        /// Save context method
        /// </summary>
        public void Save()
        {
            _context.SaveChanges();
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
