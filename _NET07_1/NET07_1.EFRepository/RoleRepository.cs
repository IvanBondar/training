﻿using NET07_1.Model;
using System.Data.Entity;

namespace NET07_1.EF
{
    /// <summary>
    /// Role repository for EF data access layer
    /// </summary>
    public class RoleRepository : EFRepository<Role>
    {
        public RoleRepository(DbContext context) : base(context)
        {
        }
    }
}
