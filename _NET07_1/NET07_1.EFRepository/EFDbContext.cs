﻿using NET07_1.Model;
using System.Data.Entity;
using NET07_1.EF.Mappings;

namespace NET07_1.EF
{
    /// <summary>
    /// Class for create context of UserDb
    /// </summary>
    public class EFDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }

        public EFDbContext()
            : base("name=UserDb")
        {
        }

        /// <summary>
        /// Execute mapping entities on model creating.
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.MapRole();
            builder.MapUser();
        }
    }
}
