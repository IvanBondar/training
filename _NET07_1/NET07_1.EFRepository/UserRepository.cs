﻿using NET07_1.Model;
using System.Data.Entity;

namespace NET07_1.EF
{
    /// <summary>
    /// User repository for EF data access layer
    /// </summary>
    public class UserRepository : EFRepository<User>
    {
        public UserRepository(DbContext context) : base(context)
        {
        }
    }
}
