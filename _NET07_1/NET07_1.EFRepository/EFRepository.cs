﻿using System.Collections.Generic;
using NET07_1.Model;
using System.Data.Entity;
using System.Linq;
using NET07_1.Abstractions;

namespace NET07_1.EF
{
    /// <summary>
    /// Base generic repository for EF data access layer
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class EFRepository<TEntity> : IRepository<TEntity> where TEntity : Entity, new()
    {
        /// <summary>
        /// store for dbcontext
        /// </summary>
        readonly DbContext _context;
        /// <summary>
        /// store for dbSet
        /// </summary>
        readonly DbSet<TEntity> _dbSet;

        public EFRepository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        /// <summary>
        /// GetAll method
        /// </summary>
        /// <returns></returns>
        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.AsNoTracking().ToList();
        }
        
        /// <summary>
        /// Create method
        /// </summary>
        /// <param name="item"></param>
        public void Create(TEntity item)
        {
            _dbSet.Add(item);
        }

        /// <summary>
        /// Remove method
        /// </summary>
        /// <param name="item"></param>
        public void Remove(TEntity item)
        {
            _dbSet.Remove(item);
        }

        /// <summary>
        /// Get by id method
        /// </summary>
        /// <param name="id">Entity id</param>
        /// <returns>entity with same id</returns>
        public TEntity GetById(int id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// Update entity method
        /// </summary>
        /// <param name="item">entity for update</param>
        public void Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
