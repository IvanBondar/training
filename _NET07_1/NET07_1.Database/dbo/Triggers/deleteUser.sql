﻿CREATE TRIGGER [dbo].[deleteUser]
   ON  [dbo].[User] 
   INSTEAD OF DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @userId INT
	SELECT @userId =  (SELECT UserId FROM deleted)
    DELETE FROM [dbo].[UserRole] WHERE [UserId] = @userId
	DELETE FROM [dbo].[User] WHERE [UserId] = @userId
END