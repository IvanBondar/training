﻿CREATE TRIGGER [dbo].[deleteRole]
   ON  [dbo].[Role] 
   INSTEAD OF DELETE
AS 
BEGIN
	SET NOCOUNT ON;
	DECLARE @roleId INT
	SELECT @roleId =  (SELECT RoleId FROM deleted)
    DELETE FROM [dbo].[UserRole] WHERE [RoleId] = @roleId
	DELETE FROM [dbo].[Role] WHERE [RoleId] = @roleId
END
