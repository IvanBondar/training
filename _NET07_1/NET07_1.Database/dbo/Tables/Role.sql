﻿CREATE TABLE [dbo].[Role](
	[RoleId] [int] NOT NULL identity(1,1),
	[Name] [varchar](20) NOT NULL,
	[Description] [varchar](100) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
))

GO



