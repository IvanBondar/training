﻿using NET01_2.MatrixAgg;
using System;

namespace NET01_2_addition
{
    public class UpperTriangularMatrix<T> : SquareMatrix<T>
    {
        public UpperTriangularMatrix(int size)
            :base(size)
        {
        }

        protected override T[] InitializeMatrix(int size) => new T[(size * (size + 1)) / 2];

        new protected void CheckIndex(int x, int y)
        {
            if (x > y)
            {
                throw new IndexOutOfRangeException("You can't change this element in upper triangular matrix.");
            }

            base.CheckIndex(x, y);
        }

        protected override int GetIndex(int x, int y) => (x * Size + y) - (x * (x + 1) / 2);

        protected override T GetValue(int x, int y)
        {
            base.CheckIndex(x, y);
            if(x < y)
            {
                return default(T);
            }

            return base.GetValue(x, y);
        }

        protected override void SetValue(int x, int y, T value)
        {
            CheckIndex(x, y);
            base.SetValue(x, y, value);
        }

    }
}
