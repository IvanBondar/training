﻿using NET01_2.MatrixAgg;
using System;
namespace NET01_2.Extensions
{
    public static class MatrixExtension
    {
        public static SquareMatrix<T> Sum<T>(this SquareMatrix<T> first, SquareMatrix<T> second)
        {
            if (first.Size != second.Size || first.GetType() != second.GetType() || first == null || second == null)
            {
                throw new ArgumentException("Wrong matrix type, size or some matrix is null");
            }
                
            
            if (first is DiagonalMatrix<T>)
            {
                return SumDiagonalMatrix((DiagonalMatrix<T>)first, (DiagonalMatrix<T>)second); 
            }

            return SumSquareMatrix(first, second);
        }
        
        private static SquareMatrix<T> SumSquareMatrix<T>(SquareMatrix<T> first, SquareMatrix<T> second)
        {
            SquareMatrix<T> result = new SquareMatrix<T>(first.Size);
            dynamic itemA, itemB;

            for (int i = 0; i < first.Size; i++)
            {
                for (int j = 0; j < first.Size; j++)
                {
                    itemA = first[i, j];
                    itemB = second[i, j];
                    result[i, j] = itemA + itemB;
                }
            }
                
            return result;
        } 

        private static DiagonalMatrix<T> SumDiagonalMatrix<T>(DiagonalMatrix<T> first, DiagonalMatrix<T> second)
        {
            DiagonalMatrix<T> result = new DiagonalMatrix<T>(first.Size);
            dynamic itemA, itemB;

            for (int i = 0; i < first.Size; i++)
            {
                itemA = first[i, i];
                itemB = second[i, i];
                result[i, i] = itemA + itemB;
            }

            return result;
        }
    }
}
