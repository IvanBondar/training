﻿using NET01_2.Extensions;
using NET01_2.MatrixAgg;
using System;
using System.Linq;

namespace NET01_2
{
    class Program
    {
        /// <summary>
        /// The entry point for the application.</summary>
        /// <param name="args">A list of command line arguments.</param>
        static void Main(string[] args)
        {
            //TestSumDiagonalMatrices();
            //TestSumSquareMatrices();
            //TestEventHandler();
            //TestExceptions();
        }

        /// <summary>
        /// Method TestEventHandler checks to raise events.</summary>
        public static void TestEventHandler()
        {

            DiagonalMatrix<int> diagMatrix = new DiagonalMatrix<int>(3);
            SquareMatrix<int> squareMatrix = new SquareMatrix<int>(3);

            squareMatrix.ChangeElement += OnMatrixElementChanged;
            squareMatrix.ChangeElement += (object obj, MatrixEventArgs<int> arg) 
                => { PrintValue($"[Through the lambda expression] old value {arg.value}"); };
            squareMatrix.ChangeElement += delegate (object obj, MatrixEventArgs<int> arg)
            {
                PrintValue($"[Through anonymous method] {arg.value}");
            };

            diagMatrix.ChangeElement += (object obj, MatrixEventArgs<int> arg) 
                => { PrintValue($"[Diagonal Matrix. Through the lambda expression] old value {arg.value}"); };

            squareMatrix[0, 0] = 4;
            squareMatrix[0, 0] = 1;
            diagMatrix[0, 0] = 10;
        }

        /// <summary>
        /// Method TestExceptions checks to raise exceptions.</summary>
        public static void TestExceptions()
        {
            try
            {
                DiagonalMatrix<int> dm1 = new DiagonalMatrix<int>(-1);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                PrintValue($"Exception message: {ex.Message}");
            }

            try
            {
                DiagonalMatrix<int> dm2 = new DiagonalMatrix<int>(3);
                dm2[0, 1] = 7;
            }
            catch (IndexOutOfRangeException ex)
            {
                PrintValue($"Exception message: {ex.Message}");
            }
        }

        /// <summary>
        /// Method MatrixElementChanged is target for event.</summary>
        /// <param name="msg">Message from raised event.</param>
        public static void OnMatrixElementChanged<T>(object obj, MatrixEventArgs<T> arg)
        {
            PrintValue($"[Through the method] old value {arg.value}");
        }

        /// <summary>
        /// Testing sum of two Diagonal Matrix.</summary>
        public static void TestSumDiagonalMatrices()
        {
            DiagonalMatrix<int> a = new DiagonalMatrix<int>(3);
            DiagonalMatrix<int> b = new DiagonalMatrix<int>(3);

            for (int i = 0; i < a.Size; i++)
            {
                a[i, i] = i;
                b[i, i] = i;
            }

            DiagonalMatrix<int> c = (DiagonalMatrix<int>)a.Sum(b);
            Console.WriteLine();
        }

        /// <summary>
        /// Testing sum of two Square Matrix.</summary>
        public static void TestSumSquareMatrices()
        {
            SquareMatrix<int> a = new SquareMatrix<int>(3);
            SquareMatrix<int> b = new SquareMatrix<int>(3);

            for (int i = 0; i < a.Size; i++)
            {
                for (int j = 0; j < a.Size; j++)
                {
                    a[i, j] = j;
                    b[i, j] = j;
                }
            }

            SquareMatrix<int> c = a.Sum(b);
        }

        /// <summary>
        /// Prints the value into the current output stream.</summary>
        /// <param name="value">Value to print.</param>
        public static void PrintValue(string value)
        {
            Console.WriteLine(value);
        }
    }
}
