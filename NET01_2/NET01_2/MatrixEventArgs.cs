﻿using System;

namespace NET01_2
{
    public class MatrixEventArgs<T> : EventArgs
    {
        public readonly T value;

        public MatrixEventArgs(T oldValue)
        {
            value = oldValue;
        }
    }
}
