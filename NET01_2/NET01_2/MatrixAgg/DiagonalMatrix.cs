﻿using System;

namespace NET01_2.MatrixAgg
{
    /// <summary>
    /// Represents diagonal matrix.</summary>
    /// <typeparam name="T">Type of matrix elemets.</typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        /// <summary>
        /// The class constructor.</summary>
        /// <param name="size">Size of creating matrix.</param>
        public DiagonalMatrix(int size)
            :base(size)
        {
        }

        protected override T[] InitializeMatrix(int size) => new T[size];
        
        /// <summary>
        /// Checking index to correct value</summary>
        /// <param name="x">X index for check.</param>
        /// <param name="y">Y index for check.</param>
        /// <exception cref="IndexOutOfRangeException">Throw when x != y.</exception>
        new protected void CheckIndex(int x, int y)
        {
            if (x != y)
            {
                throw new IndexOutOfRangeException("You can't change this element in diagonal matrix.");
            }
        }

        protected override int GetIndex(int x, int y) => x;

        protected override void SetValue(int x, int y, T value)
        {
            CheckIndex(x, y);
            base.SetValue(x, y, value);
        }

        protected override T GetValue(int x, int y)
        {
            base.CheckIndex(x, y);
            if (x != y)
            {
                return default(T);
            }

            return base.GetValue(x, y);
        }

    }
}
