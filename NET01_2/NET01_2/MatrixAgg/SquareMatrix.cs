﻿using System;
using System.Linq;

namespace NET01_2.MatrixAgg
{
    /// <summary>
    /// Represents square matrix.</summary>
    /// <typeparam name="T">Type of matrix elemets.</typeparam>
    public class SquareMatrix<T>
    {
        /// <summary>
        /// Store for the matrix values</summary>
        protected T[] matrixValues;

        /// <summary>
        /// Return the matrix's size.</summary>
        public int Size { get; }

        protected virtual void OnElementChange(MatrixEventArgs<T> value)
        {
            ChangeElement?.Invoke(this, value);
        }

        /// <summary>
        /// EventHandler for Matrix</summary>
        public event EventHandler<MatrixEventArgs<T>> ChangeElement;

        /// <summary>
        /// Indexer for square matrix.</summary>
        /// <param name="x">X position of matrix element.</param>
        /// <param name="y">Y position of matrix element.</param>
        /// <returns>Return element on [x,y] position matrix.</returns>
        public virtual T this[int x, int y]
        {
            get { return GetValue(x, y); }
            set { SetValue(x, y, value); }
        }

        /// <summary>
        /// The class constructor.</summary>
        /// <param name="size">Size of creating matrix.</param>
        public SquareMatrix(int size)
        {
            CheckMatrixSize(size);
            Size = size;
            matrixValues = InitializeMatrix(size);
        }

        protected virtual T[] InitializeMatrix(int size) => new T[size * size];

        protected virtual int GetIndex(int x, int y) => (Size) * x + y;

        protected bool IsSameValue(int x, int y, T value) => matrixValues[GetIndex(x, y)].Equals(value);

        protected virtual void SetValue(int x, int y, T value)
        {
            CheckIndex(x, y);
            if (!IsSameValue(x, y, value))
            {
                OnElementChange(new MatrixEventArgs<T>(matrixValues[GetIndex(x, y)]));
                matrixValues[GetIndex(x, y)] = value;
            }
        }

        protected virtual T GetValue(int x, int y)
        {
            CheckIndex(x, y);
            return matrixValues[GetIndex(x, y)];
        } 

        /// <summary>
        /// Checking matrix size.</summary>
        /// <param name="x">х to check.</param>
        /// <param name="y">н to check.</param>
        /// <exception cref="ArgumentOutOfRangeException">Throw when size &lt= 0</exception>
        protected void CheckMatrixSize(int size)
        {
            if (size < 0)
            {
                throw new ArgumentOutOfRangeException("Matrix size can't be null or negative");
            }
        }

        /// <summary>
        /// Checking index to correct value.</summary>
        /// <param name="x">x position in matrix.</param>
        /// <param name="y">y position in matrix.</param>
        /// <exception cref="IndexOutOfRangeException">Throw when position is wrong</exception>
        protected void CheckIndex(int x, int y)
        {
            if (x < 0 || y < 0 || x > Size - 1 || y > Size - 1)
            {
                throw new IndexOutOfRangeException("Wrong index");
            }
        }

        public override string ToString()
            => string.Join(" ", matrixValues.Select(p => p.ToString()).ToArray());
    }
}
