﻿using System;

namespace NET01_2.UInt128Agg
{
    /// <summary>
    /// Represents structure UInt128.</summary>
    struct UInt128 : IComparable<UInt128>, IEquatable<UInt128>
    {
        /// <summary>
        /// Store for low part of UInt128 structure.</summary>
        private ulong _lo;
        /// <summary>
        /// Store for high part of UInt128 structure.</summary>
        private ulong _hi;

        /// <summary>
        /// Initializes a new instance of the UInt128.</summary>
        /// <param name="high">Initial value for high part of structure.</param>
        /// <param name="low">Initial value for low part of structure</param>
        public UInt128(ulong high, ulong low)
        {
            _lo = low;
            _hi = high;
        }

        /// <summary>
        /// Implements the operator +.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static UInt128 operator +(UInt128 left, UInt128 right)
        {
            unchecked
            {
                UInt128 result = new UInt128();
                result._lo = left._lo + right._lo;
                if (left._lo > result._lo || right._lo > result._lo)
                {
                    result._hi++;
                }

                result._hi += left._hi + right._hi;

                return result;
            }
        }

        /// <summary>
        /// Implements the operator -.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static UInt128 operator -(UInt128 left, UInt128 right)
        {
            unchecked
            {                           
                UInt128 result = new UInt128();

                if (left._lo < right._lo)
                {
                    left._hi--;
                    result._lo = ulong.MaxValue - right._lo + left._lo;
                }
                else
                {
                    result._lo = left._lo - right._lo;
                }
                result._hi = left._hi - right._hi;

                return result;
            }
        }

        /// <summary>
        /// Implements the operator ==.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator ==(UInt128 left, UInt128 right) => left.Equals(right);

        /// <summary>
        /// Implements the operator !=.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator !=(UInt128 left, UInt128 right) => !left.Equals(right);

        /// <summary>
        /// Implements the operator >.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator >(UInt128 left, UInt128 right) => left.CompareTo(right) > 0;

        /// <summary>
        /// Implements the operator <.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator <(UInt128 left, UInt128 right) => left.CompareTo(right) < 0;

        /// <summary>
        /// Implements the operator >=.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator >=(UInt128 left, UInt128 right) => left.CompareTo(right) >= 0;

        /// <summary>
        /// Implements the operator <=.</summary>
        /// <param name="left">The left</param>
        /// <param name="right">The right</param>
        /// <returns>The result of the operator.</returns>
        public static bool operator <=(UInt128 left, UInt128 right) => left.CompareTo(right) <= 0;

        /// <summary>
        /// Performs an implicit conversion from long to UInt128.</summary>
        /// <param name="value">The value.</param>
        public static implicit operator UInt128(long value) => new UInt128(0, (ulong)value);

        /// <summary>
        /// Performs an implicit conversion from ulong to UInt128.</summary>
        /// <param name="value">The value.</param>
        public static implicit operator UInt128(ulong value) => new UInt128(0, value);

        /// <summary>
        /// Performs an explicit conversion from UInt128 to ulong.</summary>
        /// <param name="value">The value.</param>
        public static explicit operator ulong(UInt128 value) => value._lo;

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.</summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.</returns>
        public override string ToString() => ($"[0x{_hi.ToString("X16")}].[0x{_lo.ToString("X16")}]");

        /// <summary>
        /// Returns a hash code for this instance.</summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
        public override int GetHashCode() => (_hi.ToString() + _lo.ToString()).GetHashCode();

        /// <summary>
        /// Returns a value indicating whether this instance is equal to a specified object.</summary>
        /// <param name="obj">An object to compare with this instance.</param>
        /// <returns>
        /// true if obj has the same value as this instance; otherwise, false.</returns>
        public override bool Equals(object obj)
        {
            UInt128? temp = obj as UInt128?;

            return temp.HasValue && Equals(temp.Value);
        }

        /// <summary>
        /// Returns a value indicating whether this instance is equal to other UInt128 instance.</summary>
        /// <param name="other">An instance to compare with this instance.</param>
        /// <returns>
        /// true if obj has the same value as this instance; otherwise, false.</returns>
        public bool Equals(UInt128 other) => other._hi.Equals(_hi) && other._lo.Equals(_lo);

        /// <summary>
        /// Returns a value indicating whether this instance is bigger/smaller/equal to a specified UInt128.</summary>
        /// <param name="other">UInt128 structure to compare whit this instance.</param>
        /// <returns> 
        /// 1/0/-1 if this instance bigger/equal/smaller than a specified instance.</returns>
        public int CompareTo(UInt128 other)
        {
            if ((_hi > other._hi) || (_hi == other._hi && _lo > other._lo))
                return 1;
            if ((_hi < other._hi) || (_hi == other._hi && _lo < other._lo))
                return -1;
            else
                return 0;
        }
    }
}

