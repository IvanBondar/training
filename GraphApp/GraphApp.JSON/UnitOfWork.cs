﻿using GraphApp.Domain.BaseRepository;
using GraphApp.Domain.Model;

namespace GraphApp.JSON
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JsonContext _jsonContext = new JsonContext();
        private VertexRepository _vertexRepository;
        private EdgeRepository _edgeRepository;

        public IRepository<Vertex> Vertices
        {
            get
            {
                if (_vertexRepository != null)
                {
                    return _vertexRepository;
                }

                _vertexRepository = new VertexRepository(_jsonContext);

                return _vertexRepository;
            }
        }

        public IRepository<Edge> Edges
        {
            get
            {
                if (_edgeRepository != null)
                {
                    return _edgeRepository;
                }   

                _edgeRepository = new EdgeRepository(_jsonContext);

                return _edgeRepository;
            }
        }

        public void Save()
        {
            _jsonContext.Save(); 
        }
    }
}
