﻿using System.Collections.Generic;
using System.Linq;
using GraphApp.Domain.BaseRepository;
using GraphApp.Domain.Model;

namespace GraphApp.JSON
{
    public class EdgeRepository : IRepository<Edge>
    {
        private readonly List<Edge> _edges;

        public EdgeRepository(JsonContext context) 
        {
            _edges = context.Edges;
        }

        public IEnumerable<Edge> GetAll() => _edges;

        public Edge GetById(int id) 
            => _edges.Select(x => x).FirstOrDefault(x => x.Id == id);

        public void Add(Edge entity)
        {
            _edges.Add(entity);
        }

        public void Remove(Edge entity)
        {
            _edges.Remove(entity);
        }
    }
}
