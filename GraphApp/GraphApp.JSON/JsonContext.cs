﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using GraphApp.Domain.Model;
using Newtonsoft.Json;

namespace GraphApp.JSON
{
    public class JsonContext
    {
        private readonly string _source;

        public List<Vertex> Vertices { get; set; }
        public List<Edge> Edges { get; set; }

        public JsonContext()
        {
            _source = ConfigurationManager.AppSettings["source"];
            Read();
        }

        public void Save()
        {
            var graph = new Graph
            {
                Vertices = Vertices,
                Edges = Edges
            };
            var jsonGraph = JsonConvert.SerializeObject(graph, Formatting.Indented);

            try
            {
                using (var sw = new StreamWriter(_source, false))
                {
                    sw.WriteLine(jsonGraph);
                }
            }
            catch (Exception ex)
            {
                // log ex
            }
        }

        private void Read()
        {
            if (!File.Exists(_source))
            {
                // log and create empty lists
                Vertices = new List<Vertex>();
                Edges = new List<Edge>();

                return;
            }

            try
            {
                using (var stream = new StreamReader(_source))
                {
                    var input = stream.ReadToEnd();
                    var graph = JsonConvert.DeserializeObject<Graph>(input);

                    Vertices = graph.Vertices;
                    Edges = graph.Edges;
                }
            }
            catch (ArgumentNullException ex)
            {
                // log ex
            }
            catch (Exception ex)
            {
                // log ex
            }
        }
    }
}
