﻿using System.Collections.Generic;
using System.Linq;
using GraphApp.Domain.BaseRepository;
using GraphApp.Domain.Model;

namespace GraphApp.JSON
{
    public class VertexRepository : IRepository<Vertex>
    {
        private readonly List<Vertex> _vertices;

        public VertexRepository(JsonContext context)
        {
            _vertices = context.Vertices;
        }

        public IEnumerable<Vertex> GetAll() => _vertices;

        public Vertex GetById(int id) 
            => _vertices.Select(x => x).FirstOrDefault(x => x.Id == id);

        public void Add(Vertex entity)
        {
            _vertices.Add(entity);
        }

        public void Remove(Vertex entity)
        {
            _vertices.Remove(entity);
        }
    }
}
