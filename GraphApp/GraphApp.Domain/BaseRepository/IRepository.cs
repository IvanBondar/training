﻿using System.Collections.Generic;
using GraphApp.Domain.Model;

namespace GraphApp.Domain.BaseRepository
{

    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> GetAll();
        T GetById(int id);
        void Add(T entity);
        void Remove(T entity);
    }
}
