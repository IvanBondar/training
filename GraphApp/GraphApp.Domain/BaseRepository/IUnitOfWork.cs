﻿using GraphApp.Domain.Model;

namespace GraphApp.Domain.BaseRepository
{
    public interface IUnitOfWork
    {
        IRepository<Vertex> Vertices { get; }
        IRepository<Edge> Edges { get; }

        void Save();
    }
}
