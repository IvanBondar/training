﻿using System;
using Infrastructure.Absctractions;

namespace Infrastructure
{
    public class GraphLogger : ILogger
    {
        private static readonly NLog.Logger _logger;

        static GraphLogger()
        {
            _logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Debug(Exception ex, string message)
        {
            _logger.Debug(ex, message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Error(Exception ex, string message)
        {
            _logger.Error(ex, message);
        }

        public void Fatal(string message)
        {
            _logger.Fatal(message);
        }

        public void Fatal(Exception ex, string message)
        {
            _logger.Fatal(ex, message);
        }

        public void Info(string message)
        {
            _logger.Info(message);
        }

        public void Info(Exception ex, string message)
        {
            _logger.Info(ex, message);
        }

        public void Trace(string message)
        {
            _logger.Trace(message);
        }

        public void Trace(Exception ex, string message)
        {
            _logger.Trace(ex, message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }

        public void Warn(Exception ex, string message)
        {
            _logger.Warn(ex, message);
        }
    }
}
