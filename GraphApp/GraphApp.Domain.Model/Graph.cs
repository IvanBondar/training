﻿using System.Collections.Generic;

namespace GraphApp.Domain.Model
{
    public class Graph
    {
        public List<Vertex> Vertices { get; set; } = new List<Vertex>();
        public List<Edge> Edges { get; set; } = new List<Edge>();
    }
}
