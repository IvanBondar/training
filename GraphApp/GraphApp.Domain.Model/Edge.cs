﻿namespace GraphApp.Domain.Model
{
    public class Edge : Entity
    {
        public string Name { get; set; }
        public int V1 { get; set; }
        public int V2 { get; set; }
    }
}
