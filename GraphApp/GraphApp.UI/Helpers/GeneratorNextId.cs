﻿using System.Linq;
using GraphApp.JSON;

namespace GraphApp.UI.Helpers
{
    public class GeneratorNextId
    {
        private static int _startVertexId;
        private static int _startEdgeId;

        private static readonly GeneratorNextId _instance = new GeneratorNextId();

        static GeneratorNextId()
        {
            var context = new JsonContext();
            _startEdgeId = context.Edges.Select(x => x.Id).Max() + 1;
            _startVertexId = context.Vertices.Select(x => x.Id).Max() + 1;
        }

        private GeneratorNextId()
        { }

        public static GeneratorNextId GetInstance() => _instance;

        public int NextVertexId() => _startVertexId++;

        public int NextEdgeId() => _startEdgeId++;
    }
}
