﻿using GraphApp.Domain.BaseRepository;
using GraphApp.JSON;
using GraphApp.UI.Context;
using Infrastructure.Absctractions;
using Infrastructure;
using Ninject.Modules;

namespace GraphApp.UI.Config
{
    public class IocConfiguration : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().InSingletonScope();
            Bind<IContextUI>().To<JsonContextUI>().InSingletonScope();
            Bind<ILogger>().To<GraphLogger>();
        }
    }
}
