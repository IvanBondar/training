﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using GraphApp.UI.Model;
using Infrastructure.Absctractions;
using Newtonsoft.Json;

namespace GraphApp.UI.Context
{
    public class JsonContextUI : IContextUI
    {
        private readonly string _sourceUI;
        private readonly ILogger _logger;

        public List<Vertex> Vertices { get; set; }

        public JsonContextUI(ILogger logger)
        {
            _logger = logger;
            _sourceUI = ConfigurationManager.AppSettings["sourceUI"];
            Read();
        }

        public void Save(IEnumerable<Vertex> vertices)
        {
            var jsonGraph = JsonConvert.SerializeObject(vertices, Formatting.Indented);

            try
            {
                using (var sw = new StreamWriter(_sourceUI, false))
                {
                    sw.WriteLine(jsonGraph);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Something went wrong");
            }
        }

        private void Read()
        {
            if (!File.Exists(_sourceUI))
            {
                _logger.Info("File with UI configuration doesn't exist. Created empty List<Vertex>()");
                Vertices = new List<Vertex>();

                return;
            }

            try
            {
                using (var stream = new StreamReader(_sourceUI))
                {
                    var input = stream.ReadToEnd();
                    Vertices = JsonConvert.DeserializeObject<List<Vertex>>(input);
                }
            }
            catch (ArgumentNullException ex)
            {
                _logger.Error(ex, "Argument null exception");
            }
            catch (Exception ex)
            {
                _logger.Error(ex, "Something went wrong");
            }
            finally
            {
                Vertices = Vertices ?? new List<Vertex>();
            }
            
        }
    }
}
