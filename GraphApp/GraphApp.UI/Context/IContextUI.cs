﻿using System.Collections.Generic;
using GraphApp.UI.Model;

namespace GraphApp.UI.Context
{
    public interface IContextUI
    {
        List<Vertex> Vertices { get; set; }
        void Save(IEnumerable<Vertex> vertices);
    }
}
