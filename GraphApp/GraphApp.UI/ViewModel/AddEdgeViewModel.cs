﻿using GraphApp.UI.Command;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using GraphApp.Domain.BaseRepository;
using GraphApp.UI.Model;

namespace GraphApp.UI.ViewModel
{
    public class AddEdgeViewModel : BaseViewModel
    {
        private IUnitOfWork _uow;
        private RelayCommand _okCommand;
        private RelayCommand _cancelCommand;
        private bool? _dialogResult;
        private string _edgeName;
        private Vertex _selectedVertexFrom;
        private Vertex _selectedVertexTo;
        
        public List<Vertex> Vertices { get; set; }
        
        public bool? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                OnPropertyChanged(nameof(DialogResult));
            }
        }

        public Vertex SelectedVertexFrom
        {
            get { return _selectedVertexFrom; }
            set
            {
                _selectedVertexFrom = value;
                OnPropertyChanged(nameof(SelectedVertexFrom));
            }
        }

        public Vertex SelectedVertexTo
        {
            get { return _selectedVertexTo; }
            set
            {
                _selectedVertexTo = value;
                OnPropertyChanged(nameof(SelectedVertexTo));
            }
        }

        public string EdgeName
        {
            get { return _edgeName; }
            set
            {
                _edgeName = value;
                OnPropertyChanged(nameof(EdgeName));
            }
        }

        public RelayCommand OkCommand 
            => _okCommand ?? (_okCommand = new RelayCommand(obj => Save()));

        public RelayCommand CancelCommand 
            => _cancelCommand ?? (_cancelCommand = new RelayCommand(obj => Cancel()));

        public AddEdgeViewModel(IUnitOfWork uow)
        {
            _uow = uow;
            Vertices = _uow.Vertices.GetAll()
                .Select(x => new Vertex
                {
                    Name = x.Name,
                    Id = x.Id
                }).ToList();
        }

        private void Save()
        {
            if (_selectedVertexFrom == null)
            {
                MessageBox.Show("Please, select vertex from");
                return;
            }

            if (_selectedVertexTo == null)
            {
                MessageBox.Show("Please, select vertex to");
                return;
            }

            if (string.IsNullOrEmpty(_edgeName))
            {
                MessageBox.Show("Please, enter edge name");
                return;
            }

            DialogResult = true;
        }

        private void Cancel()
        {
            DialogResult = false;
        }
    }
}
