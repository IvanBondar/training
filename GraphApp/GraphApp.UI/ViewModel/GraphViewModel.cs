﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using GraphApp.Domain.BaseRepository;
using GraphApp.UI.Command;
using GraphApp.UI.Context;
using GraphApp.UI.Helpers;
using GraphApp.UI.Model;
using GraphApp.UI.View;

namespace GraphApp.UI.ViewModel
{
    public class GraphViewModel : BaseViewModel
    {
        private const double DefaultX = 100;
        private const double DefaultY = 100;
        private const double StepX = 50;
        private const double StepY = 50;
        private const int InLineVertexCount = 9;

        private readonly IUnitOfWork _uow;
        private readonly IContextUI _contextUi;
        private readonly GeneratorNextId _generatorId = GeneratorNextId.GetInstance();
        private RelayCommand _composeCommand;

        private Edge _selectedEdge;
        private RelayCommand _addEdgeCommand;
        private RelayCommand _removeEdgeCommand;

        private Vertex _selectedVertex;
        private RelayCommand _addVertexCommand;
        private RelayCommand _removeVertexCommand;

        public ObservableCollection<Vertex> Vertices { get; set; }
        public ObservableCollection<Edge> Edges { get; set; }

        public Vertex SelectedVertex
        {
            get
            {
                return _selectedVertex;
            }
            set
            {
                _selectedVertex = value;
                OnPropertyChanged(nameof(SelectedVertex));
            }
        }

        public Edge SelectedEdge
        {
            get
            {
                return _selectedEdge;
            }
            set
            {
                _selectedEdge = value;
                OnPropertyChanged(nameof(SelectedEdge));
            }
        }

        #region Vertex Tab Command

        public RelayCommand ComposeCommand
        {
            get
            {
                return _composeCommand ??
                       (_composeCommand = new RelayCommand(obj => Compose()));
            }
        }



        public RelayCommand AddVertexCommand
        {
            get
            {
                return _addVertexCommand ??
                       (_addVertexCommand = new RelayCommand(obj =>
                       {
                           var addNodeWindow = new AddVertexWindow();

                           if (addNodeWindow.ShowDialog() == false)
                           {
                               return;
                           }

                           var vertex = new Vertex
                           {
                               Name = addNodeWindow.AddNodeViewModel.VertexName,
                               Id = _generatorId.NextVertexId()
                           };

                           Vertices.Add(vertex);
                           AddVertex(vertex);
                           SelectedVertex = vertex;
                       }));
            }
        }

        public RelayCommand RemoveVertexCommand
        {
            get
            {
                return _removeVertexCommand ??
                       (_removeVertexCommand = new RelayCommand(obj =>
                       {
                           var selectedNode = obj as Vertex;

                           if (selectedNode == null)
                           {
                               MessageBox.Show("Select vertex");
                               return;
                           }

                           RemoveVertex(SelectedVertex.Id);
                           Vertices.Remove(selectedNode);
                       }));
            }
        }

        #endregion

        #region Edge Tab Command

        public RelayCommand AddEdgeCommand
        {
            get
            {
                return _addEdgeCommand ??
                       (_addEdgeCommand = new RelayCommand(obj =>
                       {
                           var addEdgeWindow = new AddEdgeWindow();

                           if (addEdgeWindow.ShowDialog() == false)
                           {
                               return;
                           }

                           var edge = new Edge
                           {
                               Id = _generatorId.NextEdgeId(),
                               Name = addEdgeWindow.AddEdgeViewModel.EdgeName,
                               Vertex1 = addEdgeWindow.AddEdgeViewModel.SelectedVertexFrom,
                               Vertex2 = addEdgeWindow.AddEdgeViewModel.SelectedVertexTo,
                           };

                           Edges.Add(edge);
                           AddEdge(edge);
                           SelectedEdge = edge;
                       }));
            }
        }
        public RelayCommand RemoveEdgeCommand
        {
            get
            {
                return _removeEdgeCommand ?? (_removeEdgeCommand = new RelayCommand(obj =>
                {
                    var selectedLink = obj as Edge;

                    if (selectedLink == null)
                    {
                        MessageBox.Show("Select edge");
                        return;
                    }

                    var edge = _uow.Edges.GetById(SelectedEdge.Id);
                    _uow.Edges.Remove(edge);
                    _uow.Save();
                    Edges.Remove(selectedLink);
                }));
            }
        }

        #endregion

        public GraphViewModel(IUnitOfWork uow, IContextUI contextUI)
        {
            _uow = uow;
            _contextUi = contextUI;

            Vertices = new ObservableCollection<Vertex>();
            Edges = new ObservableCollection<Edge>();

            var repoVerties = _uow.Vertices.GetAll();
            var repoEdges = _uow.Edges.GetAll();


            foreach (var vertex in repoVerties)
            {
                var x = _contextUi.Vertices.FirstOrDefault(u => u.Id == vertex.Id)?.X ?? DefaultX;
                var y = _contextUi.Vertices.FirstOrDefault(u => u.Id == vertex.Id)?.Y ?? DefaultY;

                Vertices.Add(new Vertex(x, y, vertex.Id, vertex.Name));

                if (_contextUi.Vertices.Count == 0)
                {
                    Compose();
                }
            }

            foreach (var edge in repoEdges)
            {
                var v1 = Vertices.FirstOrDefault(v => v.Id == edge.V1);
                var v2 = Vertices.FirstOrDefault(v => v.Id == edge.V2);

                if (v1 == null || v2 == null)
                {
                    continue;
                }

                Edges.Add(new Edge(v1, v2, edge.Id, edge.Name));
            }
        }

        public void AddVertex(Vertex vertex)
        {
            if (vertex == null)
            {
                return;
            }

            var domainVertex = new Domain.Model.Vertex
            {
                Id = vertex.Id,
                Name = vertex.Name
            };

            _uow.Vertices.Add(domainVertex);
            _uow.Save();
        }

        public void AddEdge(Edge edge)
        {
            if (edge == null)
            {
                return;
            }

            var domainEdge = new Domain.Model.Edge
            {
                Id = edge.Id,
                Name = edge.Name,
                V1 = edge.Vertex1.Id,
                V2 = edge.Vertex2.Id
            };

            _uow.Edges.Add(domainEdge);
            _uow.Save();
        }

        public void RemoveVertex(int id)
        {
            var domainNode = _uow.Vertices.GetById(id);
            var edges = _uow.Edges.GetAll().ToList();

            foreach (var edge in edges)
            {
                if (edge.V1 == domainNode.Id || edge.V2 == domainNode.Id)
                {
                    foreach (var item in Edges.ToList())
                    {
                        if (item.Id == edge.Id)
                        {
                            Edges.Remove(item);
                        }
                    }

                    _uow.Edges.Remove(edge);
                }
            }

            _uow.Vertices.Remove(domainNode);
            _uow.Save();
        }

        public void RemoveEdge(int id)
        {
            var domainEdge = _uow.Edges.GetById(id);

            if (domainEdge == null)
            {
                return;
            }

            _uow.Edges.Remove(domainEdge);
            _uow.Save();
        }

        /// <summary>
        /// Event handler for window closing event 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnWindowClosing(object sender, CancelEventArgs e)
        {
            _contextUi.Save(Vertices.ToList());
        }

        /// <summary>
        /// Method for composing vertices by fixed template
        /// </summary>
        private void Compose()
        {
            var currentY = StepY;
            var inLineCount = 1;

            foreach (var vertex in Vertices)
            {
                if (inLineCount == InLineVertexCount)
                {
                    inLineCount = 1;
                    currentY += StepY;
                }

                vertex.X = DefaultX + StepX * inLineCount++;
                vertex.Y = currentY;
            }
        }
    }
}