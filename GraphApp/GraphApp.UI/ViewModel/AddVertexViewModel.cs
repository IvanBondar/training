﻿using System.Windows;
using GraphApp.UI.Command;

namespace GraphApp.UI.ViewModel
{
    public class AddVertexViewModel : BaseViewModel
    {
        private RelayCommand _okCommand;
        private RelayCommand _cancelCommand;
        private bool? _dialogResult;
        private string _vertexName;

        public bool? DialogResult
        {
            get { return _dialogResult; }
            set
            {
                _dialogResult = value;
                OnPropertyChanged(nameof(DialogResult));
            }
        }

        public string VertexName
        {
            get
            {
                return _vertexName;
            }
            set
            {
                _vertexName = value;
                OnPropertyChanged(nameof(VertexName));
            }
        }

        public RelayCommand OkCommand
            => _okCommand ?? (_okCommand = new RelayCommand(obj => Save()));

        public RelayCommand CancelCommand
            => _cancelCommand ?? (_cancelCommand = new RelayCommand(obj => Cancel()));

        private void Save()
        {
            if (string.IsNullOrEmpty(_vertexName))
            {
                MessageBox.Show("Please, enter vertex name");
                return;
            }

            DialogResult = true;
        }

        private void Cancel()
        {
            DialogResult = false;
        }
    }
}
