﻿using System.Windows;
using Newtonsoft.Json;

namespace GraphApp.UI.Model
{
    public class Vertex : Entity
    {
        public const double ImageSize = 32;
        private double _x;
        private double _y;
        private Point _connectionPoint;
        private string _name;

        #region Properties

        [JsonIgnore]
        public Point ConnectionPoint
        {
            get
            {
                return _connectionPoint;
                
            }
            set
            {
                _connectionPoint = value;
                OnPropertyChanged();
            }
        }

        public double X
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
                OnPropertyChanged();
            }
        }

        public double Y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
                OnPropertyChanged();
            }
        }

        [JsonIgnore]
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged();
            }
        }

        #endregion

        public Vertex()
        {
        }

        public Vertex(double x, double y, int id, string name)
        {
            _x = x;
            _y = y;
            Id = id;
            Name = name;
        }
    }
}
