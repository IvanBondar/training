﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace GraphApp.UI.Model
{
    public class ConnectorItem : ContentControl
    {
        public static readonly DependencyProperty HotspotProperty =
            DependencyProperty.Register("Hotspot", typeof(Point), typeof(ConnectorItem));

        public static readonly DependencyProperty AncestorProperty =
            DependencyProperty.Register("Ancestor", typeof(FrameworkElement), typeof(ConnectorItem),
                new FrameworkPropertyMetadata(Ancestor_PropertyChanged));

        public ConnectorItem()
        {
            Focusable = false;
            LayoutUpdated += ConnectorItem_LayoutUpdated;
            SizeChanged += ConnectorItem_SizeChanged;
        }

        public Point Hotspot
        {
            get { return (Point) GetValue(HotspotProperty); }
            set { SetValue(HotspotProperty, value); }
        }

        public FrameworkElement Ancestor
        {
            get { return (FrameworkElement) GetValue(AncestorProperty); }
            set { SetValue(AncestorProperty, value); }
        }


        private static void Ancestor_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var c = (ConnectorItem) d;
            c.UpdateHotspot();
        }

        private void ConnectorItem_LayoutUpdated(object sender, EventArgs e)
        {
            UpdateHotspot();
        }

        private void ConnectorItem_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            UpdateHotspot();
        }

        private void UpdateHotspot()
        {
            if (Ancestor == null)
            {
                return;
            }

            var center = new Point(ActualWidth / 2, ActualHeight / 2);

            if (IsVisible)
            {
                var centerRelativeToAncestor = TransformToAncestor(Ancestor).Transform(center);
                Hotspot = centerRelativeToAncestor;
            }
        }
    }
}