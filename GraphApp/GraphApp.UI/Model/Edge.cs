﻿namespace GraphApp.UI.Model
{
    public class Edge : Entity
    {
        private string _name;
        private Vertex _vertex1;
        private Vertex _vertex2;

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                OnPropertyChanged(nameof(Name));
            }
        }


        public Vertex Vertex1
        {
            get
            {
                return _vertex1;
            }

            set
            {
                _vertex1 = value;
                OnPropertyChanged(nameof(Vertex1));
            }
        }

        public Vertex Vertex2
        {
            get
            {
                return _vertex2;
            }

            set
            {
                _vertex2 = value;
                OnPropertyChanged(nameof(Vertex2));
            }
        }

        public Edge()
        {
        }

        public Edge(Vertex vertex1, Vertex vertex2, int id, string name)
        {
            _vertex1 = vertex1;
            _vertex2 = vertex2;
            Name = name;
            Id = id;
        }
    }
}
