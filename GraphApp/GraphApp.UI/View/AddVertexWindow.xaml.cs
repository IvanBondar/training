﻿using System.Windows;
using GraphApp.UI.ViewModel;

namespace GraphApp.UI.View
{
    public partial class AddVertexWindow : Window
    {
        public AddVertexViewModel AddNodeViewModel { get; set; } 

        public AddVertexWindow()
        {
            InitializeComponent();
            AddNodeViewModel = new AddVertexViewModel();
            DataContext = AddNodeViewModel;
        }
    }
}
