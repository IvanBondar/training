﻿using System;
using System.Linq;
using GraphApp.UI.ViewModel;
using System.Windows;
using GraphApp.UI.Config;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows.Input;
using GraphApp.UI.Helpers;
using GraphApp.UI.Model;

namespace GraphApp.UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly GraphViewModel _graphViewModel;
        private readonly GeneratorNextId _generatorId = GeneratorNextId.GetInstance();

        private const double Epsilon = 3;
        private const double Radius = 16;
        // flag for enabling "Remove vertex" mode
        private bool _isRemoveVertex;
        // flag for enabling "Remove link" mode
        private bool _isRemoveEdge;
        // flag for enabling "New vertex" mode
        private bool _isAddNewVertex;
        // flag for enabling "New link" mode
        private bool _isAddNewEdge;
        // flag that indicates that the link drawing with a mouse started
        private bool _isLinkStarted;
        // variable to hold the vertex drawing started from
        private Vertex _linkedVertex;
        // Line drawn by the mouse before connection established
        private LineGeometry _link;

        public MainWindow()
        {
            _graphViewModel = IocKernel.Get<GraphViewModel>();
            DataContext = _graphViewModel;
            InitializeComponent();

            Closing += _graphViewModel.OnWindowClosing;
            PreviewMouseLeftButtonDown += MainWindow_PreviewMouseLeftButtonDown;
            PreviewMouseMove += MainWindow_PreviewMouseMove;
            PreviewMouseLeftButtonUp += MainWindow_PreviewMouseLeftButtonUp;
        }

        /// <summary>
        /// Event hanlder for dragging functionality support 
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e"></param>
        private void OnDragDelta(object sender, DragDeltaEventArgs e)
        {
            if (_isAddNewEdge)
            {
                return;
            }

            var thumb = (Thumb)sender;
            var vertex = (Vertex)thumb.DataContext;
            vertex.X += e.HorizontalChange;
            vertex.Y += e.VerticalChange;
        }

        /// <summary>
        /// Event handler for creating new vertex element by left mouse click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (_isRemoveVertex && e.Source.GetType() == typeof(ItemsControl))
            {
                RemoveUiVertex(e);
            }

            if (_isRemoveEdge && e.Source.GetType() == typeof(ItemsControl))
            {
                RemoveUiEdge(e);
            }

            if (_isAddNewEdge && e.Source.GetType() == typeof(ItemsControl))
            {
                if (!_isLinkStarted)
                {
                    AddNewUiEdge(e);
                }
            }

            if (_isAddNewVertex)
            {
                AddUiVertex(e);
            }
        }

        /// <summary>
        /// Handles the mouse move event when dragging/drawing the new connection link
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (!_isAddNewEdge || !_isLinkStarted)
            {
                return;
            }

            _link.EndPoint = e.GetPosition(GraphCanvas);
            e.Handled = true;
        }

        /// <summary>
        /// Handles the mouse up event applying the new connection link or resetting it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (!_isAddNewEdge || !_isLinkStarted)
            {
                return;
            }

            if (e.Source.GetType() == typeof(ItemsControl))
            {
                var point = e.GetPosition(GraphCanvas);
                foreach (var vertex in _graphViewModel.Vertices)
                {
                    if (!IsWithinCircle(vertex.ConnectionPoint, point, 16))
                    {
                        continue;
                    }

                    if (_linkedVertex != vertex)
                    {
                        var id = _generatorId.NextEdgeId();
                        var edge = new Edge(_linkedVertex, vertex, id, $"e{id}_v{_linkedVertex.Id}_v{vertex.Id}");

                        _graphViewModel.AddEdge(edge);
                        _graphViewModel.Edges.Add(edge);
                    }

                    break;
                }
            }

            connectors.Children.Remove(_link);
            _link = null;

            _isLinkStarted = _isAddNewEdge = false;
            ButtonNewVertex.IsEnabled = ButtonNewLink.IsEnabled = true;
            Mouse.OverrideCursor = null;
            e.Handled = true;
        }

        private void AddUiVertex(MouseEventArgs e)
        {
            var point = e.GetPosition(GraphCanvas);
            var id = _generatorId.NextVertexId();
            var vertex = new Vertex(point.X - 15, point.Y - 15, id, $"v{id}");
            _graphViewModel.Vertices.Add(vertex);
            _graphViewModel.AddVertex(vertex);

            _isAddNewVertex = false;
            Mouse.OverrideCursor = null;
            ButtonNewVertex.IsEnabled = ButtonNewLink.IsEnabled = true;
            e.Handled = true;
        }

        private void AddNewUiEdge(MouseEventArgs e)
        {
            if (_link != null && _link.EndPoint == _link.StartPoint)
            {
                return;
            }

            var position = e.GetPosition(GraphCanvas);
            _link = new LineGeometry(position, position);
            connectors.Children.Add(_link);
            _isLinkStarted = true;

            foreach (var vertex in _graphViewModel.Vertices)
            {
                if (!IsWithinCircle(vertex.ConnectionPoint, position, Radius))
                {
                    continue;
                }

                _linkedVertex = vertex;
                break;
            }

            e.Handled = true;
        }

        private void RemoveUiEdge(MouseEventArgs e)
        {
            var position = e.GetPosition(GraphCanvas);
            foreach (var edge in _graphViewModel.Edges.ToList())
            {
                if (!IsPointOnLine(edge.Vertex1.ConnectionPoint, edge.Vertex2.ConnectionPoint, position))
                {
                    continue;
                }

                _graphViewModel.RemoveEdge(edge.Id);
                _graphViewModel.Edges.Remove(edge);

                break;
            }

            _isRemoveEdge = false;
            ButtonRemoveEdge.IsEnabled = true;
            UpdateLayout();
        }

        private void RemoveUiVertex(MouseEventArgs e)
        {
            var point = e.GetPosition(GraphCanvas);

            foreach (var vertex in _graphViewModel.Vertices.ToList())
            {
                if (!IsWithinCircle(vertex.ConnectionPoint, point, 16))
                {
                    continue;
                }

                foreach (var edge in _graphViewModel.Edges.ToList())
                {
                    if (edge.Vertex1.Id != vertex.Id && edge.Vertex2.Id != vertex.Id)
                    {
                        continue;
                    }

                    _graphViewModel.RemoveEdge(edge.Id);
                    _graphViewModel.Edges.Remove(edge);
                }

                _graphViewModel.RemoveVertex(vertex.Id);
                _graphViewModel.Vertices.Remove(vertex);

                break;
            }

            _isRemoveVertex = false;
            ButtonNewVertex.IsEnabled = true;
        }

        /// <summary>
        /// Event handler for enabling new vertex creation by left mouse button click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonNewNode_Click(object sender, RoutedEventArgs e)
        {
            _isAddNewVertex = true;
            Mouse.OverrideCursor = Cursors.SizeAll;
            ButtonNewVertex.IsEnabled = ButtonNewLink.IsEnabled = false;
        }

        /// <summary>
        /// Event handler for enabling new edge creation by left mouse button click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonNewLink_Click(object sender, RoutedEventArgs e)
        {
            _isAddNewEdge = true;
            Mouse.OverrideCursor = Cursors.Cross;
            ButtonNewVertex.IsEnabled = ButtonNewLink.IsEnabled = false;
        }

        /// <summary>
        /// Event handler for enabling remove vertex by left mouse button click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRemoveVertex_Click(object sender, RoutedEventArgs e)
        {
            ButtonNewVertex.IsEnabled = false;
            _isRemoveVertex = true;
        }

        /// <summary>
        /// Event handler for enabling remove edge left mouse button click 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonRemoveEdge_Click(object sender, RoutedEventArgs e)
        {
            ButtonRemoveEdge.IsEnabled = false;
            _isRemoveEdge = true;
        }

        /// <summary>
        /// Check that point is within circle
        /// </summary>
        /// <param name="center">circle center</param>
        /// <param name="mouse">mouse position point</param>
        /// <param name="radius">circle radius</param>
        /// <returns>true if point within circle</returns>
        private bool IsWithinCircle(Point center, Point mouse, double radius)
        {
            var diffX = center.X - mouse.X;
            var diffY = center.Y - mouse.Y;
            return diffX * diffX + diffY * diffY <= radius * radius;
        }

        /// <summary>
        /// Check that point is on Line
        /// </summary>
        /// <param name="endPoint1">first line point</param>
        /// <param name="endPoint2">second line point</param>
        /// <param name="point">point for check</param>
        /// <returns>true if point on line</returns>
        private bool IsPointOnLine(Point endPoint1, Point endPoint2, Point point)
        {
            Point leftPoint;
            Point rightPoint;

            if (endPoint1.X <= endPoint2.X)
            {
                leftPoint = endPoint1;
                rightPoint = endPoint2;
            }
            else
            {
                leftPoint = endPoint2;
                rightPoint = endPoint1;
            }

            if (point.X + Epsilon < leftPoint.X ||
                rightPoint.X < point.X - Epsilon)
            {
                return false;
            }

            if (point.Y + Epsilon < Math.Min(leftPoint.Y, rightPoint.Y) ||
                Math.Max(leftPoint.Y, rightPoint.Y) < point.Y - Epsilon)
            {
                return false;
            }

            var deltaX = rightPoint.X - leftPoint.X;
            var deltaY = rightPoint.Y - leftPoint.Y;

            if (deltaX == 0 || deltaY == 0)
            {
                return true;
            }

            var slope = deltaY / deltaX;
            var offset = leftPoint.Y - leftPoint.X * slope;
            var calculatedY = point.X * slope + offset;

            var result = point.Y - Epsilon <= calculatedY && calculatedY <= point.Y + Epsilon;

            return result;
        }
    }
}
