﻿using GraphApp.UI.Config;
using GraphApp.UI.ViewModel;
using System.Windows;

namespace GraphApp.UI.View
{
    /// <summary>
    /// Interaction logic for AddLinkWindow.xaml
    /// </summary>
    public partial class AddEdgeWindow : Window
    {
        public AddEdgeViewModel AddEdgeViewModel { get; set; }

        public AddEdgeWindow()
        {
            AddEdgeViewModel = IocKernel.Get<AddEdgeViewModel>();
            DataContext = AddEdgeViewModel;
            InitializeComponent();
        }
    }
}
