USE [NET06_2]
GO

INSERT INTO [dbo].[ChannelType] VALUES('first');
INSERT INTO [dbo].[ChannelType] VALUES('second');
INSERT INTO [dbo].[ChannelType] VALUES('third');


INSERT INTO [dbo].[Person] VALUES('person1')
INSERT INTO [dbo].[Person] VALUES('person2')
INSERT INTO [dbo].[Person] VALUES('person3')

INSERT INTO [dbo].[Channel] VALUES('channel1', 1);
INSERT INTO [dbo].[Channel] VALUES('channel2', 2);
INSERT INTO [dbo].[Channel] VALUES('channel3', 3);

GO

INSERT INTO [dbo].[Session] VALUES('2017-05-01', '2017-05-10', 1);
INSERT INTO [dbo].[Session] VALUES('2017-06-02', '2017-06-20', 2);
INSERT INTO [dbo].[Session] VALUES('2017-07-03', '2017-07-30', 3);
INSERT INTO [dbo].[Session] VALUES('2017-08-01', '2017-08-10', NULL);
INSERT INTO [dbo].[Session] VALUES('2017-09-02', '2017-09-20', NULL);
INSERT INTO [dbo].[Session] VALUES('2017-10-03', '2017-10-30', NULL);

GO

INSERT INTO [dbo].[SessionChannel] ([SessionId],[ChannelId]) VALUES(1, 1)
INSERT INTO [dbo].[SessionChannel] VALUES(2, 3)
INSERT INTO [dbo].[SessionChannel] VALUES(3, 2)
INSERT INTO [dbo].[SessionChannel] VALUES(4, 1)
INSERT INTO [dbo].[SessionChannel] VALUES(4, 2)
INSERT INTO [dbo].[SessionChannel] VALUES(5, 3)
INSERT INTO [dbo].[SessionChannel] VALUES(6, 1)
INSERT INTO [dbo].[SessionChannel] VALUES(6, 2)

GO

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(100, '12:05:04', 'm3', 1);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(200, '12:15:04', 'm3', 2);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(300, '12:25:04', 'm3', 3);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(400, '12:35:04', 'm3', 4);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(500, '12:45:04', 'm3', 5);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(600, '12:55:04', 'm3', 6);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(700, '13:05:04', 'm3', 7);

INSERT INTO [dbo].[Measurement]([Value] ,[TimeStamp] ,[Unit], [SessionChannelId])
VALUES(800, '13:15:04', 'm3', 8);

GO