USE [NET06_2]
GO

IF OBJECT_ID('dbo.getMeasurements', 'P') IS NOT NULL
    DROP PROCEDURE [dbo].[getMeasurements];

GO

CREATE PROCEDURE [dbo].[getMeasurements]
@startIndex INT
, @pageSize INT
, @sortExpression VARCHAR(20) = NULL
, @sessionID INT = 0 --null
, @channelID INT = 0 --null
AS
BEGIN
	IF (@startIndex IS NULL) OR (@startIndex <= 0)
	BEGIN
	   PRINT 'ERROR: You must set startIndex parameter > 0.'
	   RETURN
	END
	
	IF (@pageSize IS NULL) OR (@pageSize <= 0)
	BEGIN
	   PRINT 'ERROR: You must set pageSize parameter > 0.'
	   RETURN
	END

	IF @sessionID <= 0
	BEGIN
	   PRINT 'ERROR: sessionID must by > 0.'
	   RETURN
	END
	
	IF @channelID <= 0
	BEGIN
	   PRINT 'ERROR: channelID must by > 0.'
	   RETURN
	END

	SET NOCOUNT ON;
	
	SELECT [dbo].[Measurement].MeasurementId, [dbo].[Measurement].Value, [dbo].[Measurement].[TimeStamp], [dbo].[Measurement].Unit, [dbo].[Measurement].SessionChannelId,
			[dbo].[Channel].ChannelTypeId , [dbo].[Channel].ChannelId, [dbo].[Channel].Name AS Channel_Name,
			[dbo].[Session].SessionId, [dbo].[Session].StartTime, [dbo].[Session].EndTime,
			[dbo].[Person].PersonId, [dbo].[Person].Name AS Person_Name
	FROM [dbo].[Measurement]
	JOIN [dbo].[SessionChannel]	
	ON [dbo].[Measurement].SessionChannelId = [dbo].[SessionChannel].SessionChannelId
	JOIN [dbo].[Channel]
	ON [dbo].[Channel].ChannelId = [dbo].[SessionChannel].ChannelId
	JOIN [dbo].[Session]
	ON [dbo].[Session].SessionId = [dbo].[SessionChannel].SessionId
	LEFT JOIN [dbo].[Person]
	ON [dbo].[Person].PersonId = [dbo].[Session].PersonId
	WHERE [dbo].[SessionChannel].SessionId = COALESCE(NULLIF(@sessionID, 0), [dbo].[SessionChannel].SessionId) 
		AND [dbo].[SessionChannel].ChannelId = COALESCE(NULLIF(@channelId, 0), [dbo].[SessionChannel].ChannelId)
	ORDER BY 
     CASE WHEN @sortExpression = 'Name ASC' THEN [dbo].[Channel].Name END ASC,
	 CASE WHEN @sortExpression = 'Name DESC' THEN [dbo].[Channel].Name END DESC,
	 CASE WHEN @sortExpression = 'TimeStamp ASC' THEN [dbo].[Measurement].[TimeStamp] END ASC,
	 CASE WHEN @sortExpression = 'TimeStamp DESC' THEN [dbo].[Measurement].[TimeStamp] END DESC,
	 CASE WHEN @sortExpression = 'SessionId ASC' THEN [dbo].[SessionChannel].SessionId END ASC,
	 CASE WHEN @sortExpression = 'SessionId DESC' THEN [dbo].[SessionChannel].SessionId END DESC
	OFFSET @startIndex - 1 ROW FETCH NEXT @pageSize ROWS ONLY
END

GO