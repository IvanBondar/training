USE [NET06_2]

GO 

IF EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'trigger_ModifySession'))
DROP TRIGGER trigger_ModifySession

GO

CREATE TRIGGER trigger_ModifySession
    ON [dbo].[Session] AFTER INSERT, UPDATE, DELETE
    AS
BEGIN
	DECLARE @sessionIdOld INT
	DECLARE @startTimeOld DATE
	DECLARE @endTimeOld DATE
	DECLARE @personIdOld INT

	DECLARE @sessionIdNew INT
	DECLARE @startTimeNew DATE
	DECLARE @endTimeNew DATE
	DECLARE @personIdNew INT

	SELECT @sessionIdOld = (SELECT SessionId FROM deleted)
	SELECT @startTimeOld = (SELECT StartTime FROM deleted)
	SELECT @endTimeOld = (SELECT EndTime FROM deleted)
	SELECT @personIdOld = (SELECT PersonId FROM deleted)

	SELECT @sessionIdNew = (SELECT SessionId FROM inserted)
	SELECT @startTimeNew = (SELECT StartTime FROM inserted)
	SELECT @endTimeNew = (SELECT EndTime FROM inserted)
	SELECT @personIdNew = (SELECT PersonId FROM inserted)

	IF @sessionIdNew != @sessionIdOld 
		OR @startTimeNew != @startTimeOld 
		OR @endTimeNew != @endTimeOld
		OR @personIdNew != @personIdOld 
	INSERT INTO [dbo].[History] VALUES(@sessionIdOld, @startTimeOld, @endTimeOld, @personIdOld)
END

GO