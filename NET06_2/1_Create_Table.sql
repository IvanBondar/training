USE [NET06_2]
GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'ChannelType')
BEGIN
CREATE TABLE [dbo].[ChannelType](
	[ChannelTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_ChannelType] PRIMARY KEY CLUSTERED 
(
	[ChannelTypeId] ASC
))
END

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'Person')
BEGIN
CREATE TABLE [dbo].[Person](
	[PersonId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Person] PRIMARY KEY CLUSTERED 
(
	[PersonId] ASC
))
END

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'Channel')
BEGIN
CREATE TABLE [dbo].[Channel](
	[ChannelId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
	[ChannelTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Channel] PRIMARY KEY CLUSTERED 
(
	[ChannelId] ASC
),
 CONSTRAINT [FK_Channel_ChannelType] FOREIGN KEY([ChannelTypeId])
 REFERENCES [dbo].[ChannelType] ([ChannelTypeId])
)

IF NOT EXISTS (SELECT name from sys.indexes
             WHERE name = N'IX_Channel_ChannelTypeId')
 CREATE INDEX IX_Channel_ChannelTypeId ON [dbo].[Channel]
 (  
	 [ChannelTypeId] ASC
 )

END

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'Session')
BEGIN
CREATE TABLE [dbo].[Session](
	[SessionId] [int] IDENTITY(1,1) NOT NULL,
	[StartTime] [date] NOT NULL,
	[EndTime] [date] NOT NULL,
	[PersonId] [int] NULL,
 CONSTRAINT [PK_Session] PRIMARY KEY CLUSTERED 
(
	[SessionId] ASC
),
 CONSTRAINT [FK_Session_Person] FOREIGN KEY([PersonId])
 REFERENCES [dbo].[Person] ([PersonId]),
 CONSTRAINT [CK_ProjectCheckDate] CHECK  (([EndTime]>[StartTime]))
 )
 IF NOT EXISTS (SELECT name from sys.indexes
             WHERE name = N'IX_Session_PersonId')
  CREATE INDEX IX_Session_PersonId ON [dbo].[Session]
  (
 	 [PersonId] ASC
  )

END

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'SessionChannel')
BEGIN
CREATE TABLE [dbo].[SessionChannel](
	[SessionChannelId] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [int] NOT NULL,
	[ChannelId] [int] NOT NULL,
 CONSTRAINT [PK_SessionChannel] PRIMARY KEY CLUSTERED 
(
	[SessionChannelId] ASC
),
 CONSTRAINT [FK_SessionChannel_Channel] FOREIGN KEY([ChannelId])
 REFERENCES [dbo].[Channel] ([ChannelId]),
 CONSTRAINT [FK_SessionChannel_Session] FOREIGN KEY([SessionId])
 REFERENCES [dbo].[Session] ([SessionId]),
CONSTRAINT [IX_UniqueSessionId_ChannelId] UNIQUE NONCLUSTERED 
(
	[SessionId] ASC, [ChannelId] ASC
))
END

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'Measurement')
BEGIN
CREATE TABLE [dbo].[Measurement](
	[MeasurementId] [int] IDENTITY(1,1) NOT NULL,
	[Value] [int] NOT NULL,
	[TimeStamp] [time](0) NOT NULL,
	[Unit] [varchar](10) NOT NULL,
	[SessionChannelId] [int] NOT NULL,
 CONSTRAINT [PK_Measurement] PRIMARY KEY CLUSTERED 
(
	[MeasurementId] ASC
),
 CONSTRAINT [FK_Measurement_SessionChannel] FOREIGN KEY([SessionChannelId])
 REFERENCES [dbo].[SessionChannel] ([SessionChannelId])
)
IF NOT EXISTS (SELECT name from sys.indexes
             WHERE name = N'IX_Measurement_SessionChannelId')
 CREATE INDEX IX_Measurement_SessionChannelId ON [dbo].[Measurement]
 (
	[SessionChannelId] ASC
 )

END

GO

IF NOT EXISTS (SELECT * FROM sysobjects WHERE name = 'History')
BEGIN
CREATE TABLE [dbo].[History](
	[HistoryId] [int] IDENTITY(1,1) NOT NULL,
	[SessionId] [int] NOT NULL,
	[StartTime] [date] NOT NULL,
	[EndTime] [date] NOT NULL, 
	[PersonId] int NULL,
 CONSTRAINT [PK_History] PRIMARY KEY CLUSTERED 
(
	[HistoryId] ASC
))
END

GO

