﻿namespace NET02_3.ConfigEntity
{
    public class Window
    {
        public string Title { get; set; }
        public int? Top { get; set; }
        public int? Left { get; set; }
        public int? Width { get; set; }
        public int? Height { get; set; }

        public void RepairWindowParams()
        {
            Top = Top ?? 0;
            Left = Left ?? 0;
            Width = Width ?? 400;
            Height = Height ?? 150;
        }
    }
}
