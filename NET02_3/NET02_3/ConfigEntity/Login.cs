﻿using NET02_3.Infrastructure.Helpers;
using System;
using System.Collections.Generic;

namespace NET02_3.ConfigEntity
{
    public class Login
    {
        public string Name { get; set; }
        public List<Window> Windows { get; set; }

        public Login(IEnumerable<Window> windows)
        {
            Check.NotNull(windows, nameof(windows));

            Windows = new List<Window>(windows);
        }

        public void RepairLogin()
        {
            var mainWindow = GetMainWindow();

            if(mainWindow == null)
            {
                Windows.Insert(0, new Window()
                {
                    Title = "main",
                    Top = 0,
                    Left = 0,
                    Width = 400,
                    Height = 150
                });
            }

            

        }

        public Window GetMainWindow() => Windows.Find(x => x.Title == "main");
    }
}
