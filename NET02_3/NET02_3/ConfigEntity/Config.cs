﻿using NET02_3.Infrastructure.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace NET02_3.ConfigEntity
{
    public class Config
    {
        private static List<string> _windowObjectProperties;

        public List<Login> Logins { get; set; }

        public Config(IEnumerable<Login> logins)
        {
            Check.NotNull(logins, nameof(logins));

            Logins = new List<Login>(logins);
            var type = typeof(Window);
            _windowObjectProperties = type.GetProperties().Select(x => x.Name).ToList();
        }

        public List<string> GetLoginsWhichHaveIncorrectConfig()
        {
            var result = new List<string>();

            foreach (var login in Logins)
            {
                var mainWindow = login.GetMainWindow();

                if (mainWindow == null)
                {
                    result.Add(login.Name);
                }
                else
                {
                    var type = typeof(Window);

                    foreach (var item in _windowObjectProperties)
                    {
                        var property = type.GetProperty(item);

                        if (property.GetValue(mainWindow) == null)
                        {
                            result.Add(login.Name);
                        }
                    }
                }
            }

            return result;
        }
        
        public void RepairConfig()
        {
            foreach (var login in Logins)
            {
                var mainWindow = login.GetMainWindow();

                if (mainWindow == null)
                {
                    login.RepairLogin();
                }
                else
                {
                    mainWindow.RepairWindowParams();
                }
            }
        }
    }
}
