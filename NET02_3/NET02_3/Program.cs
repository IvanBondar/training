﻿using System.Configuration;
using System.IO;

namespace NET02_3
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = ConfigurationManager.AppSettings["path"];
            string configName = ConfigurationManager.AppSettings["configName"];
            string fullPath = Path.Combine(path, configName);

            var configManager = new ConfigManager();
            var config = configManager.Read(fullPath);
            var logins = config.GetLoginsWhichHaveIncorrectConfig();
            config.RepairConfig();
            configManager.Write(config, path);
        }
    }
}
