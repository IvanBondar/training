﻿using System;

namespace NET02_3.Infrastructure.Helpers
{
    public static class Check
    {
        private const string DefaultNotNullMessage = "Value cannot be null!";
        private const string DefaultNotNullOrEmptyMessage = "Value cannot be null or empty!";
        private const string DefaultNotNullOrWhiteSpaceMessage = "Value cannot be null/empty/white space!";

        public static void NotNull(object value, string paramName)
        {
            NotNull(value, paramName, DefaultNotNullMessage);
        }

        public static void NotNullOrEmpty(string value, string paramName)
        {
            NotNullOrEmpty(value, paramName, DefaultNotNullMessage);
        }

        public static void NotNullOrWhiteSpace(string value, string paramName)
        {
            NotNullOrWhiteSpace(value, paramName, DefaultNotNullOrWhiteSpaceMessage);
        }

        public static void NotNull(object value, string paramName, string message)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName, message);
            }
        }

        public static void NotNullOrEmpty(string value, string paramName, string message)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(message, paramName);
            }
        }

        public static void NotNullOrWhiteSpace(string value, string paramName, string message)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException(message, paramName);
            }
        }
    }
}
