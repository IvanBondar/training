﻿using NET02_3.Abstraction;
using NET02_3.ConfigEntity;
using NET02_3.Infrastructure.Helpers;
using Newtonsoft.Json;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;

namespace NET02_3
{
    public class ConfigManager : IConfigManager
    {
        private static List<string> _windowObjectProperties;

        public ConfigManager()
        {
            var type = typeof(Window);
            _windowObjectProperties = type.GetProperties().Select(x => x.Name).ToList();
        }        

        public void Write(Config config, string path)
        {
            Check.NotNullOrWhiteSpace(path, nameof(path));

            foreach (var login in config.Logins)
            {
                var dir = Path.Combine(path, login.Name);
                Directory.CreateDirectory(dir);
                var fullPath = Path.Combine(dir, $"{login.Name}.json");
                File.WriteAllText(fullPath, ToJson(login));
            }
        }

        public Config Read(string path)
        {
            Check.NotNullOrWhiteSpace(path, nameof(path));

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Incorrect path or file doesn't exist", path);
            }

            var logins = new List<Login>();
            var config = new XmlDocument();
            config.Load(path);
            var xRoot = config.DocumentElement;

            foreach (XmlNode xnode in xRoot)
            {
                if (xnode.Attributes != null)
                {
                    logins.Add(GetLogin(xnode));
                }
            }

            return new Config(logins);
        }

        private Login GetLogin(XmlNode xmlLogin)
        {
            var windows = new List<Window>();

            foreach (XmlNode window in xmlLogin)
            {
                if (window.Attributes != null && window.LocalName == "window")
                {
                    windows.Add(GetWindow(window));
                }
            }

            var login = new Login(windows);
            var attr = xmlLogin.Attributes.GetNamedItem("name");

            login.Name = attr?.Value;

            return login;
        }

        private Window GetWindow(XmlNode xmlWindow)
        {
            var window = new Window();

            foreach (XmlNode position in xmlWindow.ChildNodes)
            {
                var windowProp = ToTitleCase(position.LocalName);

                if (_windowObjectProperties.Contains(windowProp))
                {
                    int item;
                    var validPosition = int.TryParse(position.InnerText, out item);

                    if (validPosition)
                    {
                        var type = typeof(Window);
                        var property = type.GetProperty(windowProp);
                        property.SetValue(window, item);
                    }
                }
            }

            var attr = xmlWindow.Attributes.GetNamedItem("title");
            window.Title = attr?.Value;

            return window;
        }

        private string ToJson(Login login) => JsonConvert.SerializeObject(login);

        private string ToTitleCase(string str)
            => CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
    }
}
