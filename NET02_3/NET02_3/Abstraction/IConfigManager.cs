﻿using NET02_3.ConfigEntity;

namespace NET02_3.Abstraction
{
    public interface IConfigManager
    {
        Config Read(string path);
        void Write(Config config, string path);
    }
}
