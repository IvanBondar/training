﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2.Infrastructure.Helpers;

namespace NET02_2.Tests
{
    [TestClass]
    public class CheckTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NotNull_ObjectIsNull_ThrowArgumentNullException()
        {
            Check.NotNull(null, "message");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NotNullOrEmprty_StringIsNull_ThrowArgumentException()
        {
            Check.NotNullOrEmpty(null, "message");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void NotNullOrEmprty_StringIsEmpty_ThrowArgumentException()
        {
            Check.NotNullOrEmpty(string.Empty, "message");
        }
    }
}
