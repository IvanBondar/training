﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2.Extensions;
using NET02_2.TestEntity;

namespace NET02_2.Tests
{
    [TestClass]
    public class StatisticExtensionsTests
    {
        [TestMethod]
        public void NameWithPassedTests_CalculateResult_ReturnDictionaryWithResult()
        {
            //arrange
            int totalPassedTests;
            var statistics = GetSmallStatistics();

            //act
            var result = statistics.NameWithPassedTests();

            //assert
            Assert.IsTrue(result.Count == 1);
            Assert.IsTrue(result.ContainsKey("Ivan Bondar"));
            Assert.IsTrue(result.TryGetValue("Ivan Bondar", out totalPassedTests));
            Assert.AreEqual(2, totalPassedTests);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NameWithPassedTests_StatisticsEqualNull_ThrowArgumentNullException()
        {
            //arrange
            Statistics statistics = null;

            //act
            statistics.NameWithPassedTests();
        }

        [TestMethod]
        public void TopTest_CalculateResult_ReturnTopTest()
        {
            //arrange
            var statistics = GetBigStatistics();

            //act
            var result = statistics.TopTest();

            //assert
            Assert.IsNotNull(result);
            Assert.AreEqual("t1", result.Name);
        }

        [TestMethod]
        public void TopTest_EmptyStatistics_ReturNull()
        {
            //arrange
            var statistics = new Statistics();

            //act
            var result = statistics.TopTest();

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TopTest_StatisticsEqualNull_ThrowArgumentNullException()
        {
            //arrange
            Statistics statistics = null;

            //act
            var result = statistics.TopTest();
        }

        [TestMethod]
        public void NameWithTotalRightAnswers_CalculateResult_ReturnDictionaryWithResult()
        {
            //arrange
            var statistic = GetSmallStatistics();
            int totalRightAnswers;

            //act
            var result = statistic.NameWithTotalRightAnswers();

            //assert
            Assert.IsTrue(result.Count == 1);
            Assert.IsTrue(result.TryGetValue("Ivan Bondar", out totalRightAnswers));
            Assert.AreEqual(2, totalRightAnswers);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void NameWithTotalRightAnswers_StaticticsEqualNull_ThrowArgumentNullException()
        {
            //arrange
            Statistics statistics = null;

            //act
            var result = statistics.NameWithTotalRightAnswers();
        }

        [TestMethod]
        public void QuestionWithMinRightAnswers_CalculateResult_ReturnDictionaryWithResult()
        {
            //arrange
            var statistic = GetSmallStatistics();

            //act
            var result = statistic.QuestionWithMinRightAnswers();

            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Question == "q2");
        }

        [TestMethod]
        public void QuestionWithMinRightAnswers_EmptyStatistics_ReturNull()
        {
            //arrange
            var statistics = new Statistics();

            //act
            var result = statistics.QuestionWithMinRightAnswers();

            //assert
            Assert.IsNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void QuestionWithMinRightAnswers_StaticticsEqualNull_ThrowArgumentNullException()
        {
            //arrange
            Statistics statistics = null;

            //act
            var result = statistics.QuestionWithMinRightAnswers();
        }


        private static Statistics GetSmallStatistics()
        {
            var q1 = new TestQuestion(1, "q1");
            var q2 = new TestQuestion(2, "q2");

            var a1 = new TestAnswer(q1, true);
            var a2 = new TestAnswer(q2, false);

            var t1 = new Test("t1", DateTime.Now);
            var t2 = new Test("t2", DateTime.Now);

            t1.AddAnswers(a1, a2);
            t2.AddAnswers(a1);

            var statistics = new Statistics();
            statistics.Add("Ivan Bondar", new List<Test> { t1, t2 });

            return statistics;
        }

        private static Statistics GetBigStatistics()
        {
            var q1 = new TestQuestion(1, "q1");
            var q2 = new TestQuestion(2, "q2");
            var q3 = new TestQuestion(3, "q3");

            var a1 = new TestAnswer(q1, true);
            var a2 = new TestAnswer(q2, false);
            var a3 = new TestAnswer(q2, true);
            var a4 = new TestAnswer(q3, false);

            var t1 = new Test("t1", DateTime.Now);
            var t2 = new Test("t2", DateTime.Now);

            t1.AddAnswers(a1, a2, a4);
            t2.AddAnswers(a1, a3, a4);

            var statistics = new Statistics();
            statistics.Add("Ivan Bondar", new List<Test> { t1, t2 });
            statistics.Add("Roman Fomin", new List<Test> { t1 });
            statistics.Add("Pupkin, Petr", new List<Test> { t1, t2 });

            return statistics;
        }
    }
}
