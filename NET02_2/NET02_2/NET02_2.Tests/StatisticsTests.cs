﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using NET02_2.TestEntity;

namespace NET02_2.Tests
{
    [TestClass]
    public class StatisticsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Add_KeyIsNull_ThrowArgumentNullException()
        {
            //arrange
            var statistics = new Statistics();

            //act
            statistics.Add(null, new List<Test>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Add_TestsIsNull_ThrowArgumentNullException()
        {
            //arrange
            var statistics = new Statistics();

            //act
            statistics.Add("test1", null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Add_TestsContainNullTest_ThrowArgumentNullException()
        {
            //arrange
            var statistics = new Statistics();
            var tests = new List<Test>()
            {
                new Test("t1", DateTime.Now),
                null
            };

            //act
            statistics.Add("Petr Pupkin", tests);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Add_TestsWithExistingKey_ThrowArgumentNullException()
        {
            //arrange
            var statistics = new Statistics();
            var tests = new List<Test>()
            {
                new Test("t1", DateTime.Now),
            };
            statistics.Add("Petr Pupkin", tests);

            //act
            statistics.Add("Petr Pupkin", tests);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Add_WrongKey_ThrowArgumentException()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);

            //act
            statistics.Add("key", tests);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Add_KeyExists_ThrowArgumentException()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);
            statistics.Add("Petr Pupkin", tests);

            //act
            statistics.Add("Petr Pupkin", tests);
        }

        [TestMethod]
        public void Add_ValuesAreCorrect_ValuesAdded()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);

            //act
            statistics.Add("Petr Pupkin", tests);

            //assert
            Assert.IsTrue(statistics.ContainsKey("Petr Pupkin"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Remove_KeyIsNull_ThrowArgumentNullException()
        {
            //arrange
            var statistics = new Statistics();

            //act
            statistics.Remove(null);
        }

        [TestMethod]
        public void Remove_ValueWithKeyExists_ValuesRemoved()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);
            statistics.Add("Petr Pupkin", tests);

            //act
            statistics.Remove("Petr Pupkin");

            //assert
            Assert.IsFalse(statistics.ContainsKey("Petr Pupkin"));
        }

        [TestMethod]
        public void Clear_CollectionCleared()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);
            statistics.Add("Petr Pupkin", tests);

            //act
            statistics.Clear();

            //assert
            Assert.IsTrue(statistics.GetStatistic().Count == 0);
        }

        [TestMethod]
        public void Containkey_KeyExists_ReturnTrue()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);
            statistics.Add("Petr Pupkin", tests);

            //act
            var result = statistics.ContainsKey("Petr Pupkin");

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Containkey_KeyExists_ReturnFalse()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);
            statistics.Add("Petr Pupkin", tests);

            //act
            var result = statistics.ContainsKey("Petr Pupkin");

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Containkey_NoKey_ReturnFalse()
        {
            //arrange
            var statistics = new Statistics();
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);
            var test = new Test("test1", DateTime.Now);
            test.AddAnswers(answer);
            var tests = new List<Test>();
            tests.Add(test);
            statistics.Add("Petr Pupkin", tests);

            //act
            var result = statistics.ContainsKey("Test Key");

            //assert
            Assert.IsFalse(result);
        }
    }
}
