﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2.TestEntity;

namespace NET02_2.Tests.TestEntityTests
{
    [TestClass]
    public class TestAnswerTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_QuestionIsNull_ThrowArgumentNullExceptio()
        {
            var test = new TestAnswer(null, true);
        }

        [TestMethod]
        public void Clone_ReturnClonedTestAnswer()
        {
            //arrange
            var question = new TestQuestion(1, "q1");
            var answer = new TestAnswer(question, true);

            //act
            var result = answer.Clone();

            //assert
            Assert.AreEqual(answer.Answer, result.Answer);
            Assert.AreEqual(answer.Question.Difficulty, result.Question.Difficulty);
            Assert.AreEqual(answer.Question.Question, result.Question.Question);
            Assert.AreEqual(answer.Question.Id, result.Question.Id);
        }
    }
}
