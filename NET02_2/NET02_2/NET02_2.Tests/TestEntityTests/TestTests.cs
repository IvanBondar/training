﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2.TestEntity;

namespace NET02_2.Tests.TestEntityTests
{
    [TestClass]
    public class TestTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NameIsNull_ThrowArgumentExceptio()
        {
            var test = new Test(null, DateTime.Now);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NameEmpty_ThrowArgumentExceptio()
        {
            var test = new Test(string.Empty, DateTime.Now);
        }

        [TestMethod]
        public void GetAnswers_ReturnAnswers()
        {
            //arrange
            var test = new Test("test1", DateTime.Now);
            var question = new TestQuestion(1, "question");
            var answer = new TestAnswer(question, true);
            test.AddAnswers(answer);

            //act
            var result = test.GetAnswers();
            
            //assert
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Count == 1);
        }

        [TestMethod]
        public void GetEnumerator_NotNull()
        {
            //arrange
            var test = new Test("test1", DateTime.Now);

            //act
            var result = test.GetEnumerator();

            //assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddAnswers_AnswersIsNull_ThrowArgumentNullExceptiot()
        {
            //arrange 
            var test = new Test("test1", DateTime.Now);

            //act
            test.AddAnswers(null);
        }

        [TestMethod]
        public void AddAnswers_AnswersExists_AnswerAddToList()
        {
            //arrange 
            var test = new Test("test1", DateTime.Now);
            var question = new TestQuestion(1, "question");
            var answer = new TestAnswer(question, true);

            //act
            test.AddAnswers(answer);

            //assert
            Assert.IsTrue(test.GetAnswers().Count == 1);
        }

        [TestMethod]
        public void AddAnswers_AnswersContainsNullAnswer_AnswerAddToList()
        {
            //arrange 
            var test = new Test("test1", DateTime.Now);
            var question = new TestQuestion(1, "question");
            var answer = new TestAnswer(question, true);

            //act
            test.AddAnswers(answer, null);

            //assert
            Assert.IsTrue(test.GetAnswers().Count == 1);
        }

        [TestMethod]
        public void Clone_ReturnClonedTest()
        {
            //arrange
            var test = new Test("test1", DateTime.Now);
            var question = new TestQuestion(1, "question");
            var answer = new TestAnswer(question, true);

            //act
            var result = test.Clone();

            //assert
            Assert.AreEqual(test.Name, result.Name);
            Assert.AreEqual(test.PassedAt, result.PassedAt);
            Assert.AreEqual(test.GetAnswers().Count, result.GetAnswers().Count);
        }

    }
}
