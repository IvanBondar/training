﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2.TestEntity;

namespace NET02_2.Tests.TestEntityTests
{
    [TestClass]
    public class TestQuestionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_QuestionIsNull_ThrowArgumentNullExceptio()
        {
            var test = new TestQuestion(1, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_QuestionIsEmprty_ThrowArgumentNullExceptio()
        {
            var test = new TestQuestion(1, string.Empty);
        }

        [TestMethod]
        public void Clone_ReturnClonedTestQuestion()
        {
            //arrange
            var question = new TestQuestion(1, "q1");

            //act
            var result = question.Clone();

            //assert
            Assert.IsTrue(question.Equals(result));
        }

        [TestMethod]
        public void Equals_ObjectsIsEqual_ReturnTrue()
        {
            //arrange
            var question = new TestQuestion(1, "1");

            //act
            var result = question.Equals(question);

            //assert
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void Equals_ObjectsHasDifferentId_ReturnFalse()
        {
            //arrange
            var question = new TestQuestion(1, "1");
            var question2 = new TestQuestion(1, "1");

            //act
            var result = question.Equals(question2);

            //assert
            Assert.IsFalse(result);
        }
    }
}
