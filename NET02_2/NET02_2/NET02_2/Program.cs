﻿using NET02_2.TestEntity;
using System;
using System.Collections.Generic;
using NET02_2.Extensions;

namespace NET02_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var q1 = new TestQuestion(1, "q1");
            var q2 = new TestQuestion(0, "q2");
            var q3 = new TestQuestion(9, "q3");
            var q4 = new TestQuestion(5, "q4");

            var a1 = new TestAnswer(q1, true);
            var a2 = new TestAnswer(q2, false);
            var a3 = new TestAnswer(q2, true);
            var a4 = new TestAnswer(q3, false);

            var t1 = new Test("t1", DateTime.Now);
            var t2 = new Test("t2", DateTime.Now);

            t1.AddAnswers(a1, a2, a4);
            t2.AddAnswers(a1, a3, a4);

            foreach (var item in t1)
            {
                Console.WriteLine($"Question {item.Question.Question} difficulty :{item.Question.Difficulty}.");
            }

            var ob1 = new object();
            var c1 = new Statistics();
            c1.Add("Ivan Bondar", new List<Test>() { t1, t2 });
            c1.Add("Roman Fomen", new List<Test> { t1, t2 });
            c1.Add("Petr, Pupkin", new List<Test> { t1, t2 });

            var stat1a = c1.NameWithPassedTests();
            var stat2a = c1.TopTest();
            var stat3a = c1.NameWithTotalRightAnswers();
            var stat4a = c1.QuestionWithMinRightAnswers();
        }
    }
}
