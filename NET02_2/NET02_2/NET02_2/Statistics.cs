﻿using NET02_2.TestEntity;
using System.Collections.Generic;
using System;
using System.Text.RegularExpressions;
using System.Linq;
using NET02_2.Infrastructure.Helpers;

namespace NET02_2
{
    public class Statistics
    {
        private readonly Dictionary<string, List<Test>> _collection;

        public Statistics()
        {
            _collection = new Dictionary<string, List<Test>>();
        }

        public Dictionary<string, List<Test>> GetStatistic()
        {
            var clone = _collection.Select(x => new
            {
                Key = x.Key,
                Tests = x.Value.Select(y => y.Clone()).ToList()
            }).ToDictionary(r => r.Key, r => r.Tests);

            return clone;
        }

        public void Add(string key, List<Test> tests)
        {
            Check.NotNull(key, "key cannot be null");
            Check.NotNull(tests, "tests cannot be null");

            if (ContainNullTest(tests))
            {
                throw new ArgumentNullException(nameof(tests), "Statistics collection can't have item equal null");
            }

            var adaptedKey = GetAdaptedKey(key);

            if (ContainsKey(adaptedKey))
            {
                throw new ArgumentException("An item with the same key has already been added.", nameof(key));
            }

            _collection.Add(adaptedKey, tests);
        }

        public void Remove(string key)
        {
            Check.NotNullOrEmpty(key, "key cannot be null/empty");

            _collection.Remove(key);
        }

        public List<Test> this[string key]
        {
            get
            {
                Check.NotNull(key, "key cannot be null");

                if (!ContainsKey(key))
                {
                    throw new ArgumentException(nameof(key), "There isn't values in collection with this key");
                }

                return _collection[key];
            }
            set
            {
                if (ContainNullTest(value))
                {
                    throw new ArgumentNullException(nameof(value), "Statistics collection can't have item equal null");
                }

                _collection[key] = value;
            }
        }

        public void Clear()
        {
            _collection.Clear();
        }

        public bool ContainsKey(string key)
        {
            Check.NotNull(key);

            return _collection.ContainsKey(key);
        }

        private bool ContainNullTest(List<Test> tests)
        {
            foreach (var test in tests)
            {
                if (test == null || test.GetAnswers().Count == 0)
                {
                    return true;
                }
            }

            return false;
        }

        private string GetAdaptedKey(string key)
        {
            if (Regex.IsMatch(key, @"^[a-zA-z]*\s[a-zA-Z]+$"))
            {
                return key;
            }

            if (Regex.IsMatch(key, @"^[a-zA-z]+[,]\s[a-zA-Z]+$"))
            {
                return key.Split(',').Select(x => x.Trim()).Aggregate((i, j) => j + " " + i);
            }

            throw new ArgumentException("Wrong key", nameof(key));
        }
    }
}
