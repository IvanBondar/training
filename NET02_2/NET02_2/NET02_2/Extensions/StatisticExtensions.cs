﻿using NET02_2.Infrastructure.Helpers;
using NET02_2.TestEntity;
using System.Collections.Generic;
using System.Linq;

namespace NET02_2.Extensions
{
    public static class StatisticExtensions
    {
        public static Dictionary<string, int> NameWithPassedTests(this Statistics stats)
        {
            Check.NotNull(stats);

            var statsClone = stats.GetStatistic();
            var result = statsClone.Select(x => new
            {
                Name = x.Key,
                TotalPassedTests = x.Value.Select(y => y).Count()
            }).ToDictionary(r => r.Name, r => r.TotalPassedTests);

            return result;
        }

        public static Test TopTest(this Statistics stats)
        {
            Check.NotNull(stats);

            var statsClone = stats.GetStatistic();
            var result = statsClone.SelectMany(x => x.Value)
                .GroupBy(x => x.Name)
                .Select(x => new
                {
                    Test = x.FirstOrDefault(),
                    Count = x.Count()
                })
            .OrderByDescending(x => x.Count)
            .FirstOrDefault();
            
            return result?.Test;
        }

        public static Dictionary<string, int> NameWithTotalRightAnswers(this Statistics stats)
        {
            Check.NotNull(stats);

            var statsClone = stats.GetStatistic();
            var result = statsClone.Select(x => new
            {
                Name = x.Key,
                NumberOfRightAnswers = x.Value.SelectMany(y => y.GetAnswers().Values)
                                           .Select(ans => ans.Answer)
                                           .Count(ans => ans)
            }).ToDictionary(r => r.Name, r => r.NumberOfRightAnswers);

            return result;
        }

        public static TestQuestion QuestionWithMinRightAnswers(this Statistics stats)
        {
            Check.NotNull(stats);

            var statsClone = stats.GetStatistic();
            var result = statsClone.SelectMany(x => x.Value.SelectMany(y => y.GetAnswers().Values))
                                    .GroupBy(x => x.Question)
                                    .Select(x => new
                                    {
                                        Question = x.Key,
                                        NumberOfWrongAnswer = x.Count(y => !y.Answer)
                                    })
                                    .OrderByDescending(x => x.NumberOfWrongAnswer).FirstOrDefault();

            return result?.Question;
        }
    }
}
