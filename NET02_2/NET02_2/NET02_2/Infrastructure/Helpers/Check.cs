﻿using System;

namespace NET02_2.Infrastructure.Helpers
{
    public static class Check
    {
        private const string DefaultNotNullMessage = "Value cannot be null!";
        private const string DefaultNotNullOrEmptyMessage = "Value cannot be null or empty!";

        public static void NotNull(object value)
        {
            NotNull(value, DefaultNotNullMessage);
        }

        public static void NotNullOrEmpty(string value)
        {
            NotNullOrEmpty(value, DefaultNotNullMessage);
        }

        public static void NotNull(object value, string message)
        {
            if (value == null)
            {
                throw new ArgumentNullException(message);
            }
        }

        public static void NotNullOrEmpty(string value, string message)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(message);
            }
        }
    }
}
