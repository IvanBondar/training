﻿using NET02_2.Infrastructure.Helpers;
using System;

namespace NET02_2.TestEntity
{
    public class TestQuestion
    {
        public Guid Id { get; set; }
        public byte Difficulty { get; set; }
        public string Question { get; set; }

        public TestQuestion(byte difficulty, string question)
        {
            Check.NotNullOrEmpty(question, "question cannot be null or empty");

            Id = Guid.NewGuid();
            Difficulty = difficulty;
            Question = question;
        }

        public TestQuestion Clone() => new TestQuestion(Difficulty, Question) { Id = Id };

        public override bool Equals(object obj)
        {
            var tmp = obj as TestQuestion;
            if(tmp != null)
            {
                return tmp.Id == Id;
            }

            return false;
        }

        public override int GetHashCode() => Id.ToString().GetHashCode();
    }
}
