﻿using NET02_2.Infrastructure.Helpers;

namespace NET02_2.TestEntity
{
    public class TestAnswer
    {
        private readonly TestQuestion _question;

        public TestQuestion Question => _question.Clone();
        public bool Answer { get; }

        public TestAnswer(TestQuestion question, bool answer)
        {
            Check.NotNull(question, "question cannot be null");

            _question = question;
            Answer = answer;
        }

        public TestAnswer Clone() => new TestAnswer(Question, Answer);
    }
}
