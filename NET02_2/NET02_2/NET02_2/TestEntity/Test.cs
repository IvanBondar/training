﻿using NET02_2.Infrastructure.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace NET02_2.TestEntity
{
    public class Test : IEnumerable<TestAnswer>
    {
        private SortedList<short, TestAnswer> _answers;

        public string Name { get; set; }
        public DateTime PassedAt { get; set; }

        public Test(string name, DateTime date)
        {
            Check.NotNullOrEmpty(name, "name cannot be null or empty");

            _answers = new SortedList<short, TestAnswer>();
            Name = name;
            PassedAt = date;
        }

        public SortedList<short, TestAnswer> GetAnswers() => CloneAnswer();

        public IEnumerator<TestAnswer> GetEnumerator()
        {
            foreach (var item in _answers.Reverse())
            {
                yield return item.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void AddAnswers(params TestAnswer[] answers)
        {
            Check.NotNull(answers, "answers cannot be null");

            foreach (var answer in answers)
            {
                if (answer != null)
                {
                    _answers.Add(answer.Question.Difficulty, answer);
                }
            }
        }

        public Test Clone()
        {
            var clone = new Test(Name, PassedAt) { Name = Name, PassedAt = PassedAt };
            clone.AddAnswers(CloneAnswer().Values.ToArray());
            return clone;
        }

        private SortedList<short, TestAnswer> CloneAnswer()
        {
            var clone = new SortedList<short, TestAnswer>();

            foreach (var item in _answers)
            {
                clone.Add(item.Key, item.Value.Clone());
            }

            return clone;
        }
    }
}
